# encoding: utf-8 

require 'nokogiri'
require 'open-uri'
require 'debugger' 

namespace :db do

	def D(*args)

		args.each_with_index do |a, i|
			puts "#{i}. [DEBUG] #{a.inspect}"
		end		 
	end

  desc "Crawle data from spmember.com"
  task :crawle_data, [:id_from, :id_end] => :environment do |t, args|
	  
	  args.with_defaults(:id_from => -1, :id_end => -1)
	  id_from = Integer(args.id_from)
	  id_end = Integer(args.id_end) 
	  id_end = id_from if id_end == -1
		if (-1 == id_from && id_end == -1) || id_from > id_end
			puts "[Error] Usage : rake db:crawle_data[id_from,id_end]" 
			fail
		end
  	crawl(id_from, id_end)
  end

  def crawl(f_id, e_id)
  	puts "[Info] Crawle news from NewsId #{f_id} to #{e_id}"
  	for i in f_id..e_id
  		begin  
			  get_news i
			rescue Exception => e  
			  puts e.message  
			  File.open("crawler_error.txt", 'a') { |file| file.write("[EXCEPTION] NewsId = #{i} Message = #{e.message}\n"); file.close } 
			end  
  	end
  end

  def get_news(id)
  	@news_id = id
  	puts "[Info] Crawling NewsId #{id}"
  	c = Content.find_by_news_id id
  	if c.nil?
			doc = Nokogiri::HTML(open("http://spmember.com/News/News.aspx?newsID=#{id}"), nil, 'utf-8')
			return false if verify_if_news_dont_exist(doc, id)
			html_cat = doc.css('.info a').at(2).text
			@cat = Category.find_by_name html_cat
			page = doc.css('#sub_main')
			if @cat.nil?
				@cat = Category.new
				@cat.name = html_cat
				@cat.color = "white"
				@cat.save
			end
			if @cat
				@created_at = nil
				@title = page.css('.text_title').text
				@is_event = determine_if_event(page)
				@created_at = page.css("p").at(1).text.strip.split(' ').first.to_datetime unless page.css("p").at(1).text.strip.split(' ').first
				@content_text_html = page.css(".text_left").to_html.gsub("src=\"/UpLoadFile", "src=\"http://www.spmember.com/UpLoadFile")
				@adress = @start_time = @end_time = nil
				if @is_event
					divtime = nil

					page.css(".text_left div").each do |d| 
						 @adress = d.text.gsub("【活动地点】", "").strip if d.text =~ /活动地点/
						 @start_time = d.children.last.text.tr("年日月", "-").gsub(/-[^0-9]+/, " ").to_datetime if d.text =~ /活动时间/
					end 
				end
				c = Content.new
				c.category = @cat
				c.title = @title
				c.data = @content_text_html
				c.news_id = @news_id
				c.created_at = @created_at
				c.updated_at = @created_at
				D @news_id, @cat, @title, @is_event, @created_at, @content_text_html
				if @is_event
					ei = c.event_infos.new
					ei.start_time = @start_time
					ei.adress = @adress
					D @adress, @start_time
				end
				c.save
			else
				puts "[SKIP] Category #{html_cat} don't exist. Skipping."
				sleep 3
			end 
  	else
  		puts "[SKIP] News with NewsId #{id} already exist. Skipping."
  	end
  end

  def verify_if_news_dont_exist(doc, id)
  	if doc.css('#error').count == 1
	  	puts "[SKIP] News with NewsId = #{id} don't exist. Skipping."
	  	true
	  else
	  	false
	  end
  end

  def determine_if_event(page)
  	event_clue_count = 0
  	event_clue_count += 1 if page.text =~ /邀请函/
  	event_clue_count += 1 if page.text =~ /活动地点/
  	event_clue_count += 1 if page.text =~ /报名/
  	event_clue_count += 1 if page.text =~ /活动时间/
  	(event_clue_count > 3) ? true : false
  end

 end
