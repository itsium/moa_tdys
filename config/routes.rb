Tuspark::Application.routes.draw do
  resources :pois


  resources :companies


  resources :contents do
    member do
      get "subscribe"
      get "unsubscribe"
      get "invitation"
    end
  end

  get "/company/get", :to => "companies#home"

  resources :categories
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root :to => "home#index"
  devise_for :users, :controllers =>
  		{
  			sessions: 'sessions',
  			registrations: 'registrations'
  		}
  devise_scope :user do
    get "/users/me", :to => "users#me"
    get "/users/add_favorite", :to => "users#add_favorite"
    get "/users/delete_favorite", :to => "users#delete_favorite"
    get "/users/list_favorites", :to => "users#list_favorites"
    get "/users/favorites", :to => "users#favorites"
  end
end
