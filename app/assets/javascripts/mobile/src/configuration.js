App.conf = {
    contact: {
        enabled: false,
        router: {
            routes: {
                '/more/contact': ['contact', 'more']
            }
        }
        //templates: ['contact']
    },
    home: {
        enabled: true,
        router: {
            routes: {
                '/home': ['home', 'home']
            }
        },
        footer: {
            id: 'menu-home-btn',
            icon: '&#128214;',
            route: '/home',
            text: t('Home')
        },
        templates: [
            'homedetail'
        ]
    },
    map: {
        enabled: true,
        router: {
            routes: {
                '/map': ['map', 'map']
            }
        },
        footer: {
            id: 'menu-map-btn',
            icon: '&#59172;',
            route: '/map',
            text: t('Map')
        }
    },
    about: {
        enabled: true,
        router: {
            routes: {
                '/more/about': ['moreabout', 'more']
            }
        }
    },
    favorites: {
        enabled: true,
        router: {
            routes: {
                '/user/favorites': ['userfavorites', 'favorites']
            },
            before: {
                '^userfavorites$': 'isAuthenticated'
            }
        },
        footer: {
            id: 'menu-favorites-btn',
            icon: '&#9733;',
            route: '/user/favorites',
            text: t('Favorites')
        },
        templates: [
            'userfavoriteslist'
        ]
    },
    user: {
        enabled: true,
        router: {
            routes: {
                '/user/profile': ['userprofile', 'more'],
                '/user/login(/?back=:back)': ['userlogin', 'more'],
                '/user/register(/?back=:back)': ['userregister', 'more']
            },
            before: {
                '^userprofile$': 'isAuthenticated',
                '^user(login|register)$': 'isAlreadyAuthenticated'
            }
        }
    },
    enabled: function(name) {
        return (typeof this[name] === 'object' && this[name].enabled === true);
    }
};
