/* News detail page */

App.views.Newsdetail = (function() {

    var _handlebars = Handlebars,
        _settings = App.Settings,
        _serverUrl = _settings.get('serverUrl');

    _handlebars.registerHelper('hasImage', function(context, options) {
        if (context && context.length) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    _handlebars.registerHelper('getImage', function(medias, index) {
        index = index || 0;
        return '"' + _serverUrl + medias[index].medium.replace('"', '\\"') + '"';
    });

    _handlebars.registerHelper('getUrl', function(url) {
        return '"' + _serverUrl + url.replace('"', '\\"') + '"';
    });

    _handlebars.registerHelper('nl2br', function(str) {
        if (str.indexOf('<br>') === -1) {
            var breakTag = '<br />';
            str = (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        }
        return new _handlebars.SafeString(str);
    });

    var _mapTpl, _dateTpl,
        _id = 'newsdetail',
        _getUrl = _settings.gets('serverUrl', 'contentGet').join(''),
        _addFavUrl = _settings.gets('serverUrl', 'addFavorite').join(''),
        _delFavUrl = _settings.gets('serverUrl', 'deleteFavorite').join(''),
        _subscribeUrl = _settings.gets('serverUrl', 'subscribeEvent').join(''),
        _unsubscribeUrl = _settings.gets('serverUrl', 'unsubscribeEvent').join(''),
        _backUrl,
        _detailTpl,
        _titleEl,
        _detailTitleEl,
        _newsId,
        _curData,
        _fs = Fs,
        _win = window,
        _scrollSave = {},
        _storage = _fs.Storage,
        _helpers = App.helpers,
        _selector = _fs.Selector,
        _parent = _fs.views.Container;

    var _onAfterRender = function() {
        var doc = document;

        _mapTpl = _fs.Templates.get('newsdetailmap');
        _dateTpl = _fs.Templates.get('newsdetaildate');

        _detailTpl.el = doc.getElementById(_id + '-item');
        _detailTpl.parentEl = _detailTpl.el.parentNode;
        _titleEl = doc.getElementById(_id + '-title');

        _detailTitleEl = doc.getElementById('news-detail-content-header')
            .querySelector('.ui-btn-text');
        this.registerButton.textEl = doc.getElementById(this.registerButton.config.id)
            .querySelector('.ui-btn-text');
        this.unregisterButton.textEl = doc.getElementById(this.unregisterButton.config.id)
            .querySelector('.ui-btn-text');

        this.zoomEvent = new _fs.events.Click({
            autoAttach: true,
            el: this.contentDetail.el,
            scope: this,
            handler: function(e) {
                if (_selector.hasClass(e.target, 'zoomable') === true) {

                    var img = e.target.style['background-image'].replace('url(', '');
                    img = img.substring(0, img.length - 1);

                    if (!this.i) {
                        this.i = new _fs.views.ImageZoom({
                            config: {
                                hidden: true
                            }
                        });
                        this.i.compile();
                        this.i.render('body', 'beforeend');
                    }
                    this.i.updateImage(img).show();
                }
            }
        });

        if (App.conf.enabled('favorites') === true) {
            this.favBtn.iconEl = this.favBtn.el.querySelector('.icon');
        }
    };

    var _onShow = function(routeArgs) {
        _newsId = routeArgs[0];
        _backUrl = routeArgs[1];

        _detailLoading.call(this);

        var news = _storage.getItem(_id + _newsId);

        if (!news) {
            var url = _getUrl.replace('{id}', _newsId);
            _helpers.showLoading();
            _fs.Request.jsonp(url + '?callback={callback}', {
                scope: this,
                callback: function(success, data) {
                    _helpers.hideLoading();
                    if (success && data.success) {
                        _updateDetailEl.call(this, data.data);
                    } else {
                        App.errors.onJsonpError(success, data);
                    }
                }
            });
        } else {
            _updateDetailEl.call(this, news);
        }
    };

    var _onHide = function(scrollX, scrollY) {
        if (_newsId) {
            _scrollSave[_newsId] = scrollY || 0;
        }
    };

    var _detailLoading = function() {
        _detailTpl.parentEl.innerHTML = '';
        _titleEl.innerHTML = 'LOADING...';
        _selector.addClass(this.contentDetail.el, 'hidden');
        _selector.addClass(this.mapDetail.el, 'hidden');
        _selector.addClass(this.dateDetail.el, 'hidden');
        _selector.addClass(this.registerButton.el, 'hidden');
        _selector.addClass(this.unregisterButton.el, 'hidden');
    };

    var _updateDetailEl = function(data) {
        _storage.setItem(_id + _newsId, data);
        _curData = data;

        _titleEl.innerHTML = data.title;
        _detailTpl.config.data = data;
        _detailTitleEl.innerHTML = data.created_at.substring(0, 10);

        _selector.removeClass(this.contentDetail.el, 'hidden');
        //_updateComments.call(this, data);

        if (App.conf.enabled('favorites') === true) {
            _updateFavorite.call(this, data.is_favorite);
        }

        _updateLocation.call(this, data);
        _updateDate.call(this, data);
        _updateRegister.call(this, data);

        _detailTpl.compile();
        _detailTpl.render(_detailTpl.parentEl);

        _win.scrollTo(0, _scrollSave[_newsId] || 0);
    };

    /*var _updateComments = function(data) {
        var textEl = _selector.get('.ui-btn-text', this.commentsBtn.el);

        if (data.comments && data.comments.length) {
            textEl.innerHTML = data.comments.length;
        } else {
            textEl.innerHTML = '0';
        }
    };*/

    var _updateRegister = function(data) {
        var infos = (data && data.event_info ? data.event_info : null);

        if (infos) {

            if (!infos.open_subscription) {
                this.registerButton.textEl.innerHTML = t('Not opened yet');
                this.registerButton.setDisabled(true);
            } else if (infos.is_full) {
                this.registerButton.textEl.innerHTML = t('Full');
                this.registerButton.setDisabled(true);
            } else {
                this.registerButton.textEl.innerHTML = t('Subscribe to event');
                this.registerButton.setDisabled(false);
            }

            if (infos.open_subscription && infos.has_subscribe) {
                _selector.removeClass(this.unregisterButton.el, 'hidden');
                _selector.addClass(this.registerButton.el, 'hidden');
            } else {
                _selector.removeClass(this.registerButton.el, 'hidden');
                _selector.addClass(this.unregisterButton.el, 'hidden');
            }

        } else {
            _selector.addClass(this.registerButton.el, 'hidden');
            _selector.addClass(this.unregisterButton.el, 'hidden');
        }
    };

    var _updateLocation = function(data) {
        var mapTextEl = _selector.get('.ui-btn-text', this.mapBtn.el);

        if (data && data.event_info && data.event_info.adress) {
            this.mapBtn.setDisabled(false);
            //mapTextEl.innerHTML = '<span class="icon btn-check">&#10003;</span>';
            mapTextEl.innerHTML = '&nbsp;';
            this.mapDetail.contentEl.innerHTML = _mapTpl(data.event_info);
            _selector.removeClass(this.mapDetail.el, 'hidden');
        } else {
            this.mapBtn.setDisabled(true);
            //mapTextEl.innerHTML = '<span class="icon btn-check">&#10060;</span>';
            mapTextEl.innerHTML = '&nbsp;';
            _selector.addClass(this.mapDetail.el, 'hidden');
        }
    };

    var _updateDate = function(data) {
        var dateTextEl = _selector.get('.ui-btn-text', this.dateBtn.el);

        if (data && data.event_info && data.event_info.start_time) {
            this.dateBtn.setDisabled(false);
            //dateTextEl.innerHTML = '<span class="icon btn-check">&#10003;</span>';
            dateTextEl.innerHTML = '&nbsp;';
            this.dateDetail.contentEl.innerHTML = _dateTpl(data.event_info);
            _selector.removeClass(this.dateDetail.el, 'hidden');
        } else {
            this.dateBtn.setDisabled(true);
            //dateTextEl.innerHTML = '<span class="icon btn-check">&#10060;</span>';
            dateTextEl.innerHTML = '&nbsp;';
            _selector.addClass(this.dateDetail.el, 'hidden');
        }
    };

    var _updateFavorite = function(enabled) {
        if (enabled) {
            this.favBtn.iconEl.innerHTML = '&#9733;';
            _selector.addClass(this.favBtn.el, 'is-fav');
        } else {
            this.favBtn.iconEl.innerHTML = '&#9734;';
            _selector.removeClass(this.favBtn.el, 'is-fav');
        }
    };

    var _toggleFavorite = function() {
        if (_helpers.isLoading()) {
            return false;
        }

        var user = _settings.get('currentUser');

        if (typeof user !== 'object') {
            _settings.set('flashlogin', t('You need to login for add a favorite.'));
            _fs.History.navigate('/user/login/?back=' + _fs.History.here(true));
            return false;
        }

        var isFav = _curData.is_favorite,
            url = (isFav ? _delFavUrl : _addFavUrl);

        _helpers.showLoading();
        url = url.replace('{id}', _curData.id) + '&callback={callback}';
        _fs.Request.jsonp(url, {
            scope: this,
            callback: function(success, data) {
                _helpers.hideLoading();
                if (success && data.success) {
                    _curData.is_favorite = !isFav;
                    _storage.setItem(_id + _newsId, _curData);
                    _updateFavorite.call(this, _curData.is_favorite);
                    App.Settings.set('newsupdated', true);
                } else {
                    App.errors.onJsonpError(success, data);
                }
            }
        });
    };

    var _onRegisterClick = function() {
        var user = _settings.get('currentUser');

        if (typeof user !== 'object') {
            _settings.set('flashlogin', t('You need to login for register to an event.'));
            _fs.History.navigate('/user/login/?back=' + _fs.History.here(true));
            return;
        }

        _helpers.showLoading();

        var url = _subscribeUrl.replace('{id}',
            _curData.id) + '?callback={callback}';

        _fs.Request.jsonp(url, {
            scope: this,
            callback: function(success, data) {
                _helpers.hideLoading();
                if (success && data.success) {
                    _curData.is_favorite = true;
                    _curData.event_info.has_subscribe = true;
                    _storage.setItem(_id + _newsId, _curData);

                    _updateFavorite.call(this, _curData.is_favorite);
                    _updateRegister.call(this, _curData);
                } else {
                    App.errors.onJsonpError(success, data);
                }
            }
        });
    };

    var _onUnregisterClick = function() {
        _helpers.showLoading();

        var url = _unsubscribeUrl.replace('{id}',
            _curData.id) + '?callback={callback}';

        _fs.Request.jsonp(url, {
            scope: this,
            callback: function(success, data) {
                _helpers.hideLoading();
                if (success && data.success) {
                    _curData.event_info.has_subscribe = false;
                    _storage.setItem(_id + _newsId, _curData);

                    _updateRegister.call(this, _curData);
                } else {
                    App.errors.onJsonpError(success, data);
                }
            }
        });
    };

    return _parent.subclass({

        constructor: function(opts) {

            var self = this;

            _detailTpl = new _fs.views.Template({
                template: _fs.Templates.get('newsdetail')
            });

            var headerItems = [{
                xtype: 'button',
                config: {
                    ui: 'f',
                    size: 'large',
                    cssClass: 'ui-btn-left simple-btn',
                    icon: '&#59233;'
                },
                scope: this,
                handler: function() {
                    if (_backUrl) {
                        _fs.History.navigate(decodeURIComponent(_backUrl));
                    } else {
                        _fs.History.navigate('/news/search');
                    }
                }
            }];

            if (App.conf.enabled('favorites') === true) {
                headerItems.push({
                    xtype: 'button',
                    ref: 'favBtn',
                    config: {
                        ui: 'f',
                        size: 'large',
                        cssClass: 'ui-btn-right simple-btn-large',
                        icon: '&#9734;'
                    },
                    scope: this,
                    handler: _toggleFavorite
                });
            }

            headerItems.push(['<h1 id="', _id, '-title',
                '" class="ui-title"></h1>'].join(''));

            var items = [{
                xtype: 'header',
                config: {
                    ui: 'f',
                    position: 'fixed'
                },
                items: headerItems
            }, {
                xtype: 'abstract',
                config: {
                    style: 'padding-bottom: 51px; padding-top: 7px;'
                },
                ref: 'content',
                xtpl: 'content',
                items: [{
                    xtype: 'button',
                    ref: 'mapBtn',
                    config: {
                        text: '',
                        icon: '&#59176;',
                        ui: 'light-gray',
                        cssClass: 'btn-notext simple-btn-mini',
                        style: 'float: right; margin: 0px -10px 0px 0px; z-index: 10;',
                        size: 'medium'
                    },
                    scope: this,
                    handler: function() {
                        _selector.scrollToEl(document.getElementById('news-detail-map'), 0, -51);
                    }
                }, {
                    xtype: 'button',
                    ref: 'dateBtn',
                    config: {
                        text: '',
                        icon: '&#128340;',
                        ui: 'light-gray',
                        cssClass: 'btn-notext simple-btn-mini',
                        style: 'float: right; margin: 0px -10px 0px 0px; z-index: 10;',
                        size: 'medium'
                    },
                    scope: this,
                    handler: function() {
                        _selector.scrollToEl(document.getElementById('news-detail-date'), 0, -51);
                    }
                }, {
                    xtype: 'collapsible',
                    ref: 'contentDetail',
                    config: {
                        id: 'news-detail-content',
                        size: 'small',
                        header: {
                            title: t('Loading'),
                            ui: 'light-gray'
                        },
                        iconCollapsed: 'calendar',
                        iconExpanded: 'calendar',
                        canCollapse: false,
                        content: {
                            ui: 'c'
                        }
                    },
                    items: _detailTpl
                }, {
                    xtype: 'collapsible',
                    ref: 'dateDetail',
                    config: {
                        id: 'news-detail-date',
                        size: 'small',
                        header: {
                            title: t('Event date'),
                            ui: 'light-gray'
                        },
                        iconCollapsed: 'clock',
                        iconExpanded: 'clock',
                        canCollapse: false,
                        content: {
                            ui: 'c'
                        }
                    }
                }, {
                    xtype: 'collapsible',
                    ref: 'mapDetail',
                    config: {
                        id: 'news-detail-map',
                        size: 'small',
                        header: {
                            title: t('Map'),
                            ui: 'light-gray'
                        },
                        iconCollapsed: 'map',
                        iconExpanded: 'map',
                        canCollapse: false,
                        content: {
                            ui: 'c'
                        }
                    }
                }, {
                    xtype: 'button',
                    ref: 'registerButton',
                    config: {
                        inline: false,
                        ui: 'g',
                        text: t('Subscribe to event'),
                        icon: '&#59136;'
                    },
                    scope: this,
                    handler: _onRegisterClick
                }, {
                    xtype: 'button',
                    ref: 'unregisterButton',
                    config: {
                        inline: false,
                        ui: 'd',
                        text: t('Unsubscribe to event'),
                        icon: '&#59201;'
                    },
                    scope: this,
                    handler: _onUnregisterClick
                }]
            }];

            _parent.prototype.constructor.call(self, {
                config: _fs.utils.applyIfAuto({
                    style: 'padding-top: 43px;'
                }, opts.config),
                listeners: {
                    scope: self,
                    afterrender: _onAfterRender
                },
                items: items
            });

            self.showEID = self.on('show', _onShow, self);
            self.hideEID = self.on('hide', _onHide, self);
        }
    });

}());