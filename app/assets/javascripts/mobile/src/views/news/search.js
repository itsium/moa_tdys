/* News page */

App.views.Newssearch = (function() {

    var _handlebars = Handlebars,
        _serverUrl = App.Settings.get('serverUrl'),
        _contentUrl = App.Settings.gets('serverUrl', 'contentSearch').join(''),
        _categoryUrl = App.Settings.gets('serverUrl', 'categoryList').join('');

    _handlebars.registerHelper('hasThumbnail', function(context, options) {
        if (context && context.length) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    _handlebars.registerHelper('getThumbnail', function(medias) {
        return '"' + _serverUrl + medias[0].thumb.replace('"', '\\"') + '"';
    });

    var _needReload,
        _newsList,
        _header,
        _categoryMenu,
        _categoryList,
        _fs = Fs,
        _fsviews = _fs.views,
        _debug = _fs.debug,
        _parent = _fsviews.Container,
        _settings = App.Settings,
        _emptyTpl = [
            '<div class="news-listing-empty">',
                '<span class="icon">&#128240;</span>',
                '<span class="message">',
                    t('No news.'),
                '</span>',
            '</div>'
        ].join('');

    var _onShow = function() {
        if (_needReload === true) {
            _newsList.reload();
            _needReload = false;
        }
        this.showCategoriesEvent.attach();
    };

    var _onHide = function() {
        this.showCategoriesEvent.detach();
    };

    var _onAfterRender = function() {

        function forceReload() {
            _needReload = true;
        }

        _settings.on('currentUserchanged', forceReload);
        _settings.on('newsupdatedchanged', forceReload);

        this.headerEl = document.getElementById(_header.config.id);
        this.showCategoriesEvent = new _fs.events.Click({
            autoAttach: true,
            el: this.headerEl,
            handler: function() {
                _categoryMenu.show();
            }
        });
    };

    return _parent.subclass({

        constructor: function(opts) {

            this.categoriesStore = new Fs.data.Store({
                enableHashTable: true,
                storageCache: true,
                reader: {
                    rootProperty: 'data',
                    successProperty: 'success',
                    totalProperty: 'total'
                },
                proxy: {
                    type: 'jsonp',
                    cache: false,
                    url: _categoryUrl + '?callback={callback}'
                }
            });

            var self = this;

            this.categoriesStore.on('afterload', function(success, store, newRecords) {
                var record = {
                    id: 0,
                    name: t('Show all'),
                    color: 'white'
                };
                self.categoriesStore.addRecord(record);
                _categoryList.loadItems([record]);
            }, this);
            this.categoriesStore.on('loaderror', App.errors.onStoreError);

            this.newsStore = new Fs.data.PagingStore({
                enableHashTable: true,
                recordPerPage: 10,
                reader: {
                    rootProperty: 'data',
                    successProperty: 'success',
                    totalProperty: 'total'
                },
                proxy: {
                    type: 'jsonp',
                    cache: false,
                    url: _contentUrl + '?callback={callback}'
                }
            });
            this.newsStore.on('loaderror', App.errors.onStoreError);

            _newsList = new _fsviews.ListBuffered({
                config: {
                    cssClass: 'newslist scroll',
                    ui: 'w'
                },
                store: this.newsStore,
                itemTpl: _fs.Templates.get('newssearchlist'),
                emptyTpl: _emptyTpl,
                listeners: {
                    scope: this,
                    itemselect: function(e) {
                        var recordId,
                            item = _newsList.getItemFromEvent(e);

                        if (item !== false) {
                            recordId = item.getAttribute('data-id');
                            Fs.History.navigate('/news/detail/' + recordId);
                        }
                    }
                }
            });
            _header = new _fsviews.Header({
                config: {
                    id: 'news-header',
                    ui: 'f'
                },
                items: ['<h1 class="ui-title" id="news-category-title">' + G_APP_NAME + '<div class="icon news-header-arrow">&#9662;</div></h1>']
            });

            _categoryList = new _fsviews.List({
                config: {
                    cssClass: 'categorylist list-flat', // scroll
                    ui: 'flat',
                    style: 'height: 70%; min-height: 30px; max-height: 300px;'
                },
                store: this.categoriesStore,
                itemTpl: _fs.Templates.get('newssearchcategorylist'),
                listeners: {
                    scope: this,
                    itemselect: function(e) {
                        var item = _categoryList.getItemFromEvent(e),
                            category_id = item.getAttribute('data-id'),
                            record = this.categoriesStore.getRecord(category_id);
                        var catname = record.name;
                        if (!record.id) {
                            catname = G_APP_NAME;
                        }
                        document.getElementById('news-category-title').innerHTML = catname +
                            '<div class="icon news-header-arrow">&#9662;</div>';
                        _debug.log('news', 'category item click', category_id);
                        _categoryMenu.hide();
                        if (!record.id) {
                            _newsList.store.setProxyOpts({
                                jsonParams: {}
                            });
                        } else {
                            _newsList.store.setProxyOpts({
                                jsonParams: {
                                    category: category_id
                                }
                            });
                        }
                        _newsList.reload();
                        _debug.log('news', 'store proxy opts', _newsList.store.getProxyOpts());
                    }
                }
            });

            // category menu floating panel
            _categoryMenu = new _fsviews.FloatingPanel({
                config: {
                    id: 'category-menu',
                    hidden: true,
                    overlay: 'darklight',
                    arrow: 'top',
                    style: [
                        'top: 55px;',
                        'width: 30%; min-width: 175px; max-width: 350px;',
                        'min-height: 40px;'// max-height: 300px;'
                    ].join('')
                },
                //renderer: this,
                items: [
                    _categoryList
                ]
            });

            opts.config.style = 'padding-bottom: 52px;';
            opts = {
                config: opts.config,
                items: [
                    _header,
                    _newsList,
                    _categoryMenu
                ],
                listeners: {
                    scope: this,
                    afterrender: _onAfterRender
                }
            };

            _parent.prototype.constructor.call(this, opts);

            this.on('show', _onShow, this);
            this.on('hide', _onHide, this);
        }
    });

}());