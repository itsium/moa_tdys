/* home.js */

App.views.Home = (function() {

    var _fs = Fs,
        _url = App.Settings.gets('serverUrl', 'homeGet').join(''),
        _request = _fs.Request,
        _parent = _fs.views.Container;

    var _onafterrender = function() {

        this.detailTpl.parentEl = this.detailTpl.el.parentNode;

        _request.jsonp(_url + '?callback={callback}', {
            scope: this,
            callback: function(success, data) {

                if (success === true &&
                    typeof data === 'object' &&
                    data.success === true) {
                    _updateLayout.call(this, data);
                } else {
                    App.errors.onJsonpError(success, data);
                }

            }
        });
    };

    var _updateLayout = function(data) {

        var i = -1,
            len = data.data.images.length,
            images = [],
            url = App.Settings.get('serverUrl');

        while (++i < len) {
            images.push('"' + url + '/' +
                data.data.images[i].url.replace('"', '\\"') + '"');
        }

        this.carousel = new _fs.views.Carousel({
            config: {
                style: 'border-bottom: 2px solid #ddd;',
                images: images,
                height: '250px',
                animation: 1000
            }
        });

        this.carousel.compile();
        this.carousel.render(this.content.el, 'beforebegin');
        this.on('show', function() {
            this.carousel.fire('show');
        }, this);
        this.on('hide', function() {
            this.carousel.fire('hide');
        }, this);
        this.carousel.fire('show');

        this.detailTpl.data = data;
        this.detailTpl.compile();
        this.detailTpl.render(this.content.el);

    };

    return _parent.subclass({

        constructor: function(opts) {

            this.detailTpl = new _fs.views.Template({
                template: _fs.Templates.get('homedetail')
            });

            opts.config.style = 'padding-bottom: 52px;';

            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'f',
                        title: t('Home')
                    }
                }, {
                    xtype: 'abstract',
                    xtpl: 'content',
                    ref: 'content'
                }]
            });

            this.on('afterrender', _onafterrender, this);
        }

    });

}());