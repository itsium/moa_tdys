/* contact.js */

App.views.Contact = (function() {

    var _fs = Fs,
        _request = _fs.Request,
        _parent = _fs.views.Container;

    return _parent.subclass({

        constructor: function(opts) {

            opts.config.style = 'padding-bottom: 52px;';

            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'f',
                        title: t('Home')
                    }
                }, {
                    xtype: 'abstract',
                    xtpl: 'content',
                    //xtype: 'container',
                    ref: 'content',
                    config: {
                        style: 'padding-top: 0px !important;'
                    },
                    items: [{
                        xtype: 'collapsible',
                        config: {
                            header: {
                                title: t('Map'),
                                ui: 'f'
                            },
                            iconPos: 'left',
                            size: 'small'
                        },
                        items: ['<p>PROUT</p>']
                    }, {
                        xtype: 'collapsible',
                        config: {
                            header: {
                                title: t('Direction'),
                                ui: 'f'
                            },
                            iconPos: 'left',
                            size: 'small'
                        },
                        items: ['<p>LOL</p>']
                    }]
                }]
            });

        }

    });

}());