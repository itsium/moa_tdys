/* Layout page */

App.views.Layout = (function() {
    var _tabs,
        _header,
        _footer,
        _fs = Fs,
        _iconPos = 'top',
        _showText = false,
        _parent = _fs.views.Page;

    var _createTabButtons = function() {

        var buttons = [], conf = App.conf;

        if (conf.enabled('home') === true) {
            buttons.push(_getTabButton('home'));
        }

        buttons.push({
            xtype: 'tabbutton',
            config: {
                id: 'menu-news-btn',
                icon: '&#9776;',
                route: '/news/search',
                text: (_showText ? t('News') : undefined),
                iconPos: _iconPos
            }
        });

        if (conf.enabled('favorites') === true) {
            buttons.push(_getTabButton('favorites'));
        }

        if (conf.enabled('map') === true) {
            buttons.push(_getTabButton('map'));
        }

        if (conf.enabled('user') === true ||
            conf.enabled('about') === true) {

            buttons.push({
                xtype: 'tabbutton',
                config: {
                    id: 'menu-more-btn',
                    icon: '&#59170;',
                    route: '/more/menu',
                    text: (_showText ? t('Profile') : undefined),
                    iconPos: _iconPos
                }
            });

        }

        return buttons;

    };

    var _getTabButton = function(name) {

        var conf = App.conf;

        var btn = {
            xtype: 'tabbutton',
            config: conf[name].footer
        };

        if (_showText === false) {
            delete btn.config.text;
        }
        btn.config.iconPos = _iconPos;
        return btn;

    };

    _tabs = {
        xtype: 'tabs',
        config: {
            id: 'main-menu',
            //cssClass: 'largetabs',
            mini: false
        },
        items: _createTabButtons()
    };

    _footer = {
        xtype: 'footer',
        config: {
            ui: 'f',
            id: 'page-footer',
            position: 'fixed'
        },
        items: _tabs
    };

    return _parent.subclass({
        constructor: function(opts) {
            opts = _fs.utils.applyIfAuto(opts || {}, {
                items: _footer
            });
            _parent.prototype.constructor.call(this, opts);
        }
    });
}());