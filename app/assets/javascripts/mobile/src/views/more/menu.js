/* menu */

App.views.Moremenu = (function() {

    var _fs = Fs,
        _settings = App.Settings,
        _helpers = App.helpers,
        _errors = App.errors,
        _request = _fs.Request,
        _selector = _fs.Selector,
        _parent = _fs.views.Container;

    var _logout = function() {
        var url = _settings.gets('serverUrl',
            'userLogout').join('') + '&callback={callback}';

        _helpers.showLoading();
        _request.jsonp(url, {
            scope: this,
            callback: function(success, data) {
                _helpers.hideLoading();
                if (success && data.success) {
                    _fs.Storage.empty();
                    _settings.set('currentUser', null);
                } else {
                    _errors.onJsonpError(success, data);
                }
            }
        });
    };

    var _refreshUI = function() {
        if (this.hidden) {
            this.needRefresh = true;
            return false;
        }
        var user = arguments[2] || _settings.get('currentUser');

        if (typeof user !== 'object') {
            _selector.addClass(this.userList.el, 'hidden');
            _selector.removeClass(this.anonymousList.el, 'hidden');
        } else {
            this.userList.bubbleEl.innerHTML = user.email;
            _selector.removeClass(this.userList.el, 'hidden');
            _selector.addClass(this.anonymousList.el, 'hidden');
        }
    };

    var _onShow = function() {
        this.hidden = false;
        if (this.needRefresh) {
            this.needRefresh = false;
            _refreshUI.call(this);
        }
    };

    var _onHide = function() {
        this.hidden = true;
    };

    var _onAfterRender = function() {
        if (App.conf.enabled('user') === true) {
            _settings.on('currentUserchanged', _refreshUI, this);
            this.on('show', _onShow, this);
            this.on('hide', _onHide, this);
            this.userList.bubbleEl = this.userList.el.querySelector('.ui-li-count');
            _refreshUI.call(this);
        }
    };

    var _onItemSelect = function(event) {
        var href = event.target.getAttribute('href');

        if (href === '#/user/logout') {
            _logout();
        } else {
            _fs.History.navigate(href.replace('#', ''));
        }
    };

    return _parent.subclass({

        constructor: function(opts) {

            var items = [];

            if (App.conf.enabled('user') === true) {

                items.push({xtype: 'list',
                    ref: 'userList',
                    config: {
                        ui: 'cleanlist',
                        icon: 'arrow-r',
                        inset: true,
                        corner: 'all'
                    },
                    itemTpl: _fs.Templates.get('moremenu'),
                    items: [{
                        type: 'divider',
                        ui: 'd',
                        label: t('Logged as'),
                        bubble: '-'
                    }, {
                        label: t('My profile'),
                        labelIcon: '&#59170;',
                        route: '/user/profile'
                    }, {
                        label: t('Logout'),
                        labelIcon: '&#10150;',
                        route: '/user/logout'
                    }],
                    listeners: {
                        scope: this,
                        itemselect: _onItemSelect
                    }
                });

                items.push({
                    xtype: 'list',
                    ref: 'anonymousList',
                    config: {
                        ui: 'cleanlist',
                        icon: 'arrow-r',
                        inset: true,
                        corner: 'all',
                        shadow: true
                    },
                    itemTpl: _fs.Templates.get('moremenu'),
                    items: [{
                        type: 'divider',
                        ui: 'c',
                        label: t('Account')
                    }, {
                        label: t('Login'),
                        labelIcon: '&#128100;',
                        route: '/user/login/?back=' + _fs.History.here(true)
                    }, {
                        label: t('Register'),
                        labelIcon: '&#59136;',
                        route: '/user/register/?back=' + _fs.History.here(true)
                    }]
                });

            }

            if (App.conf.enabled('about') === true ||
                App.conf.enabled('contact') === true) {

                var btns = [];

                if (App.conf.enabled('contact')) {
                    btns.push({
                        label: t('Contact'),
                        labelIcon: '&#59176;',
                        route: '/more/contact'
                    });
                }

                if (App.conf.enabled('about')) {
                    btns.push({
                        label: t('About us'),
                        labelIcon: '&#59141;',
                        route: '/more/about'
                    });
                }

                items.push({
                    xtype: 'list',
                    config: {
                        ui: 'cleanlist',
                        icon: 'arrow-r',
                        inset: true,
                        corner: 'all',
                        shadow: true
                    },
                    itemTpl: _fs.Templates.get('moremenu'),
                    items: btns
                });
            }

            opts.config.style = 'padding-bottom: 52px;';

            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'f',
                        title: t('More')
                    }
                }, {
                    xtype: 'abstract',
                    xtpl: 'content',
                    items: items
                }],
                listeners: {
                    scope: this,
                    afterrender: _onAfterRender
                }
            });
        }

    });

}());