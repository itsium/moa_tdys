/* about */

App.views.Moreabout = (function() {

    var _fs = Fs,
        _parent = _fs.views.Container;


    var _show = function() {
        _fs.Selector.addClass(document.getElementById('page-footer'), 'hidden');
    };

    var _hide = function() {
        _fs.Selector.removeClass(document.getElementById('page-footer'), 'hidden');
    };

    return _parent.subclass({

        constructor: function(opts) {

            var blabla = '北京意得捷优科技有限公司是一家集自主研发及国外技术为一体的专业APP开发公司。\n以独创的技术快速为客户打造出精致、实用、兼容性强的移动应用。\n旨在打造出适用于国内中小企业需求的新媒体工具。
\n详情及合作方式请关注：<a href="http://www.itsium.cn" target="_blank">www.itsium.cn</a>';

            if (blabla.indexOf('<br>') === -1) {
                var breakTag = '<br />';
                blabla = (blabla + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
            }

            var date = (new Date()).getFullYear(),
                version = G_APP_VERSION,
                about = [
                '<div class="ui-grid-solo about">',

                    '<div class="ui-block-a icon-app">',
                        //'<div class="icon-app">&nbsp;</div>',
                    '</div>',

                '</div><div class="ui-grid-solo">',

                    '<div class="ui-block-a">',
                        '<div class="desc-app">',
                            '<span class="title-app">', G_COMPANY_NAME, '</span><br>',
                            '<span class="date-app">&nbsp;version ', version, '</span>',
                        '</div>',
                    '</div>',

                '</div>',
                //'<hr class="separator" style="border-style: solid; border-color: #f5f5f5; width: 50%; margin-bottom: 30px;">',
                '<p>', blabla, '</p><br>',
                '<hr class="separator"><i class="separator-text">', date, '</i>',
                '<div class="about-devby">Developed by ',
                    '<a href="http://www.itsium.cn" target="_blank">itsium.cn</a>',
                '</div>'
            ].join('');

            var items = [{
                xtype: 'header',
                config: {
                    ui: 'c',
                    title: t('About')
                },
                items: [{
                    xtype: 'button',
                    config: {
                        ui: 'f',
                        size: 'large',
                        cssClass: 'ui-btn-left simple-btn',
                        icon: '&#59233;',
                        route: '/more/menu'
                    }
                }]
            }, {
                xtype: 'abstract',
                xtpl: 'content',
                config: {
                    //style: 'padding-bottom: 51px;'
                },
                items: about
            }];

            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: items
            });

            this.on('show', _show, this);
            this.on('hide', _hide, this);

        }

    });

}());