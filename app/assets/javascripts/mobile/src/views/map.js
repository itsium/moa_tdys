/* map.js */

App.views.Map = (function() {

    var _data,
        _self,
        _win = window,
        _doc = _win.document,
        _markers = [],
        _dataReady = false,
        _mapReady = false,
        _toolbarsHeight = 0,
        _fs = Fs,
        _url = App.Settings.gets('serverUrl', 'mapGet').join(''),
        _request = _fs.Request,
        _parent = _fs.views.Container;

    _win.baiduinit = function() {
        _mapReady = true;

        if (_dataReady === true) {
            _drawMap();
        }
    };

    _doc.addEventListener('DOMContentLoaded', function() {
        var style = _doc.createElement('style');

        style.innerHTML = [
            '.BMap_pop, .BMap_shadow {',
                'margin-top: -26px !important;',
            '}'
        ].join('');

        style.id = 'baidumap-infowin-fix';

        _doc.head.appendChild(style);

        _doc.removeEventListener('DOMContentLoaded', arguments.callee, false);
    }, false);

    var _drawMap = function() {

        var item,
            i = -1,
            bm = BMap,
            center = _data.center,
            data = _data.data,
            length = _data.total,
            map = new bm.Map('poimap');

        map.centerAndZoom(new bm.Point(center.longitude, center.latitude), 12);

        while (++i < length) {
            item = data[i];
            _markers.push(new bm.Marker(new bm.Point(item.longitude, item.latitude)));

            _markers[i].addEventListener('click', (function(marker, item) {

                return function() {
                    var html = '',
                        opts = {
                        minWidth: 200,
                        enableMessage: false,
                        title : item.name
                    };

                    if (typeof item.image === 'string' && item.image.length) {
                        html = [
                            html,
                            '<div style="margin: 5px auto; width: 95%; height: 130px; ',
                            'background-image: url(', item.image, '); background-size: cover; background-position: center center; background-repeat: no-repeat;">&nbsp;</div>'
                        ].join('');
                    }
                    html = [html, '<p>', item.description, '</p>'].join('');

                    var infoWindow = new bm.InfoWindow(html, opts);
                    map.openInfoWindow(infoWindow, this.getPosition());
                };
            })(_markers[i], item));

            map.addOverlay(_markers[i]);
        }

    };

    var _onafterrender = function() {

        _toolbarsHeight = this.el.querySelector('.ui-header').offsetHeight +
            document.getElementById('page-footer').offsetHeight;

        _resizeMap();

        _request.jsonp(_url + '?callback={callback}', {
            scope: this,
            callback: function(success, data) {

                if (success === true &&
                    typeof data === 'object' &&
                    data.success === true) {

                    _data = data;
                    _dataReady = true;
                    if (_mapReady === true) {
                        _drawMap();
                    }

                } else {
                    App.errors.onJsonpError(success, data);
                }

            }
        });

    };

    var _resizeMap = function() {

        var height = window.innerHeight - _toolbarsHeight - 40 - 100;

        document.getElementById('poimap-viewport').style.height = height + 'px';

    };

    var _onshow = function() {

        if (!this.eventResize) {
            this.eventResize = new _fs.events.Resize();
        }
        this.eventResize.attach(window, _resizeMap, this);

        _resizeMap();

    };

    var _onhide = function() {

        if (this.eventResize) {
            this.eventResize.detach();
        }

    };

    return _parent.subclass({

        constructor: function(opts) {

            _self = this;

            opts.config.style = 'padding-bottom: 52px;';

            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'f',
                        title: t('Map')
                    }
                },
                '<div id="poimap-viewport" style="width: 100%; height: 300px;"><div id="poimap" style="position: absolute;width: 100%;height: 100%;overflow: hidden;margin:0; top: 50px; left: 0; padding: 0;"></div></div>'
                ]
            });

            this.on('afterrender', _onafterrender, this);
            this.on('show', _onshow, this);
            this.on('hide', _onhide, this);
        }

    });

}());