/* user register */

App.views.Userregister = (function() {

    var _fs = Fs,
        _url = App.Settings.gets('serverUrl', 'userRegister').join(''),
        _helpers = App.helpers,
        _errors = App.errors,
        _request = _fs.Request,
        _parent = _fs.views.Container;

    var _onAfterRender = function() {
        this.keypressEvent = new _fs.events.Abstract({
            eventName: 'keypress',
            el: this.el,
            autoAttach: true,
            scope: this,
            handler: _keypressed
        });
    };

    var _onShow = function(routeArgs) {
        _backUrl = decodeURIComponent(routeArgs[0]) || undefined;
        this.keypressEvent.attach();
    };

    var _onHide = function() {
        this.keypressEvent.detach();
    };

    var _keypressed = function(e) {
        if (e.keyCode === 13) {
            e.target.blur();
            _register.call(this);
        }
    };

    var _register = function() {
        var fields = _helpers.fieldsToJson('input[name]', this.el),
            url = _url + '&callback={callback}';

        if (fields !== false) {
            url = _helpers.replaceByValues(url, fields, encodeURIComponent);
            _helpers.showLoading();
            _request.jsonp(url, {
                scope: this,
                callback: function(success, data) {
                    _helpers.hideLoading();
                    if (success && data.success) {
                        App.Settings.set('currentUser', data.user);
                        _fs.History.navigate('');
                    } else {
                        _errors.onJsonpError(success, data);
                    }
                }
            });
        }
    };

    return _parent.subclass({

        xtpl: 'container',

        constructor: function(opts) {

            var items = [{
                xtype: 'textfield',
                ref: 'emailField',
                config: {
                    name: 'name',
                    icon: 'user',
                    type: 'text',
                    required: false,
                    placeHolder: t('Name') + '...'
                }
            }, {
                xtype: 'textfield',
                ref: 'emailField',
                config: {
                    name: 'email',
                    icon: 'mail',
                    type: 'email',
                    required: true,
                    placeHolder: t('Email') + '...'
                }
            }, {
                xtype: 'textfield',
                ref: 'passField',
                config: {
                    name: 'password',
                    icon: 'key',
                    type: 'password',
                    required: true,
                    placeHolder: t('Password') + '...'
                }
            }, {
                xtype: 'textfield',
                ref: 'passconfirmField',
                config: {
                    name: 'password_confirmation',
                    icon: 'key',
                    type: 'password',
                    required: true,
                    placeHolder: t('Confirm password') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    icon: 'phone',
                    type: 'tel',
                    name: 'phone',
                    placeHolder: t('Phone') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    icon: 'qq',
                    name: 'qq',
                    type: 'tel',
                    placeHolder: t('qq') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    icon: 'wechat',
                    name: 'wechat',
                    placeHolder: t('wechat') + '...'
                }
            }, {
                xtype: 'select',
                config: {
                    emptyText: t('interested'),
                    value: '',
                    options: [{
                        label: '',
                        value: '',
                        disabled: true
                    }, {
                        label: t('zypx'),
                        value: t('zypx')
                    }, {
                        label: t('zyzl'),
                        value: t('zyzl')
                    }]
                },
                listeners: {
                    scope: this,
                    change: function(select, opt) {
                        this.el.querySelector('input[name="interested"]').value = opt.value;
                    }
                }
            }, '<input type="hidden" name="interested" value="" />', {
                xtype: 'button',
                config: {
                    inline: false,
                    ui: 'd',
                    text: t('Register'),
                    xicon: '&#128274;'
                },
                scope: this,
                handler: _register
            }];

            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'f',
                        title: t('Register')
                    },
                    items: [{
                        xtype: 'button',
                        config: {
                            ui: 'f',
                            size: 'large',
                            cssClass: 'ui-btn-left simple-btn',
                            icon: '&#59233;'
                        },
                        scope: this,
                        handler: function() {
                            if (_backUrl) {
                                _fs.History.navigate(_backUrl);
                            } else {
                                _fs.History.navigate('/more/menu');
                            }
                        }
                    }]
                }, {
                    xtype: 'abstract',
                    xtpl: 'content',
                    config: {
                        style: 'padding-bottom: 51px;'
                    },
                    items: items
                }],
                listeners: {
                    scope: this,
                    afterrender: _onAfterRender
                }
            });

            this.on('show', _onShow, this);
            this.on('hide', _onHide, this);
        }

    });

}());