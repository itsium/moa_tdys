/* user login */

App.views.Userlogin = (function() {

    var _backUrl,
        _fs = Fs,
        _app = App,
        _settings = _app.Settings,
        _url = _settings.gets('serverUrl', 'userLogin').join(''),
        _helpers = _app.helpers,
        _errors = _app.errors,
        _request = _fs.Request,
        _parent = _fs.views.Container;

    var _onAfterRender = function() {
        this.keypressEvent = new _fs.events.Abstract({
            eventName: 'keypress',
            el: this.el,
            autoAttach: true,
            scope: this,
            handler: _keypressed
        });

        this.flashMsg = this.flash.el.querySelector('.message');
        _settings.on('flashloginchanged', _showflash, this);
    };

    var _showflash = function(set, name, value) {

        var msg = value || _settings.get('flashlogin');

        if (typeof msg === 'string') {
            this.flashMsg.innerHTML = msg;
            this.flash.show();
            if (this.hidden === false) {
                _settings.empty('flashlogin');
            }
        }
    };

    var _onShow = function(routeArgs) {
        this.hidden = false;
        _backUrl = (routeArgs[0] ? decodeURIComponent(routeArgs[0]) : false);
        if (_backUrl && _backUrl.indexOf('favorites') !== -1) {
            _settings.set('flashlogin',
                t('You need to login to use favorites.'));
        }
        this.keypressEvent.attach();
        _showflash.call(this);
    };

    var _onHide = function() {
        this.hidden = true;
        this.keypressEvent.detach();
        this.flash.hide();
    };

    var _keypressed = function(e) {
        if (e.keyCode === 13) {
            e.target.blur();
            _login.call(this);
        }
    };

    var _login = function() {
        var fields = _helpers.fieldsToJson('input[name]', this.el),
            url = _url + '&callback={callback}';

        if (fields !== false) {
            url = _helpers.replaceByValues(url, fields, encodeURIComponent);
            _helpers.showLoading();
            _request.jsonp(url, {
                scope: this,
                callback: function(success, data) {
                    _helpers.hideLoading();
                    if (success && data.success) {
                        App.Settings.set('currentUser', data.user);
                        _fs.Storage.empty();
                        _fs.History.navigate('');
                    } else {
                        _errors.onJsonpError(success, data);
                    }
                }
            });
        }
    };

    return _parent.subclass({

        xtpl: 'container',

        constructor: function(opts) {

            var fields = [{
                xtype: 'textfield',
                config: {
                    icon: 'mail',
                    type: 'email',
                    name: 'email',
                    required: true,
                    placeHolder: t('Email') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    icon: 'key',
                    type: 'password',
                    name: 'password',
                    required: true,
                    placeHolder: t('Password') + '...'
                }
            }, {
                xtype: 'button',
                ref: 'loginBtn',
                config: {
                    inline: false,
                    ui: 'd',
                    text: t('Sign in'),
                    xicon: '&#128274;'
                },
                scope: this,
                handler: _login
            }, ('<hr class="separator"><i class="separator-text">' + t('or') + '</i>'), {
                xtype: 'button',
                config: {
                    inline: false,
                    ui: 'c',
                    text: t('Register'),
                    xicon: '&#59136;'
                },
                scope: this,
                handler: function() {
                    _fs.History.navigate('/user/register' +
                        (_backUrl ? ('/?back=' + encodeURIComponent(_backUrl)) : ''));
                }
            }];

            var flashmsg = [{
                xtype: 'flash',
                config: {
                    type: 'warning',
                    hidden: true
                },
                ref: 'flash',
                items: ['<p><div class="icon">&#9888;</div><span class="message"></span></p>']
            }];

            var items = [{
                xtype: 'header',
                config: {
                    ui: 'f',
                    title: t('Login')
                },
                items: [{
                    xtype: 'button',
                    config: {
                        ui: 'f',
                        size: 'large',
                        cssClass: 'ui-btn-left simple-btn',
                        icon: '&#59233;'
                    },
                    scope: this,
                    handler: function() {
                        if (_backUrl) {
                            _fs.History.navigate(_backUrl);
                        } else {
                            _fs.History.navigate('/more/menu');
                        }
                    }
                }]
            }, {
                xtype: 'abstract',
                xtpl: 'content',
                config: {
                    style: 'padding-bottom: 51px;'
                },
                items: flashmsg.concat(fields)
            }];

            _parent.prototype.constructor.call(this, {
                config: opts.config,
                items: items,
                listeners: {
                    scope: this,
                    afterrender: _onAfterRender
                }
            });

            this.on('show', _onShow, this);
            this.on('hide', _onHide, this);
        }

    });

}());