/* User profile */

App.views.Userprofile = (function() {

    var _fs = Fs,
        _settings = App.Settings,
        _url = _settings.gets('serverUrl', 'userProfileUpdate').join(''),
        _helpers = App.helpers,
        _errors = App.errors,
        _request = _fs.Request,
        _parent = _fs.views.Container;

    var _onAfterRender = function() {
        this.keypressEvent = new _fs.events.Abstract({
            eventName: 'keypress',
            el: this.el,
            autoAttach: true,
            scope: this,
            handler: _keypressed
        });

        var user = _settings.get('currentUser'),
            fields = _fs.Selector.gets('input[name]', this.el);

        _settings.on('currentUserchanged', function(settings, name, value) {
            _helpers.jsonToFields(value, fields);
            this.interested.fire('change', this.interested, {
                label: document.getElementById('interestedfield').value
            });
        }, this);

        if (user) {
            _helpers.jsonToFields(user, fields);
            this.interested.fire('change', this.interested, {
                label: document.getElementById('interestedfield').value
            });
        }
    };

    var _keypressed = function(e) {
        if (e.keyCode === 13) {
            e.target.blur();
            _updateProfile.call(this);
        }
    };

    var _updateProfile = function() {
        var fields = _helpers.fieldsToJson('input[name]', this.el),
            url = _url + '&callback={callback}';

        if (fields !== false) {
            url = _helpers.replaceByValues(url, fields, encodeURIComponent);
            _helpers.showLoading();
            _request.jsonp(url, {
                scope: this,
                callback: function(success, data) {
                    _helpers.hideLoading();
                    if (success && data.success) {
                        _settings.set('currentUser', data.user);
                    } else {
                        _errors.onJsonpError(success, data);
                    }
                }
            });
        } else {
            var self = this;
            setTimeout(function() {
                _fs.Selector.get('input[name="current_password"]', self.el).focus();
            }, 500);
        }
    };

    return _parent.subclass({

        constructor: function(opts) {

            var self = this;

            var profile = [{
                xtype: 'textfield',
                config: {
                    label: t('Email'),
                    labelInline: true,
                    icon: 'mail',
                    type: 'tel',
                    name: 'email',
                    readonly: true,
                    required: true,
                    disabled: true,
                    placeHolder: t('Email') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('Name'),
                    labelInline: true,
                    icon: 'user',
                    name: 'name',
                    placeHolder: t('Name') + '...'
                }
            }/*, {
                xtype: 'textfield',
                config: {
                    label: t('Company'),
                    labelInline: true,
                    icon: 'company',
                    name: 'company',
                    placeHolder: t('Company') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('Role'),
                    labelInline: true,
                    icon: 'role',
                    name: 'role',
                    placeHolder: t('Role') + '...'
                }
            }*/, {
                xtype: 'textfield',
                config: {
                    label: t('Phone'),
                    labelInline: true,
                    icon: 'phone',
                    type: 'tel',
                    name: 'phone',
                    placeHolder: t('Phone') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('qq'),
                    labelInline: true,
                    icon: 'qq',
                    name: 'qq',
                    type: 'tel',
                    placeHolder: t('qq') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('wechat'),
                    labelInline: true,
                    icon: 'wechat',
                    name: 'wechat',
                    placeHolder: t('wechat') + '...'
                }
            }, {
                xtype: 'textfield',
                config: {
                    label: t('Current password'),
                    labelInline: true,
                    icon: 'key',
                    type: 'password',
                    name: 'current_password',
                    required: true,
                    placeHolder: t('Password') + '...'
                }
            }, {
                xtype: 'select',
                ref: 'interested',
                config: {
                    label: t('interested'),
                    labelInline: true,
                    emptyText: t('interested'),
                    value: '',
                    options: [{
                        label: '',
                        value: '',
                        disabled: true
                    }, {
                        label: t('zypx'),
                        value: t('zypx')
                    }, {
                        label: t('zyzl'),
                        value: t('zyzl')
                    }]
                },
                listeners: {
                    scope: this,
                    change: function(select, opt) {
                        this.el.querySelector('input[name="interested"]').value = opt.value;
                    }
                }
            }, '<input type="hidden" name="interested" id="interestedfield" value="" />', {
                xtype: 'button',
                config: {
                    text: t('Update'),
                    ui: 'd',
                    inline: false
                },
                scope: this,
                handler: _updateProfile
            }];

            var items = [{
                xtype: 'header',
                config: {
                    ui: 'f',
                    title: t('My profile')
                },
                items: [{
                    xtype: 'button',
                    config: {
                        ui: 'f',
                        size: 'large',
                        cssClass: 'ui-btn-left simple-btn',
                        icon: '&#59233;',
                        route: '/more/menu'
                    }
                }]
            }, {
                xtype: 'abstract',
                config: {
                    style: 'padding-bottom: 51px;'
                },
                ref: 'content',
                xtpl: 'content',
                items: profile
            }];

            _parent.prototype.constructor.call(self, {
                config: opts.config,
                items: items,
                listeners: {
                    scope: this,
                    afterrender: _onAfterRender
                }
            });
        }
    });

}());