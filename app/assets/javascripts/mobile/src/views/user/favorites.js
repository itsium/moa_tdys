/* User favorites page */

App.views.Userfavorites = (function() {

    // handlebars helpers are in news/search.js

    var _needReload,
        _fs = Fs,
        _settings = App.Settings,
        _parent = _fs.views.Container,
        _contentUrl = App.Settings.gets('serverUrl', 'userFavorites').join(''),
        _emptyTpl = [
            '<div class="news-listing-empty">',
                '<span class="icon">&#9733;</span>',
                '<span class="message">',
                    t('No favorites.'),
                '</span>',
            '</div>'
        ].join('');

    var _onShow = function() {
        if (_needReload === true) {
            this.newsList.reload();
            _needReload = false;
        }
    };

    var _onAfterRender = function() {

        function forceReload() {
            _needReload = true;
        }

        _settings.on('currentUserchanged', forceReload);
        _settings.on('newsupdatedchanged', forceReload);
    };

    return _parent.subclass({
        constructor: function(opts) {

            this.newsStore = new _fs.data.PagingStore({
                enableHashTable: true,
                recordPerPage: 10,
                reader: {
                    rootProperty: 'data',
                    successProperty: 'success',
                    totalProperty: 'total'
                },
                proxy: {
                    type: 'jsonp',
                    cache: false,
                    url: _contentUrl + '?callback={callback}'
                }
            });
            this.newsStore.on('loaderror', App.errors.onStoreError);

            opts.config.style = 'padding-bottom: 52px;';
            opts = {
                config: opts.config,
                items: [{
                    xtype: 'header',
                    config: {
                        ui: 'f',
                        title: t('Favorites')
                    },
                    items: [{
                        xtype: 'button',
                        config: {
                            ui: 'f',
                            size: 'large',
                            cssClass: 'ui-btn-right simple-btn-medium',
                            icon: '&#10227;'
                        },
                        scope: this,
                        handler: function() {
                            this.newsList.reload();
                        }
                    }]
                }, {
                    xtype: 'listbuffered',
                    config: {
                        cssClass: 'newslist scroll',
                        ui: 'w'
                    },
                    ref: 'newsList',
                    store: this.newsStore,
                    itemTpl: _fs.Templates.get('userfavoriteslist'),
                    emptyTpl: _emptyTpl,
                    listeners: {
                        scope: this,
                        itemselect: function(e) {
                            var recordId,
                                item = this.newsList.getItemFromEvent(e);

                            recordId = item.getAttribute('data-id');
                            _fs.History.navigate('/news/detail/' + recordId +
                                '/?back=' + encodeURIComponent('/user/favorites'));
                        }
                    }
                }]
            };
            _parent.prototype.constructor.call(this, opts);

            this.on('show', _onShow, this);
            this.on('afterrender', _onAfterRender, this);
        }
    });

}());