(function() {

    Fs.Templates.setPath('/assets/mobile/templates/');

    var _getTemplates = function() {

        var i, item, templates = [], conf = App.conf;

        for (i in conf) {
            item = conf[i];

            if (item.enabled === true &&
                item.templates) {
                templates = templates.concat(item.templates);
            }
        }

        return templates;

    };

    var app = new Fs.App({
        require: {
            templates: [
                'moremenu',
                'newsdetail',
                'newsdetaildate',
                'newsdetailmap',
                'newssearchcategorylist',
                'newssearchlist'
            ].concat(_getTemplates())
        },
        defaultRoute: '',
        engine: 'JqueryMobile',
        ui: 'c',
        debug: false,
        onready: function() {

    	   var main = new App.controllers.Main();

            if (window.canAddToHome()) {
                var addtohome = new Fs.views.AddToHome({
                    renderTo: 'body',
                    timeout: (60 * 60 * 24 * 7), // one week timeout
                    config: {
                        image: '/assets/icons/Icon-72@2x.png'
                    },
                    items: '先点击<div class="icon addtohome-content-icon">&#59157;</div><br>再"添加到主屏幕"'
                });
            }
        }
    });

}());