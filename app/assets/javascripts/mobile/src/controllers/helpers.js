/* helpers */

App.helpers = (function() {

    var _fs = Fs,
        _loadmaskHidden = true,
        _loadmask;

    var _getLoadmask = function() {
        if (!_loadmask) {
            _loadmask = new _fs.views.LoadMask({
                config: {
                    fullscreen: true
                }
            });
            _loadmask.compile();
            _loadmask.render('body', 'afterbegin');
        }
        return _loadmask;
    };

    var _showLoading = function() {
        _loadmaskHidden = false;
        _getLoadmask().show();
    };

    var _hideLoading = function() {
        _loadmaskHidden = true;
        _getLoadmask().hide();
    };

    var _isLoading = function() {
        return !_loadmaskHidden;
    };

    var _setDisabledAll = function(value, fields, obj) {
        var i = fields.length;

        while (i--) {
            obj[fields[i]].setDisabled(value);
        }
    };

    // <form>
    var _fieldsToJson = function(selector, root) {
        var field,
            result = {},
            fields = _fs.Selector.gets(selector, root),
            i = fields.length;

        while (i--) {
            field = fields[i];
            if (field.getAttribute('required') !== null &&
                !field.value.length) {
                return false;
            } else if (field.validity.valid === false) {
                return false;
            }
            result[field.getAttribute('name')] = field.value;
        }
        return result;
    };

    var _jsonToFields = function(json, fields) {
        if (json) {
            var field, name, i = fields.length;

            while (i--) {
                field = fields[i];
                name = field.getAttribute('name');
                if (typeof json[name] !== 'undefined') {
                    field.value = json[name];
                } else {
                    field.value = '';
                }
            }
        }
    };

    var _replaceByValues = function(str, fields, encode) {
        var i;

        for (i in fields) {
            var value = fields[i];

            if (typeof encode === 'function') {
                value = encode(value);
            }
            str = str.replace('{' + i + '}', value);
        }
        return str;
    };
    // </form>

    return {
        fieldsToJson: _fieldsToJson,
        jsonToFields: _jsonToFields,
        replaceByValues: _replaceByValues,
        setDisabledAll: _setDisabledAll,
        showLoading: _showLoading,
        hideLoading: _hideLoading,
        isLoading: _isLoading,
        lang: new _fs.helpers.i18n()
    };
})();