Fs.views.Pull = (function() {

    var _fs = Fs,
        _parent = _fs.views.Template;

    document.addEventListener('DOMContentLoaded', function() {

        var style = document.createElement('style');

        style.innerHTML = [
            '.pull {',
                //'position: absolute;',
                'top: -10px;',
                'height: 0px;',
                'text-align: center;',
                'line-height: 60px;',
                'opacity: 0.3;',
                '-webkit-transition: height 600ms easeInElastic;',
                'transition: height 600ms easeInElastic;',
                'overflow: hidden;',
            '}'
        ].join('');

        style.id = 'imagezoom-style';
        document.head.appendChild(style);

        document.removeEventListener('DOMContentLoaded', arguments.callee, false);

    }, false);

    var _tpl = function() {

        return ([

            '<div id="pull" class="pull">',
                'Pull to refresh',
            '</div>'

        ].join(''));

    };

    var _onafterrender = function() {

        var win = window;

        this.el.parentNode.addEventListener('touchmove', function(e) {

            if (win.scrollY === 0) {
                console.log('move: ', e, new Date());
            }

        }, false);

    };

    var _onaftercompile = function() {

        this.off(this.eid);
        this.renderer.on('afterrender', _onafterrender,
            this, this.priority.VIEWS);

    };

    return _parent.subclass({

        xtype: 'pull',
        className: 'Fs.views.Pull',

        constructor: function(opts) {
            opts.template = _tpl;
            opts.config = {
                id: 'pull'
            };

            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onaftercompile,
                    this, this.priority.VIEWS);
        }

    });

})();