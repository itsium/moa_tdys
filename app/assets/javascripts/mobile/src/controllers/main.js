/* Main controller */

App.controllers.Main = (function() {
    // private
    var _layoutPage,
        _mainMenu,
        _activeTab,
        _lastPage,
        _defaultScrollY = (navigator.userAgent.match(/Android/i) ? 1 : 0),
        _pages = {},
        _fs = Fs,
        _history = _fs.History,
        _parent = _fs.Router,
        _app = App,
        _appViews = _app.views,
        _settings = _app.Settings,
        _selector = _fs.Selector,
        _scrollPos = {},
        _activeBtnClass = 'ui-btn-active';


    var _changeActiveTab = function(newTab) {
        var ntab = document.getElementById(newTab);

        if (!ntab) {
            return;
        }
        var activeTabs = _selector.gets('.' + _activeBtnClass,
            document.getElementById('main-menu')),
            length = activeTabs.length;

        while (length--) {
            _selector.removeClass(activeTabs[length], _activeBtnClass);
        }
        _selector.addClass(ntab, _activeBtnClass);
        _activeTab = newTab;
    };

    var _switchPage = function(newPage, routeArgs) {
        var w = window,
            fromPage = _pages[_lastPage],
            toPage = _pages[newPage];

        if (_lastPage) {
            _scrollPos[_lastPage] = (w.scrollY < 1 ? _defaultScrollY : w.scrollY);
            _selector.addClass(document.getElementById(_lastPage),
                'hidden');
            if (fromPage) {
                fromPage.fire('hide', 0, _scrollPos[_lastPage]);
            }
        }
        _selector.removeClass(document.getElementById(newPage),
            'hidden');
        w.scrollTo(0, _scrollPos[newPage] || _defaultScrollY);
        _lastPage = newPage;
        if (toPage) {
            toPage.fire('show', routeArgs);
        }
    };

    var _routeGenerate = function(name, parent) {
        return function() {
            var pageId = 'page-' + name,
                p = _pages[pageId],
                tabname = 'menu-' + (parent ? parent : name) + '-btn';

            if (!p) {
                p = _pages[pageId] = new _appViews[_fs.utils.capitalize(name)]({
                    config: {
                        id: pageId,
                        hidden: true
                    }
                });
                p.compile();
                p.render('#page-footer', 'beforebegin');
            }
            _changeActiveTab(tabname);
            _switchPage(pageId, arguments);
        };
    };

    var _parseConfiguration = function() {

        var i, item, conf = App.conf;

        for (i in conf) {
            item = conf[i];

            if (item.enabled === true && item.router) {

                if (item.router.routes) {

                    var route, rconf;

                    for (route in item.router.routes) {
                        rconf = item.router.routes[route];

                        this.routes[route] = rconf[0];
                        this[rconf[0]] = _routeGenerate.apply(this, rconf);
                    }

                }

                if (item.router.before) {

                    var regexp;

                    for (regexp in item.router.before) {
                        this.before[regexp] = item.router.before[regexp];
                    }

                }

            }
        }

    };

    // public
    return _parent.subclass({

        before: {},
        routes: {
            '': 'routeNewsSearch',
            '/news/search': 'routeNewsSearch',
            '/news/detail/:id(/?back=:back)': 'routeNewsDetail',
            //'/news/comments/:id(?back=:back)': 'routeNewsDetailComments',
            '/more/menu': 'routeMoreMenu'
        },

        constructor: function() {
            if (!_layoutPage) {
                _layoutPage = new _appViews.Layout({
                    config: {
                        id: 'mainpage'
                    }
                });
                _layoutPage.compile();
                _layoutPage.render('body');
            }
            _mainMenu = document.getElementById('main-menu');

            // Generating route with activated modules
            _parseConfiguration.call(this);

            _parent.prototype.constructor.apply(this, arguments);

            _fs.History.start();
        },

        isAuthenticated: function(callback) {
            var user = _settings.get('currentUser');

            if (!user) {
                _history.navigate('/user/login/?back=' + _history.here(true));
            } else {
                callback();
            }
        },

        isAlreadyAuthenticated: function(callback) {
            var user = _settings.get('currentUser');

            if (!user) {
                callback();
            } else {
                _history.navigate('/user/profile');
            }
        },

        routeNewsSearch: _routeGenerate('newssearch', 'news'),
        routeNewsDetail: _routeGenerate('newsdetail', 'news'),
        routeMoreMenu: _routeGenerate('moremenu', 'more')
    });
}());