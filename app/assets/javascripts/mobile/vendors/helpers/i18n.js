/* i18n */

Fs.helpers.i18n = (function(gscope) {

    var _fs = Fs,
        // stores all languages available (after load)
        _langs = {},
        // stores current language
        _currentLang = {},
        // activated language
        _lang = 'en',
        _defaultScope = gscope,
        _storage = _fs.Storage,
        _handlebars = Handlebars,
        _parent = _fs;

    var _t = function(str) {
        return (_currentLang[str] || str);
    };

    if (_handlebars) {
        _handlebars.registerHelper('t', _t);
    }

    _defaultScope.t = _t;

    return _parent.subclass({
        constructor: function(opts) {
            //
        },

        setLang: function(name) {
            _lang = name;
            _currentLang = _langs[_lang];
        },

        addLang: function(name, value, setDefault) {
            _langs[name] = value;
            if (setDefault) {
                this.setLang(name);
            }
        }
    });

}(this));