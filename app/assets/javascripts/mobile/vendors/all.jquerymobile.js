

if (!Function.prototype.bind) {
    Function.prototype.bind = function(oThis) {
        if (typeof this !== 'function') {
            
            throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
        }

        var aArgs = Array.prototype.slice.call(arguments, 1),
            fToBind = this,
            fNOP = function() {},
            fBound = function() {
                return fToBind.apply(this instanceof fNOP ? this : oThis || window,
                    aArgs.concat(Array.prototype.slice.call(arguments)));
            };

        fNOP.prototype = this.prototype;
        fBound.prototype = new fNOP();

        return fBound;
    };
}


'use strict';

(function() {

    var VERSION = '1.9',
        slice = Array.prototype.slice,
        _xtypes = {};

    var _xtype = function(xtype, obj) {
        if (obj) {
            _xtypes[xtype] = obj;
        } else {
            return (_xtypes[xtype] ||
                console.warn('[core] xtype "' + xtype +
                    '" does not exists.'));
        }
    };

    var _createSubclass = function(props) {
        props = props || {};

        var key,
            realConstructor,
            superclass = props.superclass.prototype;

        if (props.hasOwnProperty('constructor')) {
            realConstructor = props.constructor;
        } else if (typeof superclass.constructor === 'function') {
            realConstructor = superclass.constructor;
        } else {
            realConstructor = function() {};
        }

        function constructor() {
            if (!(this instanceof constructor)) {
                throw new Error('[core] Please use "new" when initializing Fs classes');
            }
            realConstructor.apply(this, arguments);
        }

        constructor.prototype = Object.create(superclass);
        constructor.prototype.constructor = constructor;

        _extend(constructor, {

            parent: superclass,

            subclass: function(obj) {
                var sclass;

                obj = obj || {};
                obj.superclass = this;
                sclass = _createSubclass(obj);

                if (obj.xtype) {
                    _xtype(obj.xtype, sclass);
                }
                return sclass;
            }
        });

        for (key in props) {
            if (key !== 'constructor' &&
                key !== 'superclass') {
                constructor.prototype[key] = props[key];
            }
        }

        return constructor;
    };

    var _extend = function() {
        var i = -1,
            args = slice.call(arguments),
            l = args.length,
            object = args.shift();

        while (++i < l) {
            var key,
                props = args[i];

            for (key in props) {
                object[key] = props[key];
            }
        }
        return object;
    };

    var Fs = {
        views: {},
        events: {},
        data: {},
        engines: {},
        config: {},
        helpers: {},
        xtype: _xtype,
        version: VERSION,

        subclass: function(obj) {
            var sclass;

            obj = obj || {};
            obj.superclass = function() {};
            sclass = _createSubclass(obj);

            if (obj.xtype) {
                _xtype(obj.xtype, sclass);
            }
            return sclass;
        }
    };

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' &&
            module.exports) {
            exports = module.exports = Fs;
        }
        exports.Fs = Fs;
    } else if (typeof define === 'function' &&
        define.amd) {
        define(function() {
            return Fs;
        });
    } else {
        window.Fs = Fs;
    }

})();


Fs.debug = new (function() {

    var _profiles = {},
        _debug = false,
        _whitelist = [];

    var isWhitelist = function(prefix) {
        if (_whitelist.length &&
            _whitelist.indexOf(prefix) === -1) {
            return false;
        }
        return true;
    };

    return Fs.subclass({

        profile: function(prefix, key) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            key = '[' + prefix + '] ' + key;
            console.time(key);
            
            return this;
        },

        log: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.log.apply(console, args);
            return this;
        },

        error: function(prefix) {
            if (!_debug) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.error.apply(console, args);
            return this;
        },

        warn: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            var i = 0,
                length = arguments.length,
                args = ['[' + prefix + ']'];
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.warn.apply(console, args);
            return this;
        },

        profileEnd: function(prefix, key) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            key = '[' + prefix + '] ' + key;
            console.timeEnd(key);
            
            
            
            return this;
        },

        memory: function(prefix) {
            if (!_debug || !isWhitelist(prefix)) {
                return this;
            }
            if (window.performance && window.performance.memory) {
                return ((window.performance.memory.totalJSHeapSize / 1024.0) + 'ko');
            }
            return 0;
        },

        set: function(debug) {
            _debug = (debug === true);
        }
    });

}())();


Fs.utils = (function() {

    var _table = '00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D';

    return {
        
        applyIf: function(obj, base, keys) {
            var i = keys.length, key;

            while (i--) {
                key = keys[i];
                if (typeof obj[key] === 'undefined' &&
                    typeof base[key] !== 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },

        applyIfAuto: function(obj, base) {
            var key;

            for (key in base) {
                if (typeof obj[key] === 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },

        apply: function(obj, base, keys) {
            var i = keys.length, key;

            while (i--) {
                key = keys[i];
                if (typeof base[key] !== 'undefined') {
                    obj[key] = base[key];
                }
            }
            return obj;
        },

        applyAuto: function(obj1, obj2) {
            var obj3 = {};

            for (var attrname in obj1) {
                obj3[attrname] = obj1[attrname];
            }
            for (var attrname in obj2) {
                obj3[attrname] = obj2[attrname];
            }
            return obj3;
        },

        arraySlice: function(array, from) {
            var result = [],
                i = from - 1,
                length = array.length;

            while (++i < length) {
                result.push(array[i]);
            }
            return result;
        },

        capitalize: function(str) {
            return (str.charAt(0).toUpperCase() + str.slice(1));
        },

        crc32: function(str, crc) {
            if (crc == window.undefined) {
                crc = 0;
            }

            var i = 0,
                iTop = str.length,
                n = 0, 
                x = 0; 

            crc = crc ^ (-1);
            for (; i < iTop; i++ ) {
                n = ( crc ^ str.charCodeAt(i)) & 0xFF;
                x = '0x' + _table.substr(n * 9, 8);
                crc = (crc >>> 8) ^ x;
            }
            return crc ^ (-1);
        }
    };

}());

Fs.Storage = new (function() {

    var _store = localStorage,
        _prefix = '';

    var _setPrefix = function(prefix) {
        if (typeof prefix === 'string') {
            _prefix = prefix;
            return true;
        }
        return false;
    };

    var _getPrefix = function(prefix) {
        if (typeof prefix === 'string') {
            return prefix;
        }
        return _prefix;
    };

    var _setItem = function(name, value, prefix) {
        return (_store.setItem(_getPrefix(prefix) + name,
            JSON.stringify(value)));
    };

    var _getItem = function(name, prefix) {
        return JSON.parse(_store.getItem(_getPrefix(prefix) + name));
    };

    var _removeItem = function(name, prefix) {
        return _store.removeItem(_getPrefix(prefix) + name);
    };

    var _empty = function(prefix) {
        prefix = _getPrefix(prefix);

        for (var name in _store) {
            if (!prefix || !name.indexOf(prefix)) {
                _removeItem(name, '');
            }
        }
    };

    return Fs.subclass({

        setPrefix: _setPrefix,
        getPrefix: _getPrefix,
        setItem: _setItem,
        getItem: _getItem,
        removeItem: _removeItem,
        empty: _empty

    });

}())();

Fs.Selector = new (function() {
    
    var _scope,
        _elUid = 0,
        _win = window;

    
    return Fs.subclass({
        constructor: function(gscope) {
            _scope = gscope;
        },

        generateId: function(prefix) {
            ++_elUid;
            prefix = prefix || 'fs';
            return (prefix + _elUid);
        },

        get: function(selector, root) {
            root = root || _scope;
            return root.querySelector(selector);
        },

        gets: function(selector, root) {
            root = root || _scope;
            return root.querySelectorAll(selector);
        },

        hasClass: function(el, cls) {
            var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
            
            if (!el || typeof el !== 'object') {
                console.warn('EL is not good', el, arguments.callee.caller.caller);
                return false;
            }
            
            return (el.className.match(reg) !== null);
        },

        addClass: function(el, cls) {
            if (!this.hasClass(el, cls)) {
                
                el.className += ' ' + cls;
                el.className = el.className.replace(/^\s+|\s+$/g, '');
                return true;
            }
            return false;
        },

        removeClass: function(el, cls) {
            if (this.hasClass(el, cls)) {
                
                var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
                el.className = el.className.replace(reg, ' ').replace(/^\s+|\s+$/g, '');
                return true;
            }
            return false;
        },

        redraw: function(el) {
            el.style.display = 'none';
            el.offsetHeight;
            el.style.display = 'block';
        },

        removeEl: function(el) {
            if (typeof el === 'string') {
                el = this.get(el);
            }
            el.parentNode.removeChild(el);
        },

        removeHtml: function(el) {
            
            while (el.firstChild) {
                el.removeChild(el.firstChild);
            }
        },

        addHtml: function(el, html) {
            el.insertAdjacentHTML('beforeEnd', html);
        },

        updateHtml: function(el, html) {
            this.removeHtml(el);
            this.addHtml(el, html);
        },

        scrollToEl: function(el, offsetX, offsetY) {
            offsetX = offsetX || 0;
            offsetY = offsetY || 0;
            _win.scrollTo(el.offsetLeft + offsetX,
                el.offsetTop + offsetY);
        }
    });
}())(document);


Fs.Request = new (function() {
    
    var jsonp_id = 0,
        _debug = Fs.debug;

    var _jsonToString = function(json, prefix) {
        var key,
            val,
            result = '',
            first = true;

        for (key in json) {
            val = json[key];
            if (first === false) {
                result += '&';
            } else {
                first = false;
            }
            result += key + '=' + encodeURIComponent(val);
        }
        _debug.log('request', 'jsonToString result', result);
        return (prefix ? prefix : '') + result;
    };

    
    return Fs.subclass({
        
        ajax: function(url, options) {
            var request,
            callback = (typeof options.callback === 'function' ?
                options.callback.bind(options.scope || window) : null);
            options.method = options.method || 'GET';
            options.data = options.data || '';

            if (options.jsonParams) {
                url += _jsonToString(options.jsonParams,
                    (url.indexOf('?') === -1 ? '?' : '&'));
            }
            if (options.params) {
                url += (url.indexOf('?') === -1 ? '?': '&') + options.params;
            }

            function stateChange() {
                if (request.readyState === 4 &&
                    callback) {
                    callback((request.status === 200 ||
                        request.status === 304) ? true : false,
                        request.responseText);
                }
            }

            request = new XMLHttpRequest();
            request.onreadystatechange = stateChange;
            request.open(options.method, url, true);
            if (options.method !== 'GET' && options.data) {
                request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                request.setRequestHeader('Content-type',
                    'application/x-www-form-urlencoded');
                request.setRequestHeader('Connection', 'close');
            }
            request.send(options.data);
        },

        
        jsonp: function(url, options) {
            var callback_name = ['jsonp', ++jsonp_id].join(''),
                callback = options.callback.bind(options.scope || window),
                script = document.createElement('script'),
                clean = function() {
                    document.body.removeChild(script);
                    delete window[callback_name];
                },
                error = function() {
                    _debug.warn('request', 'JSONP request error', url, options, arguments);
                    clean();
                    if (callback) {
                        callback(false);
                    }
                },
                success = function(data) {
                    clean();
                    if (callback) {
                        callback(true, data);
                    }
                };

            if (options.jsonParams) {
                url += _jsonToString(options.jsonParams,
                    (url.indexOf('?') === -1 ? '?' : '&'));
            }
            if (options.params) {
                url += (url.indexOf('?') === -1 ? '?': '&') + options.params;
            }

            window[callback_name] = success;
            script.onerror = error;
            script.src = url.replace('{callback}', callback_name);
            document.body.appendChild(script);
        },

        memory: function(data, options, store) {
            var i, length, callback,
                result = {},
                idProp = store.getReaderId(),
                totalProp = store.getReaderTotal(),
                successProp = store.getReaderSuccess(),
                rootProp = store.getReaderRoot();

            callback = options.callback.bind(options.scope || window);
            if (!data) {
                data = store.config.data || [];
            }
            length = data.length;
            if (data[0] && typeof data[0][idProp] === 'undefined') {
                for (i = 0; i < length; ) {
                    data[i][idProp] = ++i;
                }
            }
            result[totalProp] = length;
            result[successProp] = true;
            result[rootProp] = data;
            if (callback) {
                callback(true, result);
            }
        }
    });
}())();


Fs.History = new (function() {

    
    var _fs = Fs,
        _win = window,
        scope = _win,
        _storage = _fs.Storage,
        _isStandalone = (_win.navigator && _win.navigator.standalone === true),
        curhash = _win.location.hash.substr(1),
        routes = [];

    function runCallbacks(location) {
        
        if (_isStandalone) {
            _storage.setItem('location', location, _win.location.host);
        }

        var regexp,
            route,
            i = -1,
            length = routes.length;

        this.curhash = location;
        while (++i < length) {
            route = routes[i];
            if (route.regexp.test(location)) {
                route.callback(location);
            }
        }
        return true;
    };

    function onHashChange(event) {
        runCallbacks.call(this, _win.location.hash.substr(1));
    };

    
    return _fs.subclass({
        constructor: function(gscope) {
            this.defaultRoute = '';
            scope = gscope || _win;
            onHashChange = onHashChange.bind(this);
            scope.addEventListener('hashchange', onHashChange);
        },

        setDefaultRoute: function(path) {
            this.defaultRoute = path;
        },

        start: function() {
            var hash = curhash;
            curhash = '';

            
            if (_isStandalone) {
                hash = _storage.getItem('location', _win.location.host) || this.defaultRoute;
            }
            runCallbacks.call(this, hash);
        },

        stop: function() {
            scope.removeEventListener('hashchange', onHashChange);
        },

        here: function(encoded) {
            var hash = _win.location.hash.substr(1);

            return (encoded ? encodeURIComponent(hash) : hash);
        },

        navigate: function(location) {
            _win.location.hash = '#' + location;
        },

        route: function(route, callback) {
            routes.push({
                regexp: route,
                callback: callback
            });
        }
    });
}())(window);


Fs.Router = (function() {

    
    var optionalParam = /\((.*?)\)/g,
        namedParam = /(\(\?)?:\w+/g,
        splatParam = /\*\w+/g,
        escapeRegExp = /[\-{}\[\]+?.,\\\^$|#\s]/g,

        extractParameters = function(route, fragment) {
            return route.exec(fragment).slice(1);
        },

        routeToRegExp = function(route) {
            route = route.replace(escapeRegExp, '\\$&')
                .replace(optionalParam, '(?:$1)?')
                .replace(namedParam, function(match, optional) {
                    return optional ? match : '([^\/]+)';
                })
                .replace(splatParam, '(.*?)');
            return new RegExp('^' + route + '$');
        },

        _prepareBefore = function() {
            var self = this;

            if (self.before) {
                var pattern;

                self.beforeRegexp = {};
                for (pattern in self.before) {
                    
                    self.beforeRegexp[pattern] = new RegExp(pattern);
                }
            }
        },

        _getBefore = function(funcname) {
            var self = this;

            if (self.before) {
                var pattern;

                for (pattern in self.before) {
                    if (self.beforeRegexp[pattern].test(funcname) === true) {
                    
                        var func = self[self.before[pattern]];

                        return (func ? func : false);
                    }
                }
            }
            return false;
        };

    
    return Fs.subclass({
        constructor: function() {
            var self = this;

            _prepareBefore.call(self);
            if (self.routes) {
                var pattern;

                for (pattern in self.routes) {
                    self.route(pattern, self.routes[pattern]);
                }
            }
        },

        route: function(path, foo) {

            var before,
                self = this,
                callback = self[foo],
                history = Fs.History;

            if (callback) {
                path = routeToRegExp(path);
                before = _getBefore.call(self, foo);
                var cb = function(fragment) {
                    var args = extractParameters(path, fragment);
                    callback.apply(self, args);
                };
                if (before) {
                    history.route(path, function(fragment) {
                        before(function() {
                            return cb(fragment);
                        });
                    });
                } else {
                    history.route(path, cb);
                }
            }

            return self;
        }
    });
}());



Fs.Event = (function() {

    var _fs = Fs,
        _debug = _fs.debug,
        _utils = _fs.utils,
        _parent = _fs;

    function setupEvents() {
        _debug.log('event', 'setup');
        if (typeof this.events !== 'object') {
            
            
            this.events = {};
            this.uids = 0;
            
            
            this.pqueue = {};
            
            
            this.equeue = {};
        }
    };

    function _fire(name) {
        _debug.log('event', 'fire: ', this, arguments, this.events);
        var p = this.pqueue[name];

        if (p) {
            

            p = this.pqueue[name].slice(0);

            var e, events, elen, j, i = -1, plen = p.length,
                args = _utils.arraySlice(arguments, 1);

            while (++i < plen) {
                events = this.events[name]['p' + p[i]].slice(0);
                elen = events.length;
                j = -1;

                while (++j < elen) {
                    var e = this.equeue['u' + events[j]];

                    if (typeof e[2] !== 'function') {
                        console.warn('fire "', name, '" not a function: ',
                            arguments.callee.callee, e);
                    } else if (e[2].apply(e[3], args) === false) {
                        return false;
                    }
                }

            }
            return true;
        }
    };

    function _numSort(a, b) {
        return (b - a);
    };

    function _on(name, cb, scope, priority) {
        _debug.log('event', 'on: ', this, arguments, this.events);

        priority = priority || this.defaultPriority;
        if (typeof this.events[name] === 'undefined') {
            this.events[name] = {};
        }
        if (typeof this.events[name]['p' + priority] === 'undefined') {
            this.events[name]['p' + priority] = [];
        }
        if (typeof this.pqueue[name] === 'undefined') {
            this.pqueue[name] = [];
        }
        if (this.pqueue[name].indexOf(priority) === -1) {
            this.pqueue[name].push(priority);
            this.pqueue[name].sort(_numSort);
        }
        scope = scope || window;
        this.equeue['u' + (++this.uids)] = [name, priority, cb, scope];
        this.events[name]['p' + priority].push(this.uids);
        return this.uids;
    };

    function _countListener(name) {
        var e = this.events[name];

        if (e) {
            var total = 0,
                keys = Object.keys(this.events[name]),
                i = keys.length;

            while (i--) {
                total += e[keys[i]].length;
            }
            return total;
        }
        return 0;
    };

    function _off(uid) {
        _debug.log('event', 'off: ', arguments, uid, this.equeue['u' + uid]);
        var e = this.equeue['u' + uid];

        if (e) {
            var cb,
                length,
                name = e[0],
                priority = e[1],
                listeners = this.events[name]['p' + priority];

            listeners.splice(listeners.indexOf(uid), 1);
            length = listeners.length;

            delete this.equeue['u' + uid];

            if (!length) {
                
                delete this.events[name]['p' + priority];
                this.pqueue[name].splice(this.pqueue[name].indexOf(priority), 1);
                if (!this.pqueue[name].length) {
                    delete this.pqueue[name];
                    delete this.events[name];
                    return 0;
                }
            }
            return _countListener.call(this, name);
        }
        return false;
    };

    function _off_DEPRECATED(TOREMOVE, uid) {
        return _off.call(this, uid);
    };

    return _parent.subclass({

        
        priority: {
            CORE: 1000,
            VIEWS: 900,
            DEFAULT: 800
        },
        defaultPriority: 800,

        constructor: function(opts) {
            
            if (opts && opts.listeners) {
                this.listeners = opts.listeners;
                this.wasListened = {};
            }
            setupEvents.call(this);
            
        },

        setupListeners: function(eventNames, renderer) {
            if (this.listeners) {

                var name,
                    scope = this.listeners.scope || this,
                    i = eventNames.length;

                renderer = renderer || this;

                while (i--) {
                    name = eventNames[i];
                    if (this.listeners[name] &&
                        !this.wasListened[name]) {
                        this.wasListened[name] = true;
                        renderer.on(name, this.listeners[name], scope);
                    }
                }
            }
        },

        hasListener: function(name) {
            if (typeof this.listeners === 'object' &&
                typeof this.listeners[name] === 'function') {
                return true;
            }
            return false;
        },

        getListener: function(name) {
            return this.listeners[name];
        },

        fire: _fire,
        fireEvent: _fire, 

        on: _on,
        listenEvent: _on, 

        off: _off,
        removeListener: _off_DEPRECATED 
    });

}());


Fs.Templates = new (function() {
    
    var _fs = Fs,
        _handlebars = Handlebars,
        _request = _fs.Request;

    _handlebars.registerHelper('safe', function(str) {
        return new _handlebars.SafeString(str);
    });

    _handlebars.registerHelper('length', function(array) {
        return array.length;
    });

    _handlebars.registerHelper('geticon', function(icon) {
        return icon;
    });

    _handlebars.registerHelper('wordwrap', function(str, len) {
        if (str.length > len) {
            return str.substring(0, len) + '...';
        }
        return str;
    });

    _handlebars.registerHelper('substr', function(str, len) {
        if (str.length <= len) {
            return str;
        }
        return str.substring(0, len);
    });

    _handlebars.registerHelper('foreach', function(arr, options) {
        var length = arr.length;

        if (options.inverse && !length) {
            return options.inverse(this);
        }
        return arr.map(function(item, index) {
            if (typeof item !== 'object') {
                item = {
                    original: item
                };
            }
            item.$index = index;
            item.$first = index === 0;
            item.$last  = index === (length - 1);
            return options.fn(item);
        }).join('');
    });

    
    _handlebars.registerHelper('if_eq', function(context, options) {
        if (typeof context === 'undefined') {
            context = options.hash['default'];
        }
        if (context == options.hash.compare) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    
    _handlebars.registerHelper('if_neq', function(context, options) {
        if (typeof context === 'undefined') {
            context = options.hash['default'];
        }
        if (context != options.hash.compare) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    
    _handlebars.registerHelper('if_gteq', function(context, options) {
        if (typeof context === 'undefined') {
            context = options.hash['default'];
        }
        if (context >= options.hash.compare) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    
    _handlebars.registerHelper('get', function(context, options) {
        var hash = options.hash;
        if (typeof context === 'undefined' ||
            (hash.choices &&
            hash.choices.indexOf(context) === -1)) {
            return hash['default'];
        }
        return context;
    });

    function loadtpl(name, cb) {
        if (_handlebars.templates[name]) {
            cb(_handlebars.templates[name]);
        } else {

            var url = this.path + name + '.handlebars';

            _request.ajax(url, {
                scope: this,
                callback: function(success, data) {
                    if (success) {
                        _handlebars.templates[name] = this.compile(data);
                        return cb(_handlebars.templates[name]);
                    }
                    console.warn('Error while loading template "' + url + '".');
                    return cb(false);
                }
            });
        }
    };

    function loadtpls(names, cb) {

        var results = {},
            i = names.length,
            remaining = names.length;

        while (i--) {
            loadtpl.call(this, names[i], (function(tplname) {

                return function(tpl) {
                    results[tplname] = tpl;
                    if (!(--remaining)) {
                        cb(results);
                    }
                };

            }(names[i])));
        }
    };

    
    return _fs.subclass({
        
        constructor: function() {
            this.path = '';
        },

        setPath: function(path) {
            this.path = path;
        },

        get: function(name) {
            return _handlebars.templates[name];
        },

        load: function(names, cb) {
            if (typeof names === 'object') {
                return loadtpls.apply(this, arguments);
            }
            return loadtpl.apply(this, arguments);
        },

        compile: function(source) {
            if (typeof source === 'function') {
                return source;
            } else if (typeof _handlebars.templates[source] === 'function') {
                return _handlebars.templates[source];
            } else if (_handlebars.compile) {
                return _handlebars.compile(source);
            }
            return function() { return source };
            
        }
    });
}())();


Fs.Settings = (function() {

    var _fs = Fs,
        _parent = _fs.Event;

    return _parent.subclass({

        constructor: function(config) {
            this.vars = config || {};
            _parent.prototype.constructor.apply(this, arguments);
        },

        set: function(name, value) {
            var oldValue = this.get(name);
            this.vars[name] = value;
            this.fire('settingchanged', this, name, value, oldValue);
            this.fire(name + 'changed', this, name, value, oldValue);
        },

        get: function(name, defaultValue) {
            if (this.vars[name]) {
                return this.vars[name];
            }
            return defaultValue;
        },

        gets: function() {
            var result = [],
                i = -1,
                length = arguments.length;

            while (++i < length) {
                result.push(this.get(arguments[i]));
            }
            return result;
        },

        empty: function(name) {
            if (typeof this.vars[name] !== 'undefined') {
                delete this.vars[name];
                this.fire(name + 'empty', this, name);
            }
        }
    });

}());

Fs.events.Abstract = (function() {

    var _fs = Fs,
        _debug = _fs.debug,
        _eventsAttached = {},
        _events = {},
        _noHandler = function() {
            _debug.warn(this.xtype, 'No handler is define !');
        };

    var _globalForwardHandler = function(name) {
        return function(e) {
            var handler = _events[name][e.target.id];
            if (handler) {
                handler(e);
            }
        };
    };

    var _globalAttach = function() {
        if (!_eventsAttached[this.eventName]) {
            _eventsAttached[this.eventName] = true;
            this.globalFwd.addEventListener(this.eventName,
                _globalForwardHandler(this.eventName), this.useCapture);
        }
        if (!this.el.id || !this.el.id.length) {
            this.el.id = _fs.Selector.generateId();
        }
        if (!_events[this.eventName]) {
            _events[this.eventName] = {};
        }
        _events[this.eventName][this.el.id] = this.handler;
    };

    var _globalDetach = function() {
        delete _events[this.eventName][this.el.id];
        if (_eventsAttached[this.eventName] &&
            !_events[this.eventName].length) {
            _eventsAttached[this.eventName] = false;
            this.globalFwd.removeEventListener(this.eventName,
                _globalForwardHandler, this.useCapture);
        }
    };

    return _fs.subclass({

        xtype: 'events.abstract',
        useCapture: false,
        autoAttach: false,
        
        eventName: false,
        defaultScope: false,
        defaultHandler: false,
        defaultEl: false,
        
        globalFwd: false,

        constructor: function(opts) {
            opts = opts || {};
            _fs.utils.apply(this, opts, [
                'useCapture',
                'autoAttach',
                'globalFwd'
            ]);
            if (!this.eventName && opts.eventName) {
                this.eventName = opts.eventName;
            }
            this.defaultEl = opts.el || window;
            this.defaultScope = opts.scope || this;
            this.defaultHandler = (opts.handler ?
                opts.handler.bind(this.defaultScope) : _noHandler);
            this.attached = false;
            if (this.autoAttach) {
                this.attach();
            }
        },

        attach: function(cmp, handler, scope) {
            if (this.attached === true) {
                return false;
            }
            this.attached = true;
            this.el = cmp || this.defaultEl;
            scope = scope || this.defaultScope;
            this.handler = (handler ?
                handler.bind(scope) : this.defaultHandler);
            if (this.eventName) {
                if (this.globalFwd) {
                    _globalAttach.call(this);
                } else {
                    this.el.addEventListener(this.eventName,
                        this.handler, this.useCapture);
                }
            }
        },

        detach: function(cmp) {
            if (this.attached === false) {
                return false;
            }
            this.attached = false;
            this.el = cmp || this.el || this.defaultEl;
            this.handler = this.handler || this.defaultHandler;
            if (this.eventName) {
                if (this.globalFwd) {
                    _globalDetach.call(this);
                } else {
                    this.el.removeEventListener(this.eventName,
                        this.handler, this.useCapture);
                }
            }
        }
    });

})();

Fs.events.Click = (function() {

    
    var _fs = Fs,
        _fastclick = FastClick,
        _debug = _fs.debug,
        _isMobile = ('ontouchstart' in document.documentElement),
        _noHandler = function() {
            _debug.warn('click', 'No handler is define !');
        };

    window.addEventListener('load', function() {
        _fastclick.attach(document.body);
    }, false);

    function stopScroll(e) {
        e.preventDefault();
    };

    
    return _fs.subclass({

        xtype: 'events.click',
        defaultDisableScrolling: false,
        defaultUseCapture: false,
        autoAttach: false,
        
        defaultScope: false,
        defaultHandler: false,
        defaultEl: false,

        constructor: function(opts) {
            opts = opts || {};
            this.disableScrolling = opts.disableScrolling || this.defaultDisableScrolling;
            this.useCapture = opts.useCapture || this.defaultUseCapture;
            _fs.utils.apply(this, opts, [
                'autoAttach'
            ]);
            this.interval = opts.interval || this.defaultInterval;
            this.defaultEl = opts.el || window;
            this.defaultScope = opts.scope || this;
            this.defaultHandler = (opts.handler ?
                opts.handler.bind(this.defaultScope) : _noHandler);

            this.attached = false;
            if (this.autoAttach) {
                this.attach();
            }
        },

        attach: function(cmp, handler, scope) {

            if (this.attached === true) {
                return false;
            }

            this.attached = true;
            this.el = cmp || this.defaultEl;
            scope = scope || this.defaultScope;
            this.handler = (handler ?
                handler.bind(scope) : this.defaultHandler);

            this.el.addEventListener('click', this.handler, this.useCapture);
            if (this.disableScrolling) {
                this.el.addEventListener('touchmove', stopScroll, this.useCapture);
            }
        },

        detach: function(cmp) {

            if (this.attached === false) {
                return false;
            }

            this.attached = false;
            this.el = cmp || this.el || this.defaultEl;
            this.handler = this.handler || this.defaultHandler;

            this.el.removeEventListener('click', this.handler, this.useCapture);
            if (this.disableScrolling) {
                this.el.removeEventListener('touchmove', stopScroll, this.useCapture);
            }
        }
    });
}());


Fs.events.Scroll = (function() {

    
    var _lastEvent,
        _fs = Fs,
        _debug = _fs.debug,
        _window = window,
        _doc = document,
        _screenHeight = _doc.height,
        _eventsAttached = false,
        _scrolled = false,
        _events = new _fs.Event();

    var _noHandler = function() {
        _debug.error('scroll', '[Fs.events.Scroll] No handler is define !');
    };

    var _poolHandler = function() {
        if (_scrolled === true) {
            _scrolled = false;
            _events.fire('scroll', _lastEvent);
        }
    };

    var _windowScrollListener = function(event) {
        _scrolled = true;
        _lastEvent = event;
        _debug.log('scroll', 'window scroll', event, event.target, _events);
    };

    var _windowResizeListener = function(event) {
        _screenHeight = _doc.height;
        _debug.log('scroll', 'window resize', _screenHeight);
    };



    
    return _fs.subclass({

        xtype: 'events.scroll',
        defaultInterval: 500,
        useCapture: false,
        autoAttach: false,
        
        defaultScope: false,
        defaultHandler: false,
        defaultEl: false,

        constructor: function(opts) {
            opts = opts || {};
            _fs.utils.apply(this, opts, [
                'useCapture',
                'autoAttach'
            ]);
            this.interval = opts.interval || this.defaultInterval;
            this.defaultEl = opts.el || window;
            this.defaultScope = opts.scope || this;
            this.defaultHandler = (opts.handler ?
                opts.handler.bind(this.defaultScope) : _noHandler);
            this.attached = false;
            if (this.autoAttach) {
                this.attach();
            }
        },

        attach: function(cmp, handler, scope) {
            if (this.attached === true) {
                return false;
            }
            this.attached = true;
            if (_eventsAttached === false) {
                _eventsAttached = true;
                _window.addEventListener('scroll',
                    _windowScrollListener, false);
                _window.addEventListener('resize',
                    _windowResizeListener, false);
                _window.setInterval(_poolHandler, this.interval);
            }
            this.el = cmp || this.defaultEl;
            scope = scope || this.defaultScope;
            this.handler = (handler ?
                handler.bind(scope) : this.defaultHandler);
            this.euid = _events.on('scroll', this.handler, scope);
        },

        detach: function(cmp) {
            if (this.attached === false) {
                return false;
            }
            this.attached = false;
            var length = _events.off(this.euid);
            this.el = cmp || this.el || this.defaultEl;
            this.handler = this.handler || this.defaultHandler;
            if (length === 0) {
                this.reset();
            }
        },

        reset: function() {
            if (_eventsAttached !== true) {
                return false;
            }
            _eventsAttached = false;
            _window.removeEventListener('scroll',
                _windowScrollListener, false);
            _window.removeEventListener('resize',
                _windowResizeListener, false);
            _window.clearInterval(_poolHandler);
        }
    });
})();



Fs.events.Resize = (function() {

    
    var _lastEvent,
        _window = window,
        _eventsAttached = false,
        _resized = false,
        _fs = Fs,
        _debug = _fs.debug,
        _events = new _fs.Event();

    var _noHandler = function() {
        _debug.warn('events.resize', 'No handler is define !');
    };

    var _poolHandler = function() {
        if (_resized === true) {
            _resized = false;
            _events.fire('resize', _lastEvent);
        }
    };

    var _windowResizeListener = function(event) {
        _resized = true;
        _lastEvent = event;
        _debug.log('events.resize', '[window] resize');
    };



    
    return _fs.subclass({

        defaultInterval: 500,

        constructor: function(opts) {
            opts = opts || {};
            this.interval = opts.interval || this.defaultInterval;
            this.attached = false;
        },

        attach: function(cmp, handler, scope) {
            if (this.attached === true) {
                return false;
            }
            this.attached = true;
            if (_eventsAttached === false) {
                _eventsAttached = true;
                _window.addEventListener('resize',
                    _windowResizeListener, false);
                _window.setInterval(_poolHandler, this.interval);
            }
            this.el = cmp;
            scope = scope || this;
            this.handler = handler.bind(scope) || _noHandler;
            this.euid = _events.on('resize', this.handler, scope);
        },

        detach: function(cmp) {
            if (this.attached === false) {
                return false;
            }
            this.attached = false;
            this.el = cmp || this.el;
            var length = _events.off(this.euid);
            delete this.handler;
            if (length === 0) {
                this.reset();
            }
        },

        reset: function() {
            if (_eventsAttached !== true) {
                return false;
            }
            _eventsAttached = false;
            _window.removeEventListener('resize',
                _windowResizeListener, false);
            _window.clearInterval(_poolHandler);
        }
    });
})();





Fs.events.Nobounce = (function() {

    
    var _lastEvent,
        _fs = Fs,
        _selector = _fs.Selector,
        _debug = _fs.debug,
        _doc = document,
        _parent = _fs;

    var _isParentNoBounce = function(el, className) {
        while (el) {
            if (_selector.hasClass(el, className)) {
                return true;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };

    var _disableBounce = function(e) {
        _debug.log('nobounce', 'disable bounce ?');
        if (_isParentNoBounce(e.target, this.className)) {
            _debug.log('nobounce', 'disabling bounce !');
            e.preventDefault();
        }
    }

    
    return _parent.subclass({

        defaultClassName: 'nobounce',
        defaultAutoAttach: true,
        defaultEl: _doc,

        constructor: function(opts) {
            opts = opts || {};
            this.autoAttach = opts.autoAttach || this.defaultAutoAttach;
            this.className = opts.className || this.defaultClassName;
            this.el = opts.el || this.defaultEl;
            this.attached = false;
            this.handler = _disableBounce.bind(this);
            if (this.autoAttach === true) {
                this.attach(this.el);
            }
        },

        attach: function(cmp) {
            if (this.attached === true) {
                return false;
            }
            this.attached = true;
            this.el = cmp || this.el;
            this.el.addEventListener('touchmove', this.handler, false);
        },

        detach: function(cmp) {
            if (this.attached === false) {
                return false;
            }
            this.attached = false;
            this.el = cmp || this.el;
            this.el.removeEventListener('touchmove', this.handler, false);
        }
    });
})();


Fs.events.Focus = (function() {

    
    var _fs = Fs,
        _parent = _fs.events.Abstract;

    
    return _parent.subclass({

        xtype: 'events.focus',
        eventName: 'focusin',
        globalFwd: window,

        constructor: _parent.prototype.constructor
    });
}());

Fs.events.Blur = (function() {

    
    var _fs = Fs,
        _parent = _fs.events.Abstract;

    
    return _parent.subclass({

        xtype: 'events.blur',
        eventName: 'focusout',
        globalFwd: window,

        constructor: _parent.prototype.constructor
    });
}());

Fs.events.Keypress = (function() {

    
    var _fs = Fs,
        _parent = _fs.events.Abstract;

    
    return _parent.subclass({

        xtype: 'events.keypress',
        eventName: 'keypress',
        globalFwd: window,

        constructor: function() {
            _parent.prototype.constructor.apply(this, arguments);
        }
    });
}());


Fs.data.Store = (function() {

    var _fs = Fs,
        _storage = _fs.Storage,
        _debug = _fs.debug,
        _fsrequest = _fs.Request,
        _parent = _fs.Event;

    return _parent.subclass({

        xtype: 'store',
        defaultEnableHashTable: false,
        defaultReaderRoot: 'data',
        defaultReaderTotal: 'total',
        defaultReaderSuccess: 'success',
        defaultReaderId: 'id',
        defaultStorageCache: false,

        constructor: function(config) {

            this.config = config || {};
            this.totalRecords = config.totalRecords || 0;
            this.loadedRecords = 0;
            this.records = [];
            this.isLoading = false;
            
            this.enableHashTable = config.enableHashTable || this.defaultEnableHashTable;
            this.storageCache = config.storageCache || this.defaultStorageCache;
            if (this.enableHashTable) {
                this.hashRecords = {};
            }
            this.rootProperty = this.getReaderRoot(this.defaultReaderRoot);
            this.totalProperty = this.getReaderTotal(this.defaultReaderTotal);
            this.successProperty = this.getReaderSuccess(this.defaultReaderSuccess);
            this.idProperty = this.getReaderId(this.defaultReaderId);

            _parent.prototype.constructor.apply(this, arguments);

            this.setupListeners(['load', 'afterload',
                'beforeload', 'loaderror']);

        },

        load: function(opts) {
            opts = _fs.utils.applyIfAuto(opts || {}, this.getProxyOpts());
            _debug.log('store', 'load/opts: ', opts);
            this.isLoading = true;

            var request,
                self = this,
                type = this.getProxyType(),
                finalCb = opts.callback;

            this.fire('beforeload', this, opts);
            var cb = function(success, data) {
                var total,
                    newRecords = [];

                _debug.log('store', 'callback', arguments);
                if (success && data[self.successProperty]) {
                    total = data[self.totalProperty];
                    if (typeof total !== 'undefined') {
                        self.totalRecords = total;
                    }
                    newRecords = data[self.rootProperty];
                    if (self.enableHashTable) {
                        var recordId, length = newRecords.length;
                        while (length--) {
                            recordId = newRecords[length][self.idProperty];
                            self.hashRecords['r' + recordId] = self.loadedRecords + length;
                        }
                    }
                    self.loadedRecords += newRecords.length;
                    self.records = self.records.concat(newRecords);
                } else {
                    self.fire('loaderror', success, self, data);
                }
                _debug.log('store', 'finalCb: ', finalCb);
                self.fire('load', success, self, newRecords);
                if (finalCb) {
                    finalCb(success, self, newRecords);
                }
                self.fire('afterload', success, self, newRecords);
                self.isLoading = false;
            };

            opts.url = opts.url || this.getProxyUrl();

            if (this.storageCache) {
                var key = JSON.stringify(_fs.utils.crc32(JSON.stringify(opts))),
                    results = _storage.getItem(key);
                if (results) {
                    setTimeout(function() {
                        cb(true, results);
                    }, 10);
                    return true;
                } else {
                    var tmp = cb;
                    cb = function(success, data) {
                        if (success && data[self.successProperty]) {
                            _storage.setItem(key, data);
                        }
                        tmp(success, data);
                    };
                }
            }

            if (this.getProxyCache() === false) {
                this.addUrlTimestamp(opts.url);
            }
            opts.callback = cb;
            if (type === 'jsonp') {
                request = _fsrequest.jsonp;
            } else if (type === 'memory') {
                request = _fsrequest.memory;
            } else {
                request = _fsrequest.ajax;
            }
            return request(opts.url, opts, this);
        },

        addRecord: function(record, fromStart) {
            var recordId = record[this.idProperty];
            if (this.enableHashTable) {
                this.hashRecords['r' + recordId] = this.loadedRecords;
            }
            if (fromStart === true) {
                this.records = [record].concat(this.records);
                if (this.enableHashTable) {
                    var rec,
                        i = this.loadedRecords + 1;
                    while (i--) {
                        rec = this.records[i];
                        this.hashRecords['r' + rec[this.idProperty]] = i;
                    }
                }
            } else {
                this.records = this.records.concat([record]);
            }
            this.loadedRecords += 1;
            this.totalRecords += 1;
            
        },

        reset: function() {
            this.records = [];
            this.totalRecords = 0;
            this.loadedRecords = 0;
        },

        addUrlTimestamp: function(url) {
            
            var date = new Date();
        },

        getRecord: function(recId) {
            
            if (!this.enableHashTable) {
                _debug.error('store', 'You must enable hash table for using store.getRecord by setting enableHashTable: true in store config.');
                return;
            }
            
            return this.records[this.hashRecords['r' + recId]];
        },

        getAt: function(index) {
            return this.records[index];
        },

        setProxyOpts: function(opts) {
            if (!this.config.proxy) {
                this.config.proxy = {};
            }
            this.config.proxy.opts = opts;
            _debug.warn('store', 'setProxyOpts', opts, this.config.proxy);
            return true;
        },

        getProxyOpts: function(defaultValue) {
            defaultValue = defaultValue || {};
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return p.opts || defaultValue;
            }
            return defaultValue;
        },

        getProxyUrl: function(defaultValue) {
            defaultValue = defaultValue || '';
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return p.url || defaultValue;
            }
            return defaultValue;
        },

        getProxyCache: function(defaultValue) {
            defaultValue = defaultValue || true;
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return (p.cache === false ? false : defaultValue);
            }
            return defaultValue;
        },

        getProxyType: function(defaultValue) {
            defaultValue = defaultValue || 'ajax';
            var p = this.config.proxy;

            if (typeof p === 'object') {
                return p.type || defaultValue;
            }
            return defaultValue;
        },

        getReaderRoot: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderRoot;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.rootProperty || defaultValue;
            }
            return defaultValue;
        },

        getReaderTotal: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderTotal;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.totalProperty || defaultValue;
            }
            return defaultValue;
        },

        getReaderSuccess: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderSuccess;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.successProperty || defaultValue;
            }
            return defaultValue;
        },

        getReaderId: function(defaultValue) {
            defaultValue = defaultValue || this.defaultReaderId;
            var reader = this.config.reader;

            if (typeof reader === 'object') {
                return reader.idProperty || defaultValue;
            }
            return defaultValue;
        }
    });
}());


Fs.data.PagingStore = (function() {

    var _fs = Fs,
        _parent = _fs.data.Store;

    return _parent.subclass({

        constructor: function(config) {
            config = config || {};
            this.currentPage = config.currentPage || 0;
            this.recordPerPage = config.recordPerPage || 10;
            _parent.prototype.constructor.call(this, config);
        },

        loadPage: function(page, opts) {
            _fs.debug.log('[pagingstore] !! opts', opts);
            opts = opts || {};
            opts.url = opts.url || this.getProxyUrl();
            var nb = this.recordPerPage,
                paging = 'start=' + (page * nb) +
                    '&limit=' + nb;
            if (opts.url.indexOf('?') !== -1) {
                opts.url += '&' + paging;
            } else {
                opts.url += '?' + paging;
            }
            _fs.debug.log('[pagingstore] loadPage', opts, arguments);
            return this.load(opts);
        },

        nextPage: function(opts) {
            _fs.debug.log('[pagingstore/nextPage] opts', opts);
            return this.loadPage(this.currentPage++, opts);
        },

        previousPage: function(opts) {
            return this.loadPage(this.currentPage--, opts);
        },

        reset: function() {
            this.currentPage = 0;
            _parent.prototype.reset.apply(this, arguments);
        }
    });

}());


Fs.views.Template = (function() {

    var _fs = Fs,
        _gconf = _fs.config,
        _parent = _fs.Event,
        _debug = _fs.debug,
        _selector = _fs.Selector,
        _win = window,
        
        j = 0;

    function _registerEngineEvents() {
        var self = this;

        if (!self.renderer ||
            self.engineEventsRegistered) {
            return false;
        }
        self.engineEventsRegistered = true;

        var i, e, events = self.engine.getEvents(self.xtype);

        for (i in events) {
            e = events[i];
            if (typeof e === 'function') {
                if (i.indexOf('render') >= 0) {
                    self.renderer.on(i, events[i], self, self.priority.VIEWS);
                } else {
                    self.on(i, events[i], self, self.priority.VIEWS);
                }
            }
        }
        return true;
    };

    

    return _parent.subclass({

        className: 'Fs.views.Template',
        xtype: 'template',

        constructor: function(opts) {
            var self = this;

            opts = opts || {
                template: '',
                config: {},
                items: []
            };
            self.config = opts.config || {};
            if (!self.config.id) {
                self.config.id = _selector.generateId();
            }
            self.items = opts.items || [];
            if (!self.items.indexOf) {
                self.items = [self.items];
            }
            self.data = self.config;
            self.data.items = self.items;
            if (!opts.template) {
                opts.template = self.setEngine(opts, _gconf).getTpl(self.xtpl || self.xtype);
            } else {
                self.setEngine(opts, _gconf);
            }
            if (!opts.ui && _gconf.ui) {
                opts.ui = _gconf.ui;
            }
            if (opts.ref) {
                self.ref = opts.ref;
            }
            self.tpl = opts.template;
            self.html = '';
            self.renderToEl = '';
            self.el = '';
            self.engineEventsRegistered = false;
            _parent.prototype.constructor.apply(self, arguments);
        },

        getEngine: function(config, gconfig) {
            if (config && config.engine) {
                return config.engine;
            } else if (gconfig) {
                return gconfig.engine;
            }
            return this.engine;
        },

        setEngine: function(config, gconfig) {
            var self = this;

            self.engine = self.getEngine(config, gconfig);
            self.engine = new self.engine();
            return self.engine;
        },

        compile: function(parentConfig, renderer) {
            var item, tpl,
                self = this,
                i = -1,
                items = self.items,
                length = self.items.length,
                test = ++j;

            _debug.profile(self.xtype, 'compile' + test);
            if (parentConfig) {
                if (_gconf.ui && !parentConfig.ui) {
                    parentConfig.ui = _gconf.ui;
                }
                _fs.utils.applyIf(self.config, parentConfig, ['ui']);
            }

            while (++i < length) {
                item = items[i];
                if (typeof item === 'object' &&
                    typeof item.xtype === 'string' &&
                    typeof item.__proto__.xtype === 'undefined') {
                    item = new (_fs.xtype(item.xtype))(item);
                }
                if (typeof item === 'string') {
                    tpl = _fs.Templates.compile(item);
                    self.data.items[i] = tpl(self.config);
                } else if (typeof item.compile === 'function') {
                    self.data.items[i] = item.compile(self.config, renderer || self);
                } else {
                    self.data.items[i] = item;
                }
            }

            self.html = self.tpl(self.data);
            _debug.profileEnd(self.xtype, 'compile ' + test);
            if (!self.renderer) {
                self.renderer = renderer || self;
            }
            
            
            
            if (self.ref &&
                !self.renderer[self.ref]) {
                self.renderer[self.ref] = self;
            }
            

            self.__arID = self.renderer.on('afterrender', function() {
                self.el = document.getElementById(self.config.id);
                
                self.renderer.off(self.__arID);
                delete self.__arID;
            }, self, self.priority.CORE);

            self.setupListeners(['aftercompile', 'afterrender'], self.renderer);
            self.fire('aftercompile', self, self.renderer);
            _registerEngineEvents.call(self);
            return self.html;
        },

        
        render: function(renderToEl, position) {
            var self = this;

            position = position || 'beforeEnd';
            if (typeof renderToEl === 'string') {
                
                self.renderToEl = _fs.Selector.get(renderToEl);
            } else {
                self.renderToEl = renderToEl;
            }
            
            
            
            
            

            self.renderToEl.insertAdjacentHTML(position, self.html);
            self.fire('afterrender', self);

            if (!self.el) {
                self.el = document.getElementById(self.config.id);
            }
        },

        remove: function() {
            if (this.el) {
                this.el.parentNode.removeChild(this.el);
                delete this.el;
                
                
            } else {
                console.warn('Element cannot been removed because it has not been rendered !', this);
            }
        },

        

        setUI: function() {
            console.warn('setUI is not implemented for xtype "',
                this.xtype, '" and engine "', this.getEngine().xtype, '"');
        }
    });

}());


Fs.views.Abstract = (function() {

    
    var _fs = Fs,
        _templates = _fs.Templates,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Abstract',
        xtype: 'abstract',

        constructor: function(opts) {
            opts = opts || {};
            if (opts.tpl) {
                opts.template = _templates.compile(opts.tpl);
            } else if (opts.xtpl) {
                this.xtype = opts.xtpl;
            }
            _parent.prototype.constructor.call(this, opts);
        }
    });
})();



Fs.views.LoadMask = (function() {

    var _fs = Fs,
        _selector = _fs.Selector,
        _parent = Fs.views.Template;

    return _parent.subclass({

        className: 'Fs.views.LoadMask',
        xtype: 'loadmask',

        show: function() {
            _selector.removeClass(this.el, 'hidden');
        },

        hide: function() {
            _selector.addClass(this.el, 'hidden');
        }
    });

}());

Fs.views.Container = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Container',
        xtype: 'container'

    });
})();


Fs.views.Tabs = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Tabs',
        xtype: 'tabs'

    });
})();

Fs.views.Header = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Header',
        xtype: 'header'

    });
})();


Fs.views.Page = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Page',
        xtype: 'page'

    });
})();


Fs.views.Footer = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Footer',
        xtype: 'footer'

    });
})();


Fs.views.Toolbar = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    
    return _parent.subclass({

        className: 'Fs.views.Toolbar',
        xtype: 'toolbar'

    });
})();


Fs.views.Button = (function() {

    
    var _fs = Fs,
        _debug = _fs.debug,
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        
        this.eventClick = new _fs.events.Click();
        this.eventClick.attach(this.el, this.handler, this);
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    var _setDisabled = function(disabled) {
        disabled = (disabled === true);
        this.disabled = disabled;
        if (this.el) {
            if (disabled) {
                this.el.setAttribute('disabled', 'disabled');
            } else {
                this.el.removeAttribute('disabled');
            }
        }
        this.fire('disabled', this, disabled);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Button',
        xtype: 'button',

        constructor: function(opts) {
            opts = opts || {};
            if (opts.handler) {
                this.handler = (opts.scope ?
                    opts.handler.bind(opts.scope) : opts.handler);
            }
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        handler: function(event) {
            var route = this.config.route;
            
            _debug.log('button', 'handler: ', this, event, arguments, route);
            
            if (route) {
                _fs.History.navigate(route);
            }
        },

        setDisabled: _setDisabled
    });
})();



Fs.views.TabButton = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Button;

    
    return _parent.subclass({

        className: 'Fs.views.TabButton',
        xtype: 'tabbutton',

        handler: _parent.prototype.handler

    });
})();



Fs.views.List = (function() {

    
    var _fs = Fs,
        _debug = _fs.debug,
        _selector = _fs.Selector,
        _parent = _fs.views.Template;

    

    var _findParentItem = function(el) {
        while (el) {
            if (_fs.Selector.hasClass(el, 'list-item')) {
                return el;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };

    var _onAfterRender = function(cmp) {
        var id = this.config.id;

        this.loadingEl = document.getElementById(id + '-loader');
        if (this.hasListener('itemselect')) {
            this.clickEvent = new _fs.events.Click({
                autoAttach: true,
                el: this.el,
                scope: (this.listeners.scope ? this.listeners.scope : undefined),
                handler: this.getListener('itemselect')
            });
        }
        if (this.autoload) {
            this.load();
        }
        _debug.log('list', 'afterrender', arguments, this.config.id);
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.List',
        xtype: 'list',

        defaultAutoload: true,

        constructor: function(opts) {
            opts = opts || {};
            this.store = opts.store;
            if (opts.listeners) {
                this.listeners = opts.listeners;
            }
            this.autoload = (typeof opts.autoload === 'boolean' ?
                opts.autoload : this.defaultAutoload);
            if (opts.config) {
                opts.config.isLoading = this.autoload;
            }
            this.emptyTpl = opts.emptyTpl || false;
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);

            this.itemTpl = opts.itemTpl || this.engine.getTpl(this.xtype + 'itembasic');
            if (typeof opts.itemTpl === 'string') {
                var engineItemTpl = this.engine.getTpl(this.xtype + 'item' + opts.itemTpl);
                if (engineItemTpl) {
                    this.itemTpl = engineItemTpl;
                }
            }
        },

        getItemFromEvent: function(event) {
            return _findParentItem(event.target);
        },

        reload: function(items) {
            if (this.isLoading === true) {
                return false;
            }
            this.el.innerHTML = '';

            

            if (this.store) {
                this.showLoading();
                this.store.load({
                    callback: this.storeLoaded.bind(this)
                });
            } else {
                this.loadItems(items || this.items);
            }
        },

        showLoading: function() {
            this.isLoading = true;
            _selector.removeClass(this.loadingEl, 'list-loader-hide');
        },

        hideLoading: function() {
            this.isLoading = false;
            _selector.addClass(this.loadingEl, 'list-loader-hide');
        },

        load: function(items) {
            if (this.isLoading === true) {
                return false;
            }
            if (this.store) {
                this.showLoading();
                this.store.load({
                    callback: this.storeLoaded.bind(this)
                });
            } else {
                this.loadItems(items || this.items);
            }
        },

        loadItems: function(items) {
            var itemTpl = this.itemTpl,
                config = _fs.utils.applyAuto(this.config, {});

            config.items = items;
            this.el.insertAdjacentHTML('beforeEnd', itemTpl(config));
            this.hideLoading();
        },

        storeLoaded: function(success, store, newRecords) {
            _debug.log('list', 'storeLoaded', arguments);

            this.loadItems(newRecords);

            if (!store.totalRecords && this.emptyTpl) {
                _selector.updateHtml(this.el, this.emptyTpl);
            }
        }
    });
})();




Fs.views.ListBuffered = (function() {

    
    var _window = window,
        _fs = Fs,
        _events = _fs.events,
        _debug = _fs.debug,
        _selector = _fs.Selector,
        _parent = _fs.views.Template;

    

    var _findParentItem = function(el) {
        while (el) {
            if (_selector.hasClass(el, 'list-item')) {
                return el;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };

    var _onAfterRender = function(cmp) {
        this.loadingEl = this.el.parentNode.querySelector('.list-loader');
        if (this.hasListener('itemselect')) {
            this.clickEvent = new _events.Click({
                autoAttach: true,
                el: this.el,
                scope: (this.listeners.scope ? this.listeners.scope : undefined),
                handler: this.getListener('itemselect')
            });
        }
        this.scrollEvent = new _events.Scroll({
            autoAttach: true,
            el: this.el,
            scope: this,
            handler: this.scrollHandler
        });

        this.loadNext();

        this.renderer.on('show', function() {
            this.scrollEvent.attach();
            if (this.clickEvent) {
                this.clickEvent.attach();
            }
        }, this, this.priority.VIEWS);

        this.renderer.on('hide', function() {
            this.scrollEvent.detach();
            if (this.clickEvent) {
                this.clickEvent.detach();
            }
        }, this, this.priority.VIEWS);
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.ListBuffered',
        xtype: 'listbuffered',

        constructor: function(opts) {
            opts = opts || {};
            this.store = opts.store;
            if (opts.listeners) {
                this.listeners = opts.listeners;
            }
            this.emptyTpl = opts.emptyTpl || false;
            this.isLoading = false;
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);

            this.itemTpl = opts.itemTpl || this.engine.getTpl(this.xtype + 'itembasic');
            if (typeof opts.itemTpl === 'string') {
                var engineItemTpl = this.engine.getTpl(this.xtype + 'item' + opts.itemTpl);
                if (engineItemTpl) {
                    this.itemTpl = engineItemTpl;
                }
            }
        },

        getItemFromEvent: function(event) {
            return _findParentItem(event.target);
        },

        scrollHandler: function(event) {
            var p = document.body,
                
                screenHeight = _window.screen.availHeight,
                scrollHeight = p.scrollHeight,
                totalScroll = p.scrollTop + screenHeight;
            _debug.log('listbuffered', 'scroll', totalScroll, scrollHeight);
            if (totalScroll >= scrollHeight) {
                _debug.log('listbuffered', 'loading next');
                this.loadNext();
            }
        },

        reload: function() {
            if (this.isLoading === true) {
                return false;
            }
            this.isLoading = true;
            this.el.innerHTML = '';
            this.showLoading();
            this.store.reset();
            this.store.nextPage({
                callback: this.getNext.bind(this)
            });
        },

        loadNext: function() {
            if (this.isLoading === true) {
                return false;
            } else if (this.store.loadedRecords &&
                this.store.loadedRecords === this.store.totalRecords) {
                this.hideLoading();
                return false;
            }
            this.isLoading = true;
            this.showLoading();
            this.store.nextPage({
                callback: this.getNext.bind(this)
            });
        },

        getNext: function(success, store, newRecords) {
            var conf, itemTpl = this.itemTpl || _itemTpl;

            _debug.log('listbuffered', 'getNext', arguments);
            conf = this.config;
            conf.items = newRecords;
            _debug.log('listbuffered', 'records', newRecords);
            this.el.insertAdjacentHTML('beforeEnd', itemTpl(conf));
            
            
            
            _selector.redraw(this.el);
            
            if (store.loadedRecords === store.totalRecords) {
                this.hideLoading();
            }
            this.isLoading = false;

            if (!store.totalRecords) {
                this.scrollEvent.detach();
                if (this.emptyTpl) {
                    _selector.updateHtml(this.el, this.emptyTpl);
                }
            }
        },

        showLoading: function() {
            _selector.removeClass(this.loadingEl, 'list-loader-hide');
        },

        hideLoading: function() {
            _selector.addClass(this.loadingEl, 'list-loader-hide');
        }
    });
})();



Fs.views.Textfield = (function() {

    
    var _fs = Fs,
        
        _parent = _fs.views.Template;

    var _setDisabled = function(disabled) {
        disabled = (disabled === true);
        this.disabled = disabled;
        if (this.el) {
            if (disabled) {
                this.el.setAttribute('disabled', 'disabled');
            } else {
                this.el.removeAttribute('disabled');
            }
        }
        this.fire('disabled', this, disabled);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Textfield',
        xtype: 'textfield',

        setDisabled: _setDisabled
    });
})();


Fs.views.Passwordfield = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Textfield;

    
    return _parent.subclass({

        className: 'Fs.views.Passwordfield',
        xtype: 'passwordfield',
        xtpl: 'textfield',

        constructor: function(opts) {
            opts = opts || {};
            opts.config = opts.config || {};
            opts.config.type = 'password';
            opts.config.autocomplete = 'off';
            _parent.prototype.constructor.call(this, opts);
        }
    });
})();


Fs.views.FloatingPanel = (function() {

    
    var _fs = Fs,
        _debug = _fs.debug,
        _selector = _fs.Selector,
        _win = window,
        _parent = _fs.views.Template;

    var _onAfterRender = function(cmp) {
        
        this.overlayEl = document.getElementById(this.config.id + '-overlay');
        this.clickEvent = new _fs.events.Click({
            disableScrolling: true
        });
        this.clickEvent.attach(this.overlayEl, this.hide, this);
        this.resizeEvent = new _fs.events.Resize();
        this.resizeEvent.attach(this.el, this.resize, this);
        this.resize();
    };

    var _findParentItem = function(el) {
        while (el) {
            if (_fs.Selector.hasClass(el, 'floatingpanel')) {
                return el;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };

    var _stopWindowScroll = function(e) {
        _debug.warn('floatingpanel', 'try to stop window scroll', e);
        if (_findParentItem(e.target) === false) {
            _debug.log('floatingpanel', ' stoping window scroll !', e);
            e.stopPropagation ? e.stopPropagation() : (e.cancelBubble = true);
            e.preventDefault ? e.preventDefault() : (e.returnValue = false);
            return false;
        }
    };

    var _realStopWindowScroll = function(e) {
        e.stopPropagation ? e.stopPropagation() : (e.cancelBubble = true);
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
        return false;
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.FloatingPanel',
        xtype: 'floatingpanel',

        constructor: function(opts) {
            _parent.prototype.constructor.call(this, opts);
            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        show: function() {
            _selector.removeClass(this.el, 'hidden');
            _selector.removeClass(this.overlayEl, 'hidden');
            _selector.addClass(document.body, 'scroll-stop');
            _win.addEventListener('touchstart', _stopWindowScroll, false);
        },

        hide: function() {
            _selector.addClass(this.el, 'hidden');
            _selector.addClass(this.overlayEl, 'hidden');
            _selector.removeClass(document.body, 'scroll-stop');
            _win.removeEventListener('touchstart', _stopWindowScroll, false);
        },

        resize: function(event) {
            if (!this.el.offsetWidth) {
                var self = this;
                setTimeout(function() {
                    self.resize();
                }, 100);
                return false;
            }

            var leftMargin,
                elWidth = this.el.offsetWidth,
                containerWidth = document.body.offsetWidth;

            leftMargin = (containerWidth - elWidth) / 2;
            this.el.style.left = leftMargin + 'px';
            return true;
        }
    });
})();



Fs.views.ControlGroup = (function() {

    
    var _fs = Fs,
        _debug = _fs.debug,
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        
        this.eventClick = new Fs.events.Click();
        this.eventClick.attach(this.el, this.handler, this);
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.ControlGroup',
        xtype: 'controlgroup',

        constructor: function(opts) {
            opts = opts || {};
            if (opts.handler) {
                this.handler = opts.handler;
            }
            _parent.prototype.constructor.call(this, opts);
            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        handler: function(event) {
            var route = this.config.route;
            _debug.log('controlgroup', 'handler: ', this, event, arguments, route);
            if (route) {
                _fs.History.navigate(route);
            }
        }
    });
})();



Fs.views.Collapsible = (function() {

    
    var _fs = Fs,
        _selector = _fs.Selector,
        _debug = _fs.debug,
        _templates = _fs.Templates,
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        var engine = this.getEngine(),
            id = this.config.id;

        this.headerEl = document.getElementById(id + '-header');
        this.contentEl = document.getElementById(id + '-content');
        if (this.canCollapse) {
            this.eventClick = new Fs.events.Click({
                autoAttach: true,
                el: this.headerEl,
                scope: this,
                handler: this.handler
            });
        }
    };

    var _onAfterCompile = function() {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Collapsible',
        xtype: 'collapsible',

        constructor: function(opts) {
            opts = opts || {};
            opts.config = opts.config || {};
            this.collapsed = opts.config.collapsed || false;
            if (typeof opts.config.canCollapse !== 'undefined') {
                this.canCollapse = opts.config.canCollapse;
            } else {
                this.canCollapse = true;
            }
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        toggle: function() {
            if (this.collapsed) {
                return this.expand();
            }
            return this.collapse();
        },

        collapse: function() {
            this.collapsed = true;
            this.fire('collapse', this);
        },

        expand: function() {
            this.collapsed = false;
            this.fire('expand', this);
        },

        handler: function() {
            this.toggle();
        }
    });
})();



Fs.views.FlipSwitch = (function() {

    
    var _fs = Fs,
        _selector = _fs.Selector,
        _debug = _fs.debug,
        _templates = _fs.Templates,
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        this.uiEl = document.getElementById(this.config.id + '-ui');
        this.eventClick = new Fs.events.Click();
        this.eventClick.attach(this.uiEl, this.handler, this);
        if (this.config.value === this.config.options[1].value) {
            this.check();
        } else {
            this.uncheck();
        }
    };

    var _onAfterCompile = function() {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.FlipSwitch',
        xtype: 'flipswitch',

        constructor: function(opts) {
            opts = opts || {};
            opts.config = opts.config || {};
            if (!opts.config.options) {
                opts.config.options = [{
                    value: 'off',
                    label: 'Off'
                }, {
                    value: 'on',
                    label: 'On'
                }];
            }
            if (!opts.config.value) {
                opts.config.value = opts.config.options[0].value;
            }
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        toggle: function() {
            if (this.checked) {
                return this.uncheck();
            }
            return this.check();
        },

        check: function() {
            this.fire('check', this);
            this.checked = true;
            if (this.el) {
                this.el.selectedIndex = 1;
            }
        },

        uncheck: function() {
            this.fire('uncheck', this);
            this.checked = false;
            if (this.el) {
                this.el.selectedIndex = 0;
            }
        },

        handler: function() {
            this.toggle();
        }
    });
})();



Fs.views.Checkbox = (function() {

    
    var _fs = Fs,
        _selector = _fs.Selector,
        _debug = _fs.debug,
        _templates = _fs.Templates,
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        this.uiEl = document.getElementById(this.config.id + '-ui');
        this.eventClick = new Fs.events.Click();
        this.eventClick.attach(this.uiEl, this.handler, this);
        if (this.config.value) {
            this.check();
        } else {
            this.uncheck();
        }
    };

    var _onAfterCompile = function() {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Checkbox',
        xtype: 'checkbox',

        constructor: function(opts) {
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        toggle: function() {
            if (this.checked) {
                return this.uncheck();
            }
            return this.check();
        },

        check: function() {
            this.fire('check', this);
            this.checked = true;
            if (this.el) {
                this.el.checked = true;
            }
        },

        uncheck: function() {
            this.fire('uncheck', this);
            this.checked = false;
            if (this.el) {
                this.el.checked = false;
            }
        },

        handler: function() {
            this.toggle();
        }
    });
})();



Fs.views.Textarea = (function() {

    
    var _fs = Fs,
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        if (this.clearBtn) {
            this.clearBtnEl = document.getElementById(this.config.id + '-clearBtn');
            
        }
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Textarea',
        xtype: 'textarea',

        constructor: function(opts) {
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        }
    });
})();



Fs.views.Select = (function() {

    
    var _fs = Fs,
        
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        var id = this.config.id;

        this.uiEl = document.getElementById(id + '-ui');
        this.setupListeners(['change']);
        this.changeEvent = new _fs.events.Abstract({
            autoAttach: true,
            eventName: 'change',
            el: this.el,
            scope: this,
            handler: function() {
                var item;

                if (this.store) {
                    item = this.store.getAt(this.el.selectedIndex);
                } else {
                    item = this.config.options[this.el.selectedIndex];
                }
                this.fire('change', this, item);
            }
        });
        if (this.store && this.autoload) {
            this.load();
        }
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    var _setDisabled = function(disabled) {
        disabled = (disabled === true);
        this.disabled = disabled;
        if (this.el) {
            if (disabled) {
                this.el.setAttribute('disabled', 'disabled');
            } else {
                this.el.removeAttribute('disabled');
            }
        }
        this.fire('disabled', this, disabled);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Select',
        xtype: 'select',

        defaultAutoload: true,

        constructor: function(opts) {
            opts = opts || {};
            this.store = opts.store;
            opts.config = opts.config || {};
            this.autoload = (typeof opts.autoload === 'boolean' ?
                opts.autoload : this.defaultAutoload);

            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);

            this.itemTpl = opts.itemTpl || this.engine.getTpl(this.xtype + 'itembasic');
            if (typeof opts.itemTpl === 'string') {
                var engineItemTpl = this.engine.getTpl(this.xtype + 'item' + opts.itemTpl);
                if (engineItemTpl) {
                    this.itemTpl = engineItemTpl;
                }
            }
        },

        setLoading: function(loading) {
            loading = (loading === true);
            this.isLoading = loading;
            this.fire('loading', this, loading);
        },

        setDisabled: _setDisabled,

        reload: function(items) {
            while (this.el.firstChild) {
                this.el.removeChild(this.el.firstChild);
            }
            if (this.store) {
                this.setLoading(true);
                this.store.reset();
                this.store.load({
                    callback: this.storeLoaded.bind(this)
                });
            } else {
                this.loadItems(items || this.config.options);
            }
        },

        clearSelection: function() {
            if (this.el) {
                var emptyItem = {};

                if (this.config.emptyText) {
                    emptyItem.label = this.config.emptyText;
                }
                this.el.selectedIndex = 0;
                this.fire('change', this, emptyItem);
            }
        },

        load: function(items) {
            if (this.isLoading === true) {
                return false;
            }
            if (this.store) {
                this.setLoading(true);
                this.store.load({
                    callback: this.storeLoaded.bind(this)
                });
            } else {
                this.loadItems(items || this.config.options);
            }
        },

        loadItems: function(items) {
            var itemTpl = this.itemTpl,
                config = _fs.utils.applyAuto(this.config, {});

            config.options = items;
            this.el.insertAdjacentHTML('beforeEnd', itemTpl(config));
            this.setLoading(false);
        },

        storeLoaded: function(success, store, newRecords) {
            if (this.config.emptyText) {
                store.addRecord({
                    label: this.config.emptyText
                }, true);
            }
            this.loadItems(this.store.records);
        }
    });
})();


Fs.views.RadioButton = (function() {

    
    var _radioUncheckEvent,
        _window = window,
        _fs = Fs,
        _selector = _fs.Selector,
        
        _parent = _fs.views.Template;

    
     if (_window.CustomEvent) {
        _radioUncheckEvent = new _window.CustomEvent('radioUncheck', {
            
            bubbles: true,
            cancelable: true
        });
    }

    var _findParentForm = function(el) {
        while (el) {
            if (el.tagName === 'FORM') {
                return el;
            }
            if (el.parentElement) {
                el = el.parentElement;
            } else {
                return false;
            }
        }
        return false;
    };

    var _getOtherRadios = function(formEl, name) {
        return _selector.gets('input[name="' + name + '"]', formEl);
    };

    var _onAfterRender = function() {
        var id = this.config.id;

        this.uiEl = document.getElementById(id + '-ui');
        this.clickEvent = new _fs.events.Click();
        this.clickEvent.attach(this.uiEl, this.handler, this);
        this.changeEvent = new _fs.events.Abstract({
            autoAttach: true,
            eventName: 'radioUncheck',
            el: this.el,
            scope: this,
            handler: this.uncheck
        });
        if (this.config.checked) {
            this.check();
        }
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.RadioButton',
        xtype: 'radiobutton',

        constructor: function(opts) {
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        check: function() {
            if (this.checked) {
                return false;
            }
            this.fire('check', this);
            this.checked = true;
            if (this.el) {
                var item, form = _findParentForm(),
                    radios = _getOtherRadios(form, this.config.name),
                    i = radios.length;

                while (i--) {
                    item = radios[i];
                    if (item.checked && _radioUncheckEvent) {
                        item.dispatchEvent(_radioUncheckEvent);
                    }
                }
                this.el.checked = true;
            }
        },

        uncheck: function() {
            if (!this.checked) {
                return false;
            }
            this.fire('uncheck', this);
            this.checked = false;
            if (this.el) {
                this.el.checked = false;
            }
        },

        handler: function() {
            if (!this.checked) {
                this.check();
            }
        }
    });
})();



Fs.views.Popup = (function() {

    
    var _fs = Fs,
        _debug = _fs.debug,
        _selector = _fs.Selector,
        _parent = _fs.views.Template;

    var _onAfterRender = function(cmp) {
        _debug.log('popup', 'afterrender', arguments, id);
        var id = this.config.id;

        this.overlayEl = document.getElementById(id + '-overlay');
        this.closeEl = document.getElementById(id + '-close');
        if (this.config.overlayHide !== false) {
            this.clickEvent = new _fs.events.Click({
                disableScrolling: true
            });
            this.clickEvent.attach(this.overlayEl, this.hide, this);
        }
        if (this.closeEl) {
            this.closeEvent = new _fs.events.Click({
                disableScrolling: true
            });
            this.closeEvent.attach(this.closeEl, this.hide, this);
        }
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Popup',
        xtype: 'popup',

        constructor: function(opts) {
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        show: function() {
            if (this.el) {
                _selector.removeClass(this.el, 'hidden');
                _selector.removeClass(this.overlayEl, 'hidden');
            }
        },

        hide: function() {
            if (this.el) {
                _selector.addClass(this.el, 'hidden');
                _selector.addClass(this.overlayEl, 'hidden');
            }
        }

    });
})();


Fs.views.Alert = (function() {

    
    var _fs = Fs,
        _debug = _fs.debug,
        _selector = _fs.Selector,
        _parent = _fs.views.Template;

    var _onAfterRender = function(cmp) {
        _debug.log('alert', 'afterrender', arguments, id);
        var id = this.config.id;
        
        this.overlayEl = document.getElementById(id + '-overlay');
        this.closeEl = document.getElementById(id + '-close');
        this.clickEvent = new _fs.events.Click({
            disableScrolling: true
        });
        if (this.config.overlayHide !== false) {
            this.clickEvent.attach(this.overlayEl, this.hide, this);
        } else {
            this.clickEvent.attach(this.overlayEl, function() {}, this);
        }
        if (this.closeEl) {
            this.closeEvent = new _fs.events.Click({
                disableScrolling: true
            });
            this.closeEvent.attach(this.closeEl, this.hide, this);
        }
    };

    var _onAfterCompile = function(cmp, renderer) {
        _debug.log('alert', 'on aftercompile', arguments);
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Alert',
        xtype: 'alert',

        constructor: function(opts) {
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        show: function() {
            if (this.el) {
                _selector.removeClass(this.el, 'hidden');
                _selector.removeClass(this.overlayEl, 'hidden');
            }
            this.fire('show', this);
        },

        hide: function() {
            if (this.el) {
                _selector.addClass(this.el, 'hidden');
                _selector.addClass(this.overlayEl, 'hidden');
            }
            this.fire('hide', this);
        }

    });
})();


Fs.views.Panel = (function() {

    
    var _fs = Fs,
        _selector = _fs.Selector,
        
        _parent = _fs.views.Template;

    var _onAfterRender = function() {
        var id = this.config.id;

        this.overlayEl = document.getElementById(id + '-overlay');
        this.clickEvent = new _fs.events.Click({
            disableScrolling: true
        });
        this.clickEvent.attach(this.overlayEl, function() {
            this.collapse();
        }, this);
    };

    var _onAfterCompile = function(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', _onAfterRender,
            this, this.priority.VIEWS);
    };

    
    return _parent.subclass({

        className: 'Fs.views.Panel',
        xtype: 'panel',

        constructor: function(opts) {
            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        },

        resize: function() {
            var height = window.innerHeight;

            this.el.offsetHeight = height;
            this.el.style.height = height + 'px';
        },

        expand: function() {
            var el = this.el,
                height = window.innerHeight;

            this.expanded = true;
            _selector.removeClass(this.overlayEl, 'hidden');
            this.el.addEventListener('webkitTransitionEnd', function() {
                el.offsetHeight = height;
                el.style.height = height + 'px';
                el.removeEventListener('webkitTransitionEnd',
                    arguments.callee, false);
            }, false);
            if (!this.resizeEvent) {
                this.resizeEvent = new _fs.events.Resize();
            }
            this.resizeEvent.attach(window, this.resize, this);
            this.fire('expand', this);
        },

        collapse: function() {
            this.expanded = false;
            if (this.resizeEvent) {
                this.resizeEvent.detach();
            }
            _selector.addClass(this.overlayEl, 'hidden');
            this.fire('collapse', this);
        }
    });
})();
Fs.views.AddToHome = (function() {

    var _fs = Fs,
        _priority = _fs.Event.prototype.priority,
        _parent = _fs.views.Template;

    function close() {
        if (this.timeout) {
            var timeout = (new Date()).getTime() + (this.timeout * 1000.0);

            _fs.Storage.setItem('addtohome', timeout, window.location.host);
        }
        _fs.Selector.addClass(this.el, 'hidden');
    };

    function afterrender() {
        this.closeEl = this.el.querySelector('.addtohome-close');
        this.clickEvent = new _fs.events.Click({
            autoAttach: true,
            el: this.closeEl,
            scope: this,
            handler: close
        });
    };

    function aftercompile(cmp, renderer) {
        this.off(this.eid);
        this.eid = this.renderer.on('afterrender', afterrender,
            this, _priority.VIEWS);
    };

    function canAddToHome() {
        if (window.navigator.standalone !== true &&
            (/iphone|ipod|ipad/gi).test(navigator.platform) &&
            (/Safari/i).test(navigator.appVersion)) {

            var isTimeout = _fs.Storage.getItem('addtohome', window.location.host),
                now = (new Date()).getTime();

            if (!isTimeout || isTimeout < now) {
                return true;
            }
        }
        return false;
    };

    window.canAddToHome = canAddToHome;

    return _parent.subclass({

        xtype: 'addtohome',
        className: 'Fs.views.AddToHome',

        constructor: function(opts) {
            opts = opts || {};
            this.renderTo = opts.renderTo || null;
            this.timeout = opts.timeout || false;

            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', aftercompile,
                this, _priority.VIEWS);

            if (this.renderTo) {
                this.compile();
                this.render(this.renderTo);
            }
        }

    })

}());


Fs.views.Carousel = (function() {

    
    var _fs = Fs,
        _win = window,
        _touchSlider = _win.touchSlider,
        _parent = _fs.views.Template;

    var _onAfterRender = function() {

        var opts = {
            el: this.el
            
        };

        if (typeof this.height !== 'object') {
            opts.height = this.height;
        }

        if (this.animation) {
            var self = this;

            opts.duration = this.animation;
            opts.afterslide = function() {
                if (self.stopAuto !== true && self.hidden === false) {
                    setTimeout(function() {
                        if (self.stopAuto !== true && self.hidden === false) {
                            self.slider.next();
                        }
                    }, 2000);
                }
            };
            opts.userslide = function() {
                self.stopAuto = true;
            };
        }

        this.slider = _touchSlider(opts);

    };

    var _onShow = function() {
        this.hidden = false;
        this.slider.resume();

        if (this.stopAuto !== true && this.animation) {
            var self = this;
            setTimeout(function() {
                self.slider.next();
            }, 2500);
        }

    };

    var _onHide = function() {
        this.hidden = true;
        this.slider.pause();

    };

    var _onAfterCompile = function(cmp, renderer) {

        var self = this;

        if (self.showEID) {
            self.renderer.off(self.showEID);
        }
        if (self.hideEID) {
            self.renderer.off(self.hideEID);
        }

        self.off(this.eid);

        self.arEID = self.renderer.on('afterrender', _onAfterRender,
            self, self.priority.VIEWS);

        self.showEID = self.renderer.on('show', _onShow,
            self, self.priority.VIEWS);
        self.hideEID = self.renderer.on('hide', _onHide,
            self, self.priority.VIEWS);

    };

    
    return _parent.subclass({

        className: 'Fs.views.Carousel',
        xtype: 'carousel',

        constructor: function(opts) {

            opts = opts || { config: {}};
            opts.config = opts.config || {};

            if (opts.config.animation) {
                this.animation = opts.config.animation;
            }

            if (opts.config.canFullscreen) {
                this.canFullscreen = opts.config.canFullscreen;
            }

            if (opts.config.height) {
                this.height = opts.config.height;
            }

            _parent.prototype.constructor.call(this, opts);

            this.eid = this.on('aftercompile', _onAfterCompile,
                this, this.priority.VIEWS);
        }
    });
})();
!function(){var s=Handlebars.template;Handlebars.templates=Handlebars.templates||{},Handlebars.partials.cssClass=s(function(s,a,e,l,n){function r(s){var a="";return a+=" "+c(typeof s===i?s.apply(s):s)}this.compilerInfo=[4,">= 1.0.0"],e=this.merge(e,s.helpers),n=n||{};var t,p,i="function",c=this.escapeExpression,o=this,f=e.blockHelperMissing;return p={hash:{},inverse:o.noop,fn:o.program(1,r,n),data:n},(t=e.cssClass)?t=t.call(a,p):(t=a.cssClass,t=typeof t===i?t.apply(a):t),e.cssClass||(t=f.call(a,t,p)),t||0===t?t:""})}();!function(){var e=Handlebars.template;Handlebars.templates=Handlebars.templates||{},Handlebars.partials.for_id=e(function(e,a,r,n,t){function s(e){var a="";return a+=' for="'+o(typeof e===p?e.apply(e):e)+'"'}this.compilerInfo=[4,">= 1.0.0"],r=this.merge(r,e.helpers),t=t||{};var i,l,p="function",o=this.escapeExpression,f=this,c=r.blockHelperMissing;return l={hash:{},inverse:f.noop,fn:f.program(1,s,t),data:t},(i=r.id)?i=i.call(a,l):(i=a.id,i=typeof i===p?i.apply(a):i),r.id||(i=c.call(a,i,l)),i||0===i?i:""})}();!function(){var e=Handlebars.template;Handlebars.templates=Handlebars.templates||{},Handlebars.partials.id=e(function(e,a,n,r,t){function i(e){var a="";return a+=' id="'+o(typeof e===p?e.apply(e):e)+'"'}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,e.helpers),t=t||{};var s,l,p="function",o=this.escapeExpression,d=this,c=n.blockHelperMissing;return l={hash:{},inverse:d.noop,fn:d.program(1,i,t),data:t},(s=n.id)?s=s.call(a,l):(s=a.id,s=typeof s===p?s.apply(a):s),n.id||(s=c.call(a,s,l)),s||0===s?s:""})}();!function(){var a=Handlebars.template;Handlebars.templates=Handlebars.templates||{},Handlebars.partials.items=a(function(a,e,n,r,t){function s(a,e){var r;return r=n.safe.call(a,a,{hash:{},inverse:p.noop,fn:p.program(2,l,e),data:e}),r||0===r?r:""}function l(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),t=t||{};var i,o,p=this,m="function",c=n.blockHelperMissing;return o={hash:{},inverse:p.noop,fn:p.program(1,s,t),data:t},(i=n.items)?i=i.call(e,o):(i=e.items,i=typeof i===m?i.apply(e):i),n.items||(i=c.call(e,i,o)),i||0===i?i:""})}();!function(){var e=Handlebars.template;Handlebars.templates=Handlebars.templates||{},Handlebars.partials.style=e(function(e,a,t,s,l){function n(e){var a="";return a+=' style="'+o(typeof e===i?e.apply(e):e)+'"'}this.compilerInfo=[4,">= 1.0.0"],t=this.merge(t,e.helpers),l=l||{};var r,p,i="function",o=this.escapeExpression,c=this,y=t.blockHelperMissing;return p={hash:{},inverse:c.noop,fn:c.program(1,n,l),data:l},(r=t.style)?r=r.call(a,p):(r=a.style,r=typeof r===i?r.apply(a):r),t.style||(r=y.call(a,r,p)),r||0===r?r:""})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.addtohome=a(function(a,e,s,i,d){this.compilerInfo=[4,">= 1.0.0"],s=this.merge(s,a.helpers),i=this.merge(i,a.partials),d=d||{};var t,o="",l=this,n="function",m=this.escapeExpression;return o+='<div class="addtohome"',t=l.invokePartial(i.id,"id",e,s,i,d),(t||0===t)&&(o+=t),o+='><div class="addtohome-popup"><div class="addtohome-icon"><div class="addtohome-icon-image" style="background-image: url(',(t=s.image)?t=t.call(e,{hash:{},data:d}):(t=e.image,t=typeof t===n?t.apply(e):t),o+=m(t)+');">&nbsp;</div>'+"</div>"+'<div class="addtohome-content">',t=l.invokePartial(i.items,"items",e,s,i,d),(t||0===t)&&(o+=t),o+='</div><div class="addtohome-close"><span class="addtohome-closebtn"></span></div></div></div>'})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.alert=a(function(a,e,n,r,l){function o(a,e){var r,l="";return l+=" floatingpanel-overlay-",(r=n.overlay)?r=r.call(a,{hash:{},data:e}):(r=a.overlay,r=typeof r===k?r.apply(a):r),l+=x(r)}function t(){return" floatingpanel-overlay-default"}function i(a){var e="";return e+=' id="'+x(typeof a===k?a.apply(a):a)+'-overlay"'}function p(a){var e="";return e+="ui-corner-"+x(typeof a===k?a.apply(a):a)}function s(a){var e="";return e+=" ui-body-"+x(typeof a===k?a.apply(a):a)}function u(){return" ui-overlay-shadow"}function c(a,e,r){var l,o,t="";return t+='<div class="ui-btn-',(l=n.close)?l=l.call(a,{hash:{},data:e}):(l=a.close,l=typeof l===k?l.apply(a):l),t+=x(l)+" ui-btn",l=r.ui,l=typeof l===k?l.apply(a):l,o=D.call(a,l,{hash:{},inverse:H.noop,fn:H.programWithDepth(14,h,e,a),data:e}),(o||0===o)&&(t+=o),l=r.shadow,l=typeof l===k?l.apply(a):l,o=D.call(a,l,{hash:{},inverse:H.noop,fn:H.program(16,d,e),data:e}),(o||0===o)&&(t+=o),t+=' ui-btn-corner-all ui-btn-icon-notext"',l=r.id,l=typeof l===k?l.apply(a):l,o=D.call(a,l,{hash:{},inverse:H.noop,fn:H.program(18,f,e),data:e}),(o||0===o)&&(t+=o),t+='><span class="ui-btn-inner"><span class="ui-btn-text">Close</span><span class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span></span></div>'}function h(a,e,r){var l,o="";return o+=" ui-btn-up-"+x(n.get.call(a,(l=r.header,null==l||l===!1?l:l.ui),{hash:{"default":a},data:e}))}function d(){return" ui-shadow"}function f(a){var e="";return e+=' id="'+x(typeof a===k?a.apply(a):a)+'-close"'}function v(a,e){var r,l,o,t="";return t+='<div class="ui-corner-top ui-header',o={hash:{},inverse:H.noop,fn:H.programWithDepth(21,y,e,a),data:e},(r=n.ui)?r=r.call(a,o):(r=a.ui,r=typeof r===k?r.apply(a):r),n.ui||(r=D.call(a,r,o)),(r||0===r)&&(t+=r),t+='">',r=a.header,r=null==r||r===!1?r:r.title,r=typeof r===k?r.apply(a):r,l=D.call(a,r,{hash:{},inverse:H.noop,fn:H.program(23,m,e),data:e}),(l||0===l)&&(t+=l),t+="</div>"}function y(a,e,r){var l,o="";return o+=" ui-bar-"+x(n.get.call(a,(l=r.header,null==l||l===!1?l:l.ui),{hash:{"default":a},data:e}))}function m(a){var e="";return e+='<h1 class="ui-title">'+x(typeof a===k?a.apply(a):a)+"</h1>"}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),r=this.merge(r,a.partials),l=l||{};var g,b,w="",k="function",x=this.escapeExpression,H=this,D=n.blockHelperMissing;return w+='<div class="floatingpanel-overlay',g=n["if"].call(e,e.overlay,{hash:{},inverse:H.program(3,t,l),fn:H.program(1,o,l),data:l}),(g||0===g)&&(w+=g),w+='"',b={hash:{},inverse:H.noop,fn:H.program(5,i,l),data:l},(g=n.id)?g=g.call(e,b):(g=e.id,g=typeof g===k?g.apply(e):g),n.id||(g=D.call(e,g,b)),(g||0===g)&&(w+=g),w+='></div><div class="ui-popup-container ui-popup-active"',g=H.invokePartial(r.style,"style",e,n,r,l),(g||0===g)&&(w+=g),g=H.invokePartial(r.id,"id",e,n,r,l),(g||0===g)&&(w+=g),w+='><div class="',b={hash:{},inverse:H.noop,fn:H.program(7,p,l),data:l},(g=n.corner)?g=g.call(e,b):(g=e.corner,g=typeof g===k?g.apply(e):g),n.corner||(g=D.call(e,g,b)),(g||0===g)&&(w+=g),w+=" ui-popup",b={hash:{},inverse:H.noop,fn:H.program(9,s,l),data:l},(g=n.ui)?g=g.call(e,b):(g=e.ui,g=typeof g===k?g.apply(e):g),n.ui||(g=D.call(e,g,b)),(g||0===g)&&(w+=g),b={hash:{},inverse:H.noop,fn:H.program(11,u,l),data:l},(g=n.shadow)?g=g.call(e,b):(g=e.shadow,g=typeof g===k?g.apply(e):g),n.shadow||(g=D.call(e,g,b)),(g||0===g)&&(w+=g),w+='" style="border: 0px !important;">',g=n["if"].call(e,e.close,{hash:{},inverse:H.noop,fn:H.programWithDepth(13,c,l,e),data:l}),(g||0===g)&&(w+=g),g=n["if"].call(e,e.header,{hash:{},inverse:H.noop,fn:H.program(20,v,l),data:l}),(g||0===g)&&(w+=g),w+='<div class="ui-corner-bottom ui-content',b={hash:{},inverse:H.noop,fn:H.program(9,s,l),data:l},(g=n.ui)?g=g.call(e,b):(g=e.ui,g=typeof g===k?g.apply(e):g),n.ui||(g=D.call(e,g,b)),(g||0===g)&&(w+=g),w+='">',g=H.invokePartial(r.items,"items",e,n,r,l),(g||0===g)&&(w+=g),w+="</div></div></div>"})}();!function(){var a=Handlebars.template,n=Handlebars.templates=Handlebars.templates||{};n.button=a(function(a,n,e,r,t){function i(a){var n="";return n+=" ui-btn-up-"+w(typeof a===_?a.apply(a):a)}function o(){return" ui-shadow"}function s(){return" ui-disabled"}function l(){return" ui-btn-corner-all"}function p(){return" ui-mini"}function c(){return" ui-mini ui-btn-icon-notext"}function u(){return" ui-btn-inline"}function f(a,n){var r;return r=e["if"].call(a,a.iconPos,{hash:{},inverse:H.program(18,d,n),fn:H.program(16,h,n),data:n}),r||0===r?r:""}function h(a,n){var r,t="";return t+=" ui-btn-icon-",(r=e.iconPos)?r=r.call(a,{hash:{},data:n}):(r=a.iconPos,r=typeof r===_?r.apply(a):r),t+=w(r)}function d(){return" ui-btn-icon-left"}function m(a){var n="";return n+=' style="'+w(typeof a===_?a.apply(a):a)+'"'}function v(a,n){var r;return(r=e.text)?r=r.call(a,{hash:{},data:n}):(r=a.text,r=typeof r===_?r.apply(a):r),w(r)}function y(){return"&nbsp;"}function g(a,n){var r,t="";return t+='<span class="icon override-inline-btn-icon">',r=e.geticon.call(a,a,{hash:{},inverse:H.noop,fn:H.program(27,b,n),data:n}),(r||0===r)&&(t+=r),t+="</span>"}function b(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],e=this.merge(e,a.helpers),r=this.merge(r,a.partials),t=t||{};var x,q,P="",_="function",w=this.escapeExpression,H=this,k=e.blockHelperMissing;return P+='<a class="ui-btn',q={hash:{},inverse:H.noop,fn:H.program(1,i,t),data:t},(x=e.ui)?x=x.call(n,q):(x=n.ui,x=typeof x===_?x.apply(n):x),e.ui||(x=k.call(n,x,q)),(x||0===x)&&(P+=x),q={hash:{},inverse:H.noop,fn:H.program(3,o,t),data:t},(x=e.shadow)?x=x.call(n,q):(x=n.shadow,x=typeof x===_?x.apply(n):x),e.shadow||(x=k.call(n,x,q)),(x||0===x)&&(P+=x),x=e.if_eq.call(n,n.disabled,{hash:{compare:!0,"default":!1},inverse:H.noop,fn:H.program(5,s,t),data:t}),(x||0===x)&&(P+=x),x=e.if_eq.call(n,n.roundCorners,{hash:{compare:!0,"default":!0},inverse:H.noop,fn:H.program(7,l,t),data:t}),(x||0===x)&&(P+=x),x=e.if_eq.call(n,n.size,{hash:{compare:"medium"},inverse:H.noop,fn:H.program(9,p,t),data:t}),(x||0===x)&&(P+=x),x=e.if_eq.call(n,n.size,{hash:{compare:"small"},inverse:H.noop,fn:H.program(11,c,t),data:t}),(x||0===x)&&(P+=x),x=e.if_eq.call(n,n.inline,{hash:{compare:!0,"default":!0},inverse:H.noop,fn:H.program(13,u,t),data:t}),(x||0===x)&&(P+=x),x=e["if"].call(n,n.icon,{hash:{},inverse:H.noop,fn:H.program(15,f,t),data:t}),(x||0===x)&&(P+=x),x=H.invokePartial(r.cssClass,"cssClass",n,e,r,t),(x||0===x)&&(P+=x),P+='"',q={hash:{},inverse:H.noop,fn:H.program(20,m,t),data:t},(x=e.style)?x=x.call(n,q):(x=n.style,x=typeof x===_?x.apply(n):x),e.style||(x=k.call(n,x,q)),(x||0===x)&&(P+=x),x=H.invokePartial(r.id,"id",n,e,r,t),(x||0===x)&&(P+=x),P+='><span class="ui-btn-inner"><span class="ui-btn-text">',x=e["if"].call(n,n.text,{hash:{},inverse:H.program(24,y,t),fn:H.program(22,v,t),data:t}),(x||0===x)&&(P+=x),P+="</span>",q={hash:{},inverse:H.noop,fn:H.program(26,g,t),data:t},(x=e.icon)?x=x.call(n,q):(x=n.icon,x=typeof x===_?x.apply(n):x),e.icon||(x=k.call(n,x,q)),(x||0===x)&&(P+=x),P+="</span></a>"})}();!function(){var e=Handlebars.template,a=Handlebars.templates=Handlebars.templates||{};a.carousel=e(function(e,a,s,l,i){function r(e){var a="";return a+=' style="'+c(typeof e===d?e.apply(e):e)+'"'}function t(e){var a="";return a+='<div class="slide slide-image" style="background-image: url('+c(typeof e===d?e.apply(e):e)+');"></div>'}this.compilerInfo=[4,">= 1.0.0"],s=this.merge(s,e.helpers),l=this.merge(l,e.partials),i=i||{};var n,p,o="",d="function",c=this.escapeExpression,v=this,f=s.blockHelperMissing;return o+='<div class="slider"',n=v.invokePartial(l.id,"id",a,s,l,i),(n||0===n)&&(o+=n),p={hash:{},inverse:v.noop,fn:v.program(1,r,i),data:i},(n=s.style)?n=n.call(a,p):(n=a.style,n=typeof n===d?n.apply(a):n),s.style||(n=f.call(a,n,p)),(n||0===n)&&(o+=n),o+='><div class="slides">',n=s.each.call(a,a.images,{hash:{},inverse:v.noop,fn:v.program(3,t,i),data:i}),(n||0===n)&&(o+=n),o+="</div></div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.checkbox=a(function(a,e,n,i,l){function r(){return"on"}function t(){return"off"}function p(a){var e="";return e+=" ui-btn-up-"+v(typeof a===b?a.apply(a):a)}function o(){return" ui-mini"}function s(){return" ui-fullsize"}function c(a){var e="";return e+=' id="'+v(typeof a===b?a.apply(a):a)+'-ui"'}function u(a){return v(typeof a===b?a.apply(a):a)}function f(a){var e="";return e+=' name="'+v(typeof a===b?a.apply(a):a)+'"'}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),i=this.merge(i,a.partials),l=l||{};var m,h,d="",b="function",v=this.escapeExpression,y=this,g=n.blockHelperMissing;return d+='<div class="ui-checkbox"><label class="ui-checkbox-',m=n.if_eq.call(e,e.value,{hash:{compare:!0,"default":!1},inverse:y.program(3,t,l),fn:y.program(1,r,l),data:l}),(m||0===m)&&(d+=m),d+=" ui-btn",h={hash:{},inverse:y.noop,fn:y.program(5,p,l),data:l},(m=n.ui)?m=m.call(e,h):(m=e.ui,m=typeof m===b?m.apply(e):m),n.ui||(m=g.call(e,m,h)),(m||0===m)&&(d+=m),d+=" ui-btn-corner-all",m=n.if_eq.call(e,e.size,{hash:{compare:"small","default":"medium"},inverse:y.program(9,s,l),fn:y.program(7,o,l),data:l}),(m||0===m)&&(d+=m),d+=' ui-btn-icon-left"',h={hash:{},inverse:y.noop,fn:y.program(11,c,l),data:l},(m=n.id)?m=m.call(e,h):(m=e.id,m=typeof m===b?m.apply(e):m),n.id||(m=g.call(e,m,h)),(m||0===m)&&(d+=m),d+='><span class="ui-btn-inner"><span class="ui-btn-text">',h={hash:{},inverse:y.noop,fn:y.program(13,u,l),data:l},(m=n.label)?m=m.call(e,h):(m=e.label,m=typeof m===b?m.apply(e):m),n.label||(m=g.call(e,m,h)),(m||0===m)&&(d+=m),d+='</span><span class="ui-icon ui-icon-checkbox-',m=n.if_eq.call(e,e.value,{hash:{compare:!0,"default":!1},inverse:y.program(3,t,l),fn:y.program(1,r,l),data:l}),(m||0===m)&&(d+=m),d+=' ui-icon-shadow">&nbsp;</span></span></label><input type="checkbox"',h={hash:{},inverse:y.noop,fn:y.program(15,f,l),data:l},(m=n.name)?m=m.call(e,h):(m=e.name,m=typeof m===b?m.apply(e):m),n.name||(m=g.call(e,m,h)),(m||0===m)&&(d+=m),m=y.invokePartial(i.id,"id",e,n,i,l),(m||0===m)&&(d+=m),d+="></div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.collapsible=a(function(a,e,n,l,i){function r(){var a="";return a+=" ui-collapsible-inset"}function t(a){var e="";return e+=" ui-corner-"+E(typeof a===z?a.apply(a):a)}function o(){var a="";return a+=" ui-collapsible-collapsed"}function p(){var a="";return a+=" ui-collapsible-heading-collapsed"}function s(a){var e="";return e+=' id="'+E(typeof a===z?a.apply(a):a)+'-header"'}function c(){var a="";return a+=" ui-mini"}function u(){var a="";return a+=" ui-fullsize"}function f(a){var e,n="";return n+=" ui-btn-up-"+E((e=a.header,e=null==e||e===!1?e:e.ui,typeof e===z?e.apply(a):e))}function d(a,e){var l,i,r="";return i={hash:{},inverse:j.noop,fn:j.program(18,h,e),data:e},(l=n.ui)?l=l.call(a,i):(l=a.ui,l=typeof l===z?l.apply(a):l),n.ui||(l=w.call(a,l,i)),(l||0===l)&&(r+=l),r}function h(a){var e="";return e+=" ui-btn-up-"+E(typeof a===z?a.apply(a):a)}function v(a,e){var l="";return l+=E(n.get.call(a,a.iconCollapsed,{hash:{"default":"plus"},data:e}))}function m(a,e){var l="";return l+=E(n.get.call(a,a.iconExpanded,{hash:{"default":"minus"},data:e}))}function g(a){var e,n="";return n+=" ui-body-"+E((e=a.content,e=null==e||e===!1?e:e.ui,typeof e===z?e.apply(a):e))}function y(a,e){var l,i,r="";return i={hash:{},inverse:j.noop,fn:j.program(27,b,e),data:e},(l=n.ui)?l=l.call(a,i):(l=a.ui,l=typeof l===z?l.apply(a):l),n.ui||(l=w.call(a,l,i)),(l||0===l)&&(r+=l),r}function b(a){var e="";return e+=" ui-body-"+E(typeof a===z?a.apply(a):a)}function q(){var a="";return a+=" ui-collapsible-content-collapsed"}function _(a){var e="";return e+=' id="'+E(typeof a===z?a.apply(a):a)+'-content"'}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),l=this.merge(l,a.partials),i=i||{};var H,k,x,P="",z="function",E=this.escapeExpression,j=this,w=n.blockHelperMissing;return P+='<div class="ui-collapsible',H=n.if_eq.call(e,e.inset,{hash:{compare:!0,"default":!1},inverse:j.noop,fn:j.program(1,r,i),data:i}),(H||0===H)&&(P+=H),x={hash:{},inverse:j.noop,fn:j.program(3,t,i),data:i},(H=n.corner)?H=H.call(e,x):(H=e.corner,H=typeof H===z?H.apply(e):H),n.corner||(H=w.call(e,H,x)),(H||0===H)&&(P+=H),P+=" ui-collapsible-themed-content",H=n.if_eq.call(e,e.collapsed,{hash:{compare:!0,"default":!1},inverse:j.noop,fn:j.program(5,o,i),data:i}),(H||0===H)&&(P+=H),P+='"',H=j.invokePartial(l.id,"id",e,n,l,i),(H||0===H)&&(P+=H),P+='><h4 class="ui-collapsible-heading',H=n.if_eq.call(e,e.collapsed,{hash:{compare:!0,"default":!1},inverse:j.noop,fn:j.program(7,p,i),data:i}),(H||0===H)&&(P+=H),P+='"',x={hash:{},inverse:j.noop,fn:j.program(9,s,i),data:i},(H=n.id)?H=H.call(e,x):(H=e.id,H=typeof H===z?H.apply(e):H),n.id||(H=w.call(e,H,x)),(H||0===H)&&(P+=H),P+='><a href="javascript:;" class="ui-collapsible-heading-toggle ui-btn',H=n.if_eq.call(e,e.size,{hash:{compare:"small","default":"medium"},inverse:j.program(13,u,i),fn:j.program(11,c,i),data:i}),(H||0===H)&&(P+=H),k=n["if"].call(e,(H=e.header,null==H||H===!1?H:H.ui),{hash:{},inverse:j.program(17,d,i),fn:j.program(15,f,i),data:i}),(k||0===k)&&(P+=k),P+=" ui-btn-icon-"+E(n.get.call(e,e.iconPos,{hash:{"default":"left",choices:"left|right"},data:i}))+'">'+'<span class="ui-btn-inner">'+'<span class="ui-btn-text">'+E((H=e.header,H=null==H||H===!1?H:H.title,typeof H===z?H.apply(e):H))+"</span>"+'<span class="ui-icon ui-icon-shadow'+" ui-icon-",k=n["if"].call(e,e.collapsed,{hash:{},inverse:j.program(22,m,i),fn:j.program(20,v,i),data:i}),(k||0===k)&&(P+=k),P+='">&nbsp;</span></span></a></h4><div class="ui-collapsible-content',k=n["if"].call(e,(H=e.content,null==H||H===!1?H:H.ui),{hash:{},inverse:j.program(26,y,i),fn:j.program(24,g,i),data:i}),(k||0===k)&&(P+=k),k=n.if_eq.call(e,e.collapsed,{hash:{compare:!0,"default":!1},inverse:j.noop,fn:j.program(29,q,i),data:i}),(k||0===k)&&(P+=k),P+='"',x={hash:{},inverse:j.noop,fn:j.program(31,_,i),data:i},(k=n.id)?k=k.call(e,x):(k=e.id,k=typeof k===z?k.apply(e):k),n.id||(k=w.call(e,k,x)),(k||0===k)&&(P+=k),P+=">",k=j.invokePartial(l.items,"items",e,n,l,i),(k||0===k)&&(P+=k),P+="</div></div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.container=a(function(a,e,n,r,s){function t(){return" hidden"}function l(a){var e="";return e+=" "+u(typeof a===y?a.apply(a):a)}function p(a){var e="";return e+=' style="'+u(typeof a===y?a.apply(a):a)+'"'}function o(a){var e="";return e+=' id="'+u(typeof a===y?a.apply(a):a)+'"'}function i(a,e){var r,s="";return r=n.safe.call(a,a,{hash:{},inverse:v.noop,fn:v.program(10,c,e),data:e}),(r||0===r)&&(s+=r),s}function c(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),s=s||{};var f,d,h="",y="function",u=this.escapeExpression,v=this,m=n.blockHelperMissing;return h+='<div class="container',d={hash:{},inverse:v.noop,fn:v.program(1,t,s),data:s},(f=n.hidden)?f=f.call(e,d):(f=e.hidden,f=typeof f===y?f.apply(e):f),n.hidden||(f=m.call(e,f,d)),(f||0===f)&&(h+=f),d={hash:{},inverse:v.noop,fn:v.program(3,l,s),data:s},(f=n.cssClass)?f=f.call(e,d):(f=e.cssClass,f=typeof f===y?f.apply(e):f),n.cssClass||(f=m.call(e,f,d)),(f||0===f)&&(h+=f),h+='"',d={hash:{},inverse:v.noop,fn:v.program(5,p,s),data:s},(f=n.style)?f=f.call(e,d):(f=e.style,f=typeof f===y?f.apply(e):f),n.style||(f=m.call(e,f,d)),(f||0===f)&&(h+=f),d={hash:{},inverse:v.noop,fn:v.program(7,o,s),data:s},(f=n.id)?f=f.call(e,d):(f=e.id,f=typeof f===y?f.apply(e):f),n.id||(f=m.call(e,f,d)),(f||0===f)&&(h+=f),h+=">",d={hash:{},inverse:v.noop,fn:v.program(9,i,s),data:s},(f=n.items)?f=f.call(e,d):(f=e.items,f=typeof f===y?f.apply(e):f),n.items||(f=m.call(e,f,d)),(f||0===f)&&(h+=f),h+="</div>"})}();!function(){var e=Handlebars.template,t=Handlebars.templates=Handlebars.templates||{};t.content=e(function(e,t,a,n,l){function s(e){var t="";return t+=' style="'+f(typeof e===c?e.apply(e):e)+'"'}function i(e){var t="";return t+=' id="'+f(typeof e===c?e.apply(e):e)+'"'}this.compilerInfo=[4,">= 1.0.0"],a=this.merge(a,e.helpers),n=this.merge(n,e.partials),l=l||{};var r,p,o="",c="function",f=this.escapeExpression,y=this,d=a.blockHelperMissing;return o+='<div class="ui-content"',p={hash:{},inverse:y.noop,fn:y.program(1,s,l),data:l},(r=a.style)?r=r.call(t,p):(r=t.style,r=typeof r===c?r.apply(t):r),a.style||(r=d.call(t,r,p)),(r||0===r)&&(o+=r),p={hash:{},inverse:y.noop,fn:y.program(3,i,l),data:l},(r=a.id)?r=r.call(t,p):(r=t.id,r=typeof r===c?r.apply(t):r),a.id||(r=d.call(t,r,p)),(r||0===r)&&(o+=r),o+=">",r=y.invokePartial(n.items,"items",t,a,n,l),(r||0===r)&&(o+=r),o+="</div>"})}();!function(){var n=Handlebars.template,a=Handlebars.templates=Handlebars.templates||{};a.controlgroup=n(function(n,a,o,r,i){function t(n,a){var r;return(r=o.position)?r=r.call(n,{hash:{},data:a}):(r=n.position,r=typeof r===x?r.apply(n):r),z(r)}function e(){return"horizontal"}function s(){return" ui-mini"}function l(n,a,r){var i,t,e,s="";return s+='<a class="ui-btn ',i=r.ui,i=typeof i===x?i.apply(n):i,t=P.call(n,i,{hash:{},inverse:H.noop,fn:H.program(8,p,a),data:a}),(t||0===t)&&(s+=t),s+=" ui-shadow ui-btn-corner-all",t=o["if"].call(n,n.icon,{hash:{},inverse:H.noop,fn:H.program(10,c,a),data:a}),(t||0===t)&&(s+=t),t=o.if_eq.call(n,r.size,{hash:{compare:"small"},inverse:H.noop,fn:H.program(13,f,a),data:a}),(t||0===t)&&(s+=t),t=o["if"].call(n,n.$first,{hash:{},inverse:H.noop,fn:H.program(15,h,a),data:a}),(t||0===t)&&(s+=t),t=o["if"].call(n,n.$last,{hash:{},inverse:H.noop,fn:H.program(17,d,a),data:a}),(t||0===t)&&(s+=t),s+='"><span class="ui-btn-inner">',e={hash:{},inverse:H.noop,fn:H.program(19,v,a),data:a},(t=o.text)?t=t.call(n,e):(t=n.text,t=typeof t===x?t.apply(n):t),o.text||(t=P.call(n,t,e)),(t||0===t)&&(s+=t),e={hash:{},inverse:H.noop,fn:H.program(21,m,a),data:a},(t=o.icon)?t=t.call(n,e):(t=n.icon,t=typeof t===x?t.apply(n):t),o.icon||(t=P.call(n,t,e)),(t||0===t)&&(s+=t),s+="</span></a>"}function p(n){var a="";return a+="ui-btn-up-"+z(typeof n===x?n.apply(n):n)}function c(n,a){var r;return r=o["if"].call(n,n.iconPos,{hash:{},inverse:H.noop,fn:H.program(11,u,a),data:a}),r||0===r?r:""}function u(n,a){var r,i="";return i+=" ui-btn-icon-",(r=o.iconPos)?r=r.call(n,{hash:{},data:a}):(r=n.iconPos,r=typeof r===x?r.apply(n):r),i+=z(r)}function f(){return" ui-btn-icon-notext"}function h(){return" ui-first-child"}function d(){return" ui-last-child"}function v(n){var a="";return a+='<span class="ui-btn-text">'+z(typeof n===x?n.apply(n):n)+"</span>"}function m(n,a){var r,i="";return i+='<span class="ui-icon override-btn-icon icon">',r=o.geticon.call(n,n,{hash:{},inverse:H.noop,fn:H.program(22,g,a),data:a}),(r||0===r)&&(i+=r),i+="</span>"}function g(){var n="";return n}this.compilerInfo=[4,">= 1.0.0"],o=this.merge(o,n.helpers),r=this.merge(r,n.partials),i=i||{};var y,b="",x="function",z=this.escapeExpression,H=this,P=o.blockHelperMissing;return b+='<div class="ui-corner-all ui-controlgroup ui-controlgroup-',y=o["if"].call(a,a.position,{hash:{},inverse:H.program(3,e,i),fn:H.program(1,t,i),data:i}),(y||0===y)&&(b+=y),y=o.if_eq.call(a,a.size,{hash:{compare:"medium"},inverse:H.noop,fn:H.program(5,s,i),data:i}),(y||0===y)&&(b+=y),y=o.if_eq.call(a,a.size,{hash:{compare:"small"},inverse:H.noop,fn:H.program(5,s,i),data:i}),(y||0===y)&&(b+=y),b+='"',y=H.invokePartial(r.id,"id",a,o,r,i),(y||0===y)&&(b+=y),b+='><div class="ui-controlgroup-controls">',y=o.foreach.call(a,a.buttons,{hash:{},inverse:H.noop,fn:H.programWithDepth(7,l,i,a),data:i}),(y||0===y)&&(b+=y),b+="</div></div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.flipswitch=a(function(a,e,n,i,l){function r(a,e){var l,r="";return r+='<label class="ui-slider"',l=H.invokePartial(i.for_id,"for_id",a,n,i,e),(l||0===l)&&(r+=l),r+=">"+q(typeof a===k?a.apply(a):a)+"</label>"}function t(a){var e="";return e+=' name="'+q(typeof a===k?a.apply(a):a)+'"'}function p(a,e){var i,l="";return l+='<option value="',(i=n.value)?i=i.call(a,{hash:{},data:e}):(i=a.value,i=typeof i===k?i.apply(a):i),l+=q(i)+'">',(i=n.label)?i=i.call(a,{hash:{},data:e}):(i=a.label,i=typeof i===k?i.apply(a):i),l+=q(i)+"</option>"}function o(a){var e="";return e+=' id="'+q(typeof a===k?a.apply(a):a)+'-ui"'}function s(a){var e="";return e+=" ui-btn-down-"+q(typeof a===k?a.apply(a):a)}function c(){return" ui-mini"}function u(a,e,i){var l,r="";return r+='<span class="ui-slider-label ui-slider-label-',l=n.if_eq.call(a,a.$index,{hash:{compare:0},inverse:H.program(16,d,e),fn:H.program(14,f,e),data:e}),(l||0===l)&&(r+=l),l=n.if_eq.call(a,a.$index,{hash:{compare:1},inverse:H.program(20,v,e),fn:H.program(18,h,e),data:e}),(l||0===l)&&(r+=l),r+=' ui-btn-corner-all" style="width: ',l=n.isFlipSwitchOptionActive.call(a,i.options,a.$index,i.value,{hash:{},inverse:H.program(24,y,e),fn:H.program(22,m,e),data:e}),(l||0===l)&&(r+=l),r+=';">',(l=n.label)?l=l.call(a,{hash:{},data:e}):(l=a.label,l=typeof l===k?l.apply(a):l),r+=q(l)+"</span>"}function f(){return"b"}function d(){return"a"}function h(){var a="";return a+=" ui-btn-active"}function v(){var a="";return a+=" ui-btn-down-c"}function m(){var a="";return a+="100%"}function y(){var a="";return a+="0%"}function b(a){var e="";return e+=" ui-btn-up-"+q(typeof a===k?a.apply(a):a)}function g(a){var e="";return e+=' id="'+q(typeof a===k?a.apply(a):a)+'-btn"'}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),i=this.merge(i,a.partials),l=l||{};var w,x,_="",H=this,k="function",q=this.escapeExpression,$=n.blockHelperMissing;return x={hash:{},inverse:H.noop,fn:H.program(1,r,l),data:l},(w=n.label)?w=w.call(e,x):(w=e.label,w=typeof w===k?w.apply(e):w),n.label||(w=$.call(e,w,x)),(w||0===w)&&(_+=w),_+="<select",x={hash:{},inverse:H.noop,fn:H.program(3,t,l),data:l},(w=n.name)?w=w.call(e,x):(w=e.name,w=typeof w===k?w.apply(e):w),n.name||(w=$.call(e,w,x)),(w||0===w)&&(_+=w),w=H.invokePartial(i.id,"id",e,n,i,l),(w||0===w)&&(_+=w),_+=' class="ui-slider-switch">',w=n.each.call(e,e.options,{hash:{},inverse:H.noop,fn:H.program(5,p,l),data:l}),(w||0===w)&&(_+=w),_+="</select><div ",x={hash:{},inverse:H.noop,fn:H.program(7,o,l),data:l},(w=n.id)?w=w.call(e,x):(w=e.id,w=typeof w===k?w.apply(e):w),n.id||(w=$.call(e,w,x)),(w||0===w)&&(_+=w),_+='class="ui-slider ui-slider-switch',x={hash:{},inverse:H.noop,fn:H.program(9,s,l),data:l},(w=n.ui)?w=w.call(e,x):(w=e.ui,w=typeof w===k?w.apply(e):w),n.ui||(w=$.call(e,w,x)),(w||0===w)&&(_+=w),w=n.if_eq.call(e,e.size,{hash:{compare:"small","default":"medium"},inverse:H.noop,fn:H.program(11,c,l),data:l}),(w||0===w)&&(_+=w),_+=' ui-btn-corner-all">',w=n.foreach.call(e,e.options,{hash:{},inverse:H.noop,fn:H.programWithDepth(13,u,l,e),data:l}),(w||0===w)&&(_+=w),_+='<div class="ui-slider-inneroffset"><a href="javascript:;" class="ui-slider-handle ui-slider-handle-snapping ui-btn',x={hash:{},inverse:H.noop,fn:H.program(26,b,l),data:l},(w=n.ui)?w=w.call(e,x):(w=e.ui,w=typeof w===k?w.apply(e):w),n.ui||(w=$.call(e,w,x)),(w||0===w)&&(_+=w),_+='ui-shadow ui-btn-corner-all"',x={hash:{},inverse:H.noop,fn:H.program(28,g,l),data:l},(w=n.id)?w=w.call(e,x):(w=e.id,w=typeof w===k?w.apply(e):w),n.id||(w=$.call(e,w,x)),(w||0===w)&&(_+=w),_+='style="left: ',w=n.isFlipSwitchOptionActive.call(e,e.options,0,e.value,{hash:{},inverse:H.program(22,m,l),fn:H.program(24,y,l),data:l}),(w||0===w)&&(_+=w),_+=';"><span class="ui-btn-inner"><span class="ui-btn-text"></span></span></a></div></div>'})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.floatingpanel=a(function(a,e,n,l,r){function o(){return" hidden"}function p(a,e){var l,r="";return r+=" floatingpanel-overlay-",(l=n.overlay)?l=l.call(a,{hash:{},data:e}):(l=a.overlay,l=typeof l===m?l.apply(a):l),r+=b(l)}function t(){return" floatingpanel-overlay-default"}function i(a){var e="";return e+=' id="'+b(typeof a===m?a.apply(a):a)+'-overlay"'}function s(a){var e="";return e+=" arrow-"+b(typeof a===m?a.apply(a):a)}function f(a){var e="";return e+=" floatingpanel-"+b(typeof a===m?a.apply(a):a)}function d(a){var e="";return e+=' style="'+b(typeof a===m?a.apply(a):a)+'"'}function y(a){var e="";return e+=' id="'+b(typeof a===m?a.apply(a):a)+'"'}function c(a,e){var l,r="";return l=n.safe.call(a,a,{hash:{},inverse:w.noop,fn:w.program(18,h,e),data:e}),(l||0===l)&&(r+=l),r}function h(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),r=r||{};var v,u,g="",m="function",b=this.escapeExpression,w=this,H=n.blockHelperMissing;return g+='<div class="floatingpanel-overlay',u={hash:{},inverse:w.noop,fn:w.program(1,o,r),data:r},(v=n.hidden)?v=v.call(e,u):(v=e.hidden,v=typeof v===m?v.apply(e):v),n.hidden||(v=H.call(e,v,u)),(v||0===v)&&(g+=v),v=n["if"].call(e,e.overlay,{hash:{},inverse:w.program(5,t,r),fn:w.program(3,p,r),data:r}),(v||0===v)&&(g+=v),g+='"',u={hash:{},inverse:w.noop,fn:w.program(7,i,r),data:r},(v=n.id)?v=v.call(e,u):(v=e.id,v=typeof v===m?v.apply(e):v),n.id||(v=H.call(e,v,u)),(v||0===v)&&(g+=v),g+='></div><div class="floatingpanel',u={hash:{},inverse:w.noop,fn:w.program(9,s,r),data:r},(v=n.arrow)?v=v.call(e,u):(v=e.arrow,v=typeof v===m?v.apply(e):v),n.arrow||(v=H.call(e,v,u)),(v||0===v)&&(g+=v),u={hash:{},inverse:w.noop,fn:w.program(1,o,r),data:r},(v=n.hidden)?v=v.call(e,u):(v=e.hidden,v=typeof v===m?v.apply(e):v),n.hidden||(v=H.call(e,v,u)),(v||0===v)&&(g+=v),u={hash:{},inverse:w.noop,fn:w.program(11,f,r),data:r},(v=n.ui)?v=v.call(e,u):(v=e.ui,v=typeof v===m?v.apply(e):v),n.ui||(v=H.call(e,v,u)),(v||0===v)&&(g+=v),g+='"',u={hash:{},inverse:w.noop,fn:w.program(13,d,r),data:r},(v=n.style)?v=v.call(e,u):(v=e.style,v=typeof v===m?v.apply(e):v),n.style||(v=H.call(e,v,u)),(v||0===v)&&(g+=v),u={hash:{},inverse:w.noop,fn:w.program(15,y,r),data:r},(v=n.id)?v=v.call(e,u):(v=e.id,v=typeof v===m?v.apply(e):v),n.id||(v=H.call(e,v,u)),(v||0===v)&&(g+=v),g+=">",u={hash:{},inverse:w.noop,fn:w.program(17,c,r),data:r},(v=n.items)?v=v.call(e,u):(v=e.items,v=typeof v===m?v.apply(e):v),n.items||(v=H.call(e,v,u)),(v||0===v)&&(g+=v),g+="</div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.footer=a(function(a,e,t,n,o){function p(a){var e="";return e+=" ui-bar-"+d(typeof a===v?a.apply(a):a)}function r(a){var e="";return e+=" "+d(typeof a===v?a.apply(a):a)}function l(a){var e="";return e+=" ui-footer-"+d(typeof a===v?a.apply(a):a)}function s(a){var e="";return e+=' style="'+d(typeof a===v?a.apply(a):a)+'"'}function i(a){var e="";return e+=' id="'+d(typeof a===v?a.apply(a):a)+'"'}function f(a,e){var n,o="";return n=t.safe.call(a,a,{hash:{},inverse:m.noop,fn:m.program(12,c,e),data:e}),(n||0===n)&&(o+=n),o}function c(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],t=this.merge(t,a.helpers),o=o||{};var y,u,h="",v="function",d=this.escapeExpression,m=this,g=t.blockHelperMissing;return h+='<div class="ui-footer',u={hash:{},inverse:m.noop,fn:m.program(1,p,o),data:o},(y=t.ui)?y=y.call(e,u):(y=e.ui,y=typeof y===v?y.apply(e):y),t.ui||(y=g.call(e,y,u)),(y||0===y)&&(h+=y),u={hash:{},inverse:m.noop,fn:m.program(3,r,o),data:o},(y=t.cssClass)?y=y.call(e,u):(y=e.cssClass,y=typeof y===v?y.apply(e):y),t.cssClass||(y=g.call(e,y,u)),(y||0===y)&&(h+=y),u={hash:{},inverse:m.noop,fn:m.program(5,l,o),data:o},(y=t.position)?y=y.call(e,u):(y=e.position,y=typeof y===v?y.apply(e):y),t.position||(y=g.call(e,y,u)),(y||0===y)&&(h+=y),h+='"',u={hash:{},inverse:m.noop,fn:m.program(7,s,o),data:o},(y=t.style)?y=y.call(e,u):(y=e.style,y=typeof y===v?y.apply(e):y),t.style||(y=g.call(e,y,u)),(y||0===y)&&(h+=y),u={hash:{},inverse:m.noop,fn:m.program(9,i,o),data:o},(y=t.id)?y=y.call(e,u):(y=e.id,y=typeof y===v?y.apply(e):y),t.id||(y=g.call(e,y,u)),(y||0===y)&&(h+=y),h+=">",u={hash:{},inverse:m.noop,fn:m.program(11,f,o),data:o},(y=t.items)?y=y.call(e,u):(y=e.items,y=typeof y===v?y.apply(e):y),t.items||(y=g.call(e,y,u)),(y||0===y)&&(h+=y),h+="</div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.header=a(function(a,e,t,l,p){function n(a){var e="";return e+=" ui-bar-"+m(typeof a===v?a.apply(a):a)}function r(a){var e="";return e+=" "+m(typeof a===v?a.apply(a):a)}function o(a){var e="";return e+=" ui-header-"+m(typeof a===v?a.apply(a):a)}function s(a){var e="";return e+=' style="'+m(typeof a===v?a.apply(a):a)+'"'}function i(a){var e="";return e+=' id="'+m(typeof a===v?a.apply(a):a)+'"'}function c(a){var e="";return e+='<h1 class="ui-title">'+m(typeof a===v?a.apply(a):a)+"</h1>"}function f(a,e){var l,p="";return l=t.safe.call(a,a,{hash:{},inverse:g.noop,fn:g.program(14,y,e),data:e}),(l||0===l)&&(p+=l),p}function y(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],t=this.merge(t,a.helpers),p=p||{};var u,h,d="",v="function",m=this.escapeExpression,g=this,b=t.blockHelperMissing;return d+='<div class="ui-header',h={hash:{},inverse:g.noop,fn:g.program(1,n,p),data:p},(u=t.ui)?u=u.call(e,h):(u=e.ui,u=typeof u===v?u.apply(e):u),t.ui||(u=b.call(e,u,h)),(u||0===u)&&(d+=u),h={hash:{},inverse:g.noop,fn:g.program(3,r,p),data:p},(u=t.cssClass)?u=u.call(e,h):(u=e.cssClass,u=typeof u===v?u.apply(e):u),t.cssClass||(u=b.call(e,u,h)),(u||0===u)&&(d+=u),h={hash:{},inverse:g.noop,fn:g.program(5,o,p),data:p},(u=t.position)?u=u.call(e,h):(u=e.position,u=typeof u===v?u.apply(e):u),t.position||(u=b.call(e,u,h)),(u||0===u)&&(d+=u),d+='"',h={hash:{},inverse:g.noop,fn:g.program(7,s,p),data:p},(u=t.style)?u=u.call(e,h):(u=e.style,u=typeof u===v?u.apply(e):u),t.style||(u=b.call(e,u,h)),(u||0===u)&&(d+=u),h={hash:{},inverse:g.noop,fn:g.program(9,i,p),data:p},(u=t.id)?u=u.call(e,h):(u=e.id,u=typeof u===v?u.apply(e):u),t.id||(u=b.call(e,u,h)),(u||0===u)&&(d+=u),d+=">",h={hash:{},inverse:g.noop,fn:g.program(11,c,p),data:p},(u=t.title)?u=u.call(e,h):(u=e.title,u=typeof u===v?u.apply(e):u),t.title||(u=b.call(e,u,h)),(u||0===u)&&(d+=u),h={hash:{},inverse:g.noop,fn:g.program(13,f,p),data:p},(u=t.items)?u=u.call(e,h):(u=e.items,u=typeof u===v?u.apply(e):u),t.items||(u=b.call(e,u,h)),(u||0===u)&&(d+=u),d+="</div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.list=a(function(a,e,l,r,n){function s(){return" ui-listview-inset"}function t(a){var e="";return e+=" ui-corner-"+m(typeof a===v?a.apply(a):a)}function o(){return" ui-shadow"}function i(a){var e="";return e+=" "+m(typeof a===v?a.apply(a):a)}function p(a){var e="";return e+=' style="'+m(typeof a===v?a.apply(a):a)+'"'}function c(a){var e="";return e+=' id="'+m(typeof a===v?a.apply(a):a)+'-loader"'}function f(){return" list-loader-hide"}function d(a){var e="";return e+=" list-loader-"+m(typeof a===v?a.apply(a):a)}this.compilerInfo=[4,">= 1.0.0"],l=this.merge(l,a.helpers),r=this.merge(r,a.partials),n=n||{};var u,y,h="",v="function",m=this.escapeExpression,g=this,w=l.blockHelperMissing;return h+='<ul class="ui-listview',y={hash:{},inverse:g.noop,fn:g.program(1,s,n),data:n},(u=l.inset)?u=u.call(e,y):(u=e.inset,u=typeof u===v?u.apply(e):u),l.inset||(u=w.call(e,u,y)),(u||0===u)&&(h+=u),y={hash:{},inverse:g.noop,fn:g.program(3,t,n),data:n},(u=l.corner)?u=u.call(e,y):(u=e.corner,u=typeof u===v?u.apply(e):u),l.corner||(u=w.call(e,u,y)),(u||0===u)&&(h+=u),y={hash:{},inverse:g.noop,fn:g.program(5,o,n),data:n},(u=l.shadow)?u=u.call(e,y):(u=e.shadow,u=typeof u===v?u.apply(e):u),l.shadow||(u=w.call(e,u,y)),(u||0===u)&&(h+=u),y={hash:{},inverse:g.noop,fn:g.program(7,i,n),data:n},(u=l.cssClass)?u=u.call(e,y):(u=e.cssClass,u=typeof u===v?u.apply(e):u),l.cssClass||(u=w.call(e,u,y)),(u||0===u)&&(h+=u),h+='"',y={hash:{},inverse:g.noop,fn:g.program(9,p,n),data:n},(u=l.style)?u=u.call(e,y):(u=e.style,u=typeof u===v?u.apply(e):u),l.style||(u=w.call(e,u,y)),(u||0===u)&&(h+=u),u=g.invokePartial(r.id,"id",e,l,r,n),(u||0===u)&&(h+=u),h+="></ul><div ",y={hash:{},inverse:g.noop,fn:g.program(11,c,n),data:n},(u=l.id)?u=u.call(e,y):(u=e.id,u=typeof u===v?u.apply(e):u),l.id||(u=w.call(e,u,y)),(u||0===u)&&(h+=u),h+=' class="list-loader',u=l.if_eq.call(e,e.isLoading,{hash:{compare:!1,defaults:!0},inverse:g.noop,fn:g.program(13,f,n),data:n}),(u||0===u)&&(h+=u),y={hash:{},inverse:g.noop,fn:g.program(15,d,n),data:n},(u=l.ui)?u=u.call(e,y):(u=e.ui,u=typeof u===v?u.apply(e):u),l.ui||(u=w.call(e,u,y)),(u||0===u)&&(h+=u),h+='"><a class="list-loader-text">Loading...</a></div>'})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.listbuffered=a(function(a,e,l,n,r){function t(){return" ui-listview-inset"}function s(a){var e="";return e+=" ui-listview-corner-"+b(typeof a===g?a.apply(a):a)}function o(){return" ui-shadow"}function p(a){var e="";return e+=" "+b(typeof a===g?a.apply(a):a)}function i(a){var e="";return e+=' style="'+b(typeof a===g?a.apply(a):a)+'"'}function c(a){var e="";return e+=' id="'+b(typeof a===g?a.apply(a):a)+'"'}function f(a,e,n){var r,t,s="";return s+='<li class="ui-btn ui-li',r=n.ui,r=typeof r===g?r.apply(a):r,t=H.call(a,r,{hash:{},inverse:w.noop,fn:w.program(14,u,e),data:e}),(t||0===t)&&(s+=t),s+='">',t=l.safe.call(a,a,{hash:{},inverse:w.noop,fn:w.program(16,h,e),data:e}),(t||0===t)&&(s+=t),s+="</li>"}function u(a){var e="";return e+=" ui-btn-up-"+b(typeof a===g?a.apply(a):a)}function h(){var a="";return a}function y(a){var e="";return e+=" list-loader-"+b(typeof a===g?a.apply(a):a)}this.compilerInfo=[4,">= 1.0.0"],l=this.merge(l,a.helpers),r=r||{};var d,v,m="",g="function",b=this.escapeExpression,w=this,H=l.blockHelperMissing;return m+='<ul class="ui-listview',v={hash:{},inverse:w.noop,fn:w.program(1,t,r),data:r},(d=l.inset)?d=d.call(e,v):(d=e.inset,d=typeof d===g?d.apply(e):d),l.inset||(d=H.call(e,d,v)),(d||0===d)&&(m+=d),v={hash:{},inverse:w.noop,fn:w.program(3,s,r),data:r},(d=l.corner)?d=d.call(e,v):(d=e.corner,d=typeof d===g?d.apply(e):d),l.corner||(d=H.call(e,d,v)),(d||0===d)&&(m+=d),v={hash:{},inverse:w.noop,fn:w.program(5,o,r),data:r},(d=l.shadow)?d=d.call(e,v):(d=e.shadow,d=typeof d===g?d.apply(e):d),l.shadow||(d=H.call(e,d,v)),(d||0===d)&&(m+=d),v={hash:{},inverse:w.noop,fn:w.program(7,p,r),data:r},(d=l.cssClass)?d=d.call(e,v):(d=e.cssClass,d=typeof d===g?d.apply(e):d),l.cssClass||(d=H.call(e,d,v)),(d||0===d)&&(m+=d),m+='"',v={hash:{},inverse:w.noop,fn:w.program(9,i,r),data:r},(d=l.style)?d=d.call(e,v):(d=e.style,d=typeof d===g?d.apply(e):d),l.style||(d=H.call(e,d,v)),(d||0===d)&&(m+=d),v={hash:{},inverse:w.noop,fn:w.program(11,c,r),data:r},(d=l.id)?d=d.call(e,v):(d=e.id,d=typeof d===g?d.apply(e):d),l.id||(d=H.call(e,d,v)),(d||0===d)&&(m+=d),m+=">",d=l.each.call(e,e.items,{hash:{},inverse:w.noop,fn:w.programWithDepth(13,f,r,e),data:r}),(d||0===d)&&(m+=d),m+='</ul><div class="list-loader',v={hash:{},inverse:w.noop,fn:w.program(18,y,r),data:r},(d=l.ui)?d=d.call(e,v):(d=e.ui,d=typeof d===g?d.apply(e):d),l.ui||(d=H.call(e,d,v)),(d||0===d)&&(m+=d),m+='"><a class="list-loader-text">Loading...</a></div>'})}();!function(){var e=Handlebars.template,a=Handlebars.templates=Handlebars.templates||{};a.listbuffereditem=e(function(e,a,n,t,r){function i(e,a,t){var r,i,p="";return p+='<li class="ui-btn ui-li',r=t.ui,r=typeof r===u?r.apply(e):r,i=h.call(e,r,{hash:{},inverse:c.noop,fn:c.program(2,s,a),data:a}),(i||0===i)&&(p+=i),p+='">',i=n.safe.call(e,e,{hash:{},inverse:c.noop,fn:c.program(4,l,a),data:a}),(i||0===i)&&(p+=i),p+="</li>"}function s(e){var a="";return a+=" ui-btn-up-"+f(typeof e===u?e.apply(e):e)}function l(){var e="";return e}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,e.helpers),r=r||{};var p,o="",u="function",f=this.escapeExpression,c=this,h=n.blockHelperMissing;return p=n.each.call(a,a.items,{hash:{},inverse:c.noop,fn:c.programWithDepth(1,i,r,a),data:r}),(p||0===p)&&(o+=p),o})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.listitembasic=a(function(a,e,t,i,n){function r(a,e,i){var n,r,c,f="";return f+='<li class="list-item ui-li ui-li-static',n=t.if_eq.call(a,a.$first,{hash:{compare:!0},inverse:m.noop,fn:m.program(2,l,e),data:e}),(n||0===n)&&(f+=n),n=t.if_eq.call(a,a.$last,{hash:{compare:!0},inverse:m.noop,fn:m.program(4,s,e),data:e}),(n||0===n)&&(f+=n),n=i.ui,n=typeof n===u?n.apply(a):n,r=d.call(a,n,{hash:{},inverse:m.noop,fn:m.program(6,o,e),data:e}),(r||0===r)&&(f+=r),f+='">',c={hash:{},inverse:m.noop,fn:m.program(8,p,e),data:e},(r=t.label)?r=r.call(a,c):(r=a.label,r=typeof r===u?r.apply(a):r),t.label||(r=d.call(a,r,c)),(r||0===r)&&(f+=r),f+="</li>"}function l(){return" ui-first-child"}function s(){return" ui-last-child"}function o(a){var e="";return e+=" ui-btn-up-"+h(typeof a===u?a.apply(a):a)}function p(a){return h(typeof a===u?a.apply(a):a)}this.compilerInfo=[4,">= 1.0.0"],t=this.merge(t,a.helpers),n=n||{};var c,f="",u="function",h=this.escapeExpression,m=this,d=t.blockHelperMissing;return c=t.foreach.call(e,e.items,{hash:{},inverse:m.noop,fn:m.programWithDepth(1,r,n,e),data:n}),(c||0===c)&&(f+=c),f})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.listitembutton=a(function(a,e,l,i,n){function t(a,e,i){var n,t="";return n=l.if_eq.call(a,a.type,{hash:{compare:"divider"},inverse:D.programWithDepth(16,b,e,i),fn:D.programWithDepth(2,r,e,i),data:e}),(n||0===n)&&(t+=n),t}function r(a,e,i){var n,t,r,f="";return f+='<li class="ui-li ui-li-divider',n=i.ui,n=typeof n===y?n.apply(a):n,t=W.call(a,n,{hash:{},inverse:D.noop,fn:D.programWithDepth(3,o,e,a),data:e}),(t||0===t)&&(f+=t),r={hash:{},inverse:D.noop,fn:D.program(5,p,e),data:e},(t=l.bubble)?t=t.call(a,r):(t=a.bubble,t=typeof t===y?t.apply(a):t),l.bubble||(t=W.call(a,t,r)),(t||0===t)&&(f+=t),t=l.if_eq.call(a,a.$first,{hash:{compare:!0},inverse:D.noop,fn:D.program(7,s,e),data:e}),(t||0===t)&&(f+=t),t=l.if_eq.call(a,a.$last,{hash:{compare:!0},inverse:D.noop,fn:D.program(9,h,e),data:e}),(t||0===t)&&(f+=t),f+='">',r={hash:{},inverse:D.noop,fn:D.program(11,u,e),data:e},(t=l.label)?t=t.call(a,r):(t=a.label,t=typeof t===y?t.apply(a):t),l.label||(t=W.call(a,t,r)),(t||0===t)&&(f+=t),t=l["if"].call(a,a.bubble,{hash:{},inverse:D.noop,fn:D.programWithDepth(13,c,e,i),data:e}),(t||0===t)&&(f+=t),f+="</li>"}function o(a,e,i){var n="";return n+=" ui-bar-"+g(l.get.call(a,i.ui,{hash:{"default":a},data:e}))}function p(){return" ui-li-has-count"}function s(){return" ui-first-child"}function h(){return" ui-last-child"}function u(a){return g(typeof a===y?a.apply(a):a)}function c(a,e,i){var n,t,r="";return r+='<span class="ui-li-count',n=i.ui,n=typeof n===y?n.apply(a):n,t=W.call(a,n,{hash:{},inverse:D.noop,fn:D.programWithDepth(14,f,e,a),data:e}),(t||0===t)&&(r+=t),r+=' ui-btn-corner-all">',(t=l.bubble)?t=t.call(a,{hash:{},data:e}):(t=a.bubble,t=typeof t===y?t.apply(a):t),r+=g(t)+"</span>"}function f(a,e,i){var n="";return n+=" ui-btn-up-"+g(l.get.call(a,i.ui,{hash:{"default":a},data:e}))}function b(a,e,i){var n,t,r,o="";return o+='<li class="list-item ui-btn',n=i.ui,n=typeof n===y?n.apply(a):n,t=W.call(a,n,{hash:{},inverse:D.noop,fn:D.programWithDepth(14,f,e,a),data:e}),(t||0===t)&&(o+=t),o+=" ui-btn-icon-right ui-li-has-arrow ui-li",r={hash:{},inverse:D.noop,fn:D.program(5,p,e),data:e},(t=l.bubble)?t=t.call(a,r):(t=a.bubble,t=typeof t===y?t.apply(a):t),l.bubble||(t=W.call(a,t,r)),(t||0===t)&&(o+=t),t=l.if_eq.call(a,a.$first,{hash:{compare:!0},inverse:D.noop,fn:D.program(7,s,e),data:e}),(t||0===t)&&(o+=t),t=l.if_eq.call(a,a.$last,{hash:{compare:!0},inverse:D.noop,fn:D.program(9,h,e),data:e}),(t||0===t)&&(o+=t),o+='"><div class="ui-btn-inner ui-li"><div class="ui-btn-text"><a href="javascript:;" class="ui-link-inherit">',r={hash:{},inverse:D.noop,fn:D.program(11,u,e),data:e},(t=l.label)?t=t.call(a,r):(t=a.label,t=typeof t===y?t.apply(a):t),l.label||(t=W.call(a,t,r)),(t||0===t)&&(o+=t),t=l["if"].call(a,a.bubble,{hash:{},inverse:D.noop,fn:D.programWithDepth(13,c,e,i),data:e}),(t||0===t)&&(o+=t),o+='</a></div><span class="ui-icon',n=i.icon,n=typeof n===y?n.apply(a):n,t=W.call(a,n,{hash:{},inverse:D.noop,fn:D.programWithDepth(17,d,e,a),data:e}),(t||0===t)&&(o+=t),o+=' ui-icon-shadow">&nbsp;</span></div></li>'}function d(a,e,i){var n="";return n+=" ui-icon-"+g(l.get.call(a,i.icon,{hash:{"default":a},data:e}))}this.compilerInfo=[4,">= 1.0.0"],l=this.merge(l,a.helpers),n=n||{};var v,m="",g=this.escapeExpression,y="function",D=this,W=l.blockHelperMissing;return v=l.foreach.call(e,e.items,{hash:{},inverse:D.noop,fn:D.programWithDepth(1,t,n,e),data:n}),(v||0===v)&&(m+=v),m})}();!function(){var a=Handlebars.template,l=Handlebars.templates=Handlebars.templates||{};l.loadmask=a(function(a,l,e,n,r){function s(a){var l="";return l+=" loadmask-"+y(typeof a===m?a.apply(a):a)}function o(){var a="";return a+=" loadmask-fullscreen"}function t(){var a="";return a+=" loadmask-no-fullscreen"}function p(a){var l="";return l+=' style="'+y(typeof a===m?a.apply(a):a)+'"'}function i(a){var l="";return l+=' id="'+y(typeof a===m?a.apply(a):a)+'"'}function f(a){var l="";return l+=" loadmask-msg-"+y(typeof a===m?a.apply(a):a)}this.compilerInfo=[4,">= 1.0.0"],e=this.merge(e,a.helpers),r=r||{};var c,u,d="",m="function",y=this.escapeExpression,v=this,h=e.blockHelperMissing;return d+='<div class="loadmask',u={hash:{},inverse:v.noop,fn:v.program(1,s,r),data:r},(c=e.ui)?c=c.call(l,u):(c=l.ui,c=typeof c===m?c.apply(l):c),e.ui||(c=h.call(l,c,u)),(c||0===c)&&(d+=c),c=e["if"].call(l,l.fullscreen,{hash:{},inverse:v.program(5,t,r),fn:v.program(3,o,r),data:r}),(c||0===c)&&(d+=c),d+='"',u={hash:{},inverse:v.noop,fn:v.program(7,p,r),data:r},(c=e.style)?c=c.call(l,u):(c=l.style,c=typeof c===m?c.apply(l):c),e.style||(c=h.call(l,c,u)),(c||0===c)&&(d+=c),u={hash:{},inverse:v.noop,fn:v.program(9,i,r),data:r},(c=e.id)?c=c.call(l,u):(c=l.id,c=typeof c===m?c.apply(l):c),e.id||(c=h.call(l,c,u)),(c||0===c)&&(d+=c),d+='><div class="loadmask-msg',u={hash:{},inverse:v.noop,fn:v.program(11,f,r),data:r},(c=e.ui)?c=c.call(l,u):(c=l.ui,c=typeof c===m?c.apply(l):c),e.ui||(c=h.call(l,c,u)),(c||0===c)&&(d+=c),d+='">Loading...</div></div>'})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.page=a(function(a,e,s,l,p){function t(a){var e="";return e+=" ui-body-"+u(typeof a===f?a.apply(a):a)}function r(a){var e="";return e+=" "+u(typeof a===f?a.apply(a):a)}function i(a){var e="";return e+=' style="'+u(typeof a===f?a.apply(a):a)+'"'}function n(a){var e="";return e+=' id="'+u(typeof a===f?a.apply(a):a)+'"'}this.compilerInfo=[4,">= 1.0.0"],s=this.merge(s,a.helpers),l=this.merge(l,a.partials),p=p||{};var o,c,y="",f="function",u=this.escapeExpression,d=this,h=s.blockHelperMissing;return y+='<div class="ui-page ui-page-active',c={hash:{},inverse:d.noop,fn:d.program(1,t,p),data:p},(o=s.ui)?o=o.call(e,c):(o=e.ui,o=typeof o===f?o.apply(e):o),s.ui||(o=h.call(e,o,c)),(o||0===o)&&(y+=o),c={hash:{},inverse:d.noop,fn:d.program(3,r,p),data:p},(o=s.cssClass)?o=o.call(e,c):(o=e.cssClass,o=typeof o===f?o.apply(e):o),s.cssClass||(o=h.call(e,o,c)),(o||0===o)&&(y+=o),y+='"',c={hash:{},inverse:d.noop,fn:d.program(5,i,p),data:p},(o=s.style)?o=o.call(e,c):(o=e.style,o=typeof o===f?o.apply(e):o),s.style||(o=h.call(e,o,c)),(o||0===o)&&(y+=o),c={hash:{},inverse:d.noop,fn:d.program(7,n,p),data:p},(o=s.id)?o=o.call(e,c):(o=e.id,o=typeof o===f?o.apply(e):o),s.id||(o=h.call(e,o,c)),(o||0===o)&&(y+=o),y+=">",o=d.invokePartial(l.items,"items",e,s,l,p),(o||0===o)&&(y+=o),y+="</div>"})}();!function(){var e=Handlebars.template,a=Handlebars.templates=Handlebars.templates||{};a.panel=e(function(e,a,i,n,l){function r(e){var a="";return a+=" ui-body-"+u(typeof e===d?e.apply(e):e)}function p(e){var a="";return a+=' id="'+u(typeof e===d?e.apply(e):e)+'-overlay"'}this.compilerInfo=[4,">= 1.0.0"],i=this.merge(i,e.helpers),n=this.merge(n,e.partials),l=l||{};var t,s,o="",d="function",u=this.escapeExpression,v=this,c=i.blockHelperMissing;return o+='<div class="ui-panel',s={hash:{},inverse:v.noop,fn:v.program(1,r,l),data:l},(t=i.ui)?t=t.call(a,s):(t=a.ui,t=typeof t===d?t.apply(a):t),i.ui||(t=c.call(a,t,s)),(t||0===t)&&(o+=t),o+=' panel-menu overthrow"',t=v.invokePartial(n.id,"id",a,i,n,l),(t||0===t)&&(o+=t),o+='><div class="ui-panel-inner panel-menu-inner">',t=v.invokePartial(n.items,"items",a,i,n,l),(t||0===t)&&(o+=t),o+='</div></div><div class="panel-menu-overlay"',s={hash:{},inverse:v.noop,fn:v.program(3,p,l),data:l},(t=i.id)?t=t.call(a,s):(t=a.id,t=typeof t===d?t.apply(a):t),i.id||(t=c.call(a,t,s)),(t||0===t)&&(o+=t),o+=">&nbsp;</div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.popup=a(function(a,e,n,r,o){function t(){return" hidden"}function p(a,e){var r,o="";return o+=" floatingpanel-overlay-",(r=n.overlay)?r=r.call(a,{hash:{},data:e}):(r=a.overlay,r=typeof r===q?r.apply(a):r),o+=D(r)}function l(){return" floatingpanel-overlay-default"}function i(a){var e="";return e+=' id="'+D(typeof a===q?a.apply(a):a)+'-overlay"'}function s(){return" ui-overlay-shadow"}function c(a){var e="";return e+=" ui-corner-"+D(typeof a===q?a.apply(a):a)}function f(a){var e="";return e+="margin-"+D(typeof a===q?a.apply(a):a)+": 0px;"}function u(a){return D(typeof a===q?a.apply(a):a)}function h(a,e,r){var o,t,p="";return p+='<div class="ui-header',o=r.ui,o=typeof o===q?o.apply(a):o,t=W.call(a,o,{hash:{},inverse:P.noop,fn:P.program(18,d,e),data:e}),(t||0===t)&&(p+=t),p+='">',o=a.header,o=null==o||o===!1?o:o.title,o=typeof o===q?o.apply(a):o,t=W.call(a,o,{hash:{},inverse:P.noop,fn:P.program(20,y,e),data:e}),(t||0===t)&&(p+=t),t=n["if"].call(a,(o=a.header,null==o||o===!1?o:o.close),{hash:{},inverse:P.noop,fn:P.programWithDepth(22,v,e,r),data:e}),(t||0===t)&&(p+=t),p+="</div>"}function d(a){var e="";return e+=" ui-bar-"+D(typeof a===q?a.apply(a):a)}function y(a){var e="";return e+='<h1 class="ui-title">'+D(typeof a===q?a.apply(a):a)+"</h1>"}function v(a,e,n){var r,o,t="";return t+='<a href="javascript:;" class="ui-btn-'+D((r=a.header,r=null==r||r===!1?r:r.close,typeof r===q?r.apply(a):r))+" ui-btn",r=n.shadow,r=typeof r===q?r.apply(a):r,o=W.call(a,r,{hash:{},inverse:P.noop,fn:P.program(23,m,e),data:e}),(o||0===o)&&(t+=o),r=n.corner,r=typeof r===q?r.apply(a):r,o=W.call(a,r,{hash:{},inverse:P.noop,fn:P.program(25,g,e),data:e}),(o||0===o)&&(t+=o),t+=" ui-btn-icon-notext",r=n.ui,r=typeof r===q?r.apply(a):r,o=W.call(a,r,{hash:{},inverse:P.noop,fn:P.program(27,b,e),data:e}),(o||0===o)&&(t+=o),t+='"',r=n.id,r=typeof r===q?r.apply(a):r,o=W.call(a,r,{hash:{},inverse:P.noop,fn:P.program(29,w,e),data:e}),(o||0===o)&&(t+=o),t+='><span class="ui-btn-inner"><span class="ui-btn-text">Close</span><span class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span></span></a>'}function m(){return" ui-shadow"}function g(a){var e="";return e+=" ui-btn-corner-"+D(typeof a===q?a.apply(a):a)}function b(a){var e="";return e+=" ui-btn-up-"+D(typeof a===q?a.apply(a):a)}function w(a){var e="";return e+=' id="'+D(typeof a===q?a.apply(a):a)+'-close"'}function x(a){var e="";return e+=" ui-body-"+D(typeof a===q?a.apply(a):a)}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),r=this.merge(r,a.partials),o=o||{};var H,k,S="",q="function",D=this.escapeExpression,P=this,W=n.blockHelperMissing;return S+='<div class="floatingpanel-overlay',H=n.if_eq.call(e,e.hidden,{hash:{compare:!0,"default":!0},inverse:P.noop,fn:P.program(1,t,o),data:o}),(H||0===H)&&(S+=H),H=n["if"].call(e,e.overlay,{hash:{},inverse:P.program(5,l,o),fn:P.program(3,p,o),data:o}),(H||0===H)&&(S+=H),S+='"',k={hash:{},inverse:P.noop,fn:P.program(7,i,o),data:o},(H=n.id)?H=H.call(e,k):(H=e.id,H=typeof H===q?H.apply(e):H),n.id||(H=W.call(e,H,k)),(H||0===H)&&(S+=H),S+='></div><div class="ui-dialog-contain',k={hash:{},inverse:P.noop,fn:P.program(9,s,o),data:o},(H=n.shadow)?H=H.call(e,k):(H=e.shadow,H=typeof H===q?H.apply(e):H),n.shadow||(H=W.call(e,H,k)),(H||0===H)&&(S+=H),k={hash:{},inverse:P.noop,fn:P.program(11,c,o),data:o},(H=n.corner)?H=H.call(e,k):(H=e.corner,H=typeof H===q?H.apply(e):H),n.corner||(H=W.call(e,H,k)),(H||0===H)&&(S+=H),H=n.if_eq.call(e,e.hidden,{hash:{compare:!0,"default":!0},inverse:P.noop,fn:P.program(1,t,o),data:o}),(H||0===H)&&(S+=H),S+='" style="z-index: 10000;',k={hash:{},inverse:P.noop,fn:P.program(13,f,o),data:o},(H=n.actionSheet)?H=H.call(e,k):(H=e.actionSheet,H=typeof H===q?H.apply(e):H),n.actionSheet||(H=W.call(e,H,k)),(H||0===H)&&(S+=H),k={hash:{},inverse:P.noop,fn:P.program(15,u,o),data:o},(H=n.style)?H=H.call(e,k):(H=e.style,H=typeof H===q?H.apply(e):H),n.style||(H=W.call(e,H,k)),(H||0===H)&&(S+=H),S+='"',H=P.invokePartial(r.id,"id",e,n,r,o),(H||0===H)&&(S+=H),S+=">",H=n["if"].call(e,e.header,{hash:{},inverse:P.noop,fn:P.programWithDepth(17,h,o,e),data:o}),(H||0===H)&&(S+=H),S+='<div class="ui-content',k={hash:{},inverse:P.noop,fn:P.program(31,x,o),data:o},(H=n.ui)?H=H.call(e,k):(H=e.ui,H=typeof H===q?H.apply(e):H),n.ui||(H=W.call(e,H,k)),(H||0===H)&&(S+=H),S+='">',H=P.invokePartial(r.items,"items",e,n,r,o),(H||0===H)&&(S+=H),S+="</div></div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.radiobutton=a(function(a,e,n,r,o){function l(){return"on"}function i(){return"off"}function p(a){var e="";return e+=" ui-btn-corner-"+w(typeof a===b?a.apply(a):a)}function t(){return" ui-shadow"}function s(){return" ui-mini"}function c(a){var e="";return e+=" ui-btn-up-"+w(typeof a===b?a.apply(a):a)}function u(a){var e="";return e+=' id="'+w(typeof a===b?a.apply(a):a)+'-ui"'}function f(a){return w(typeof a===b?a.apply(a):a)}function d(){return" ui-icon-shadow"}function h(a){var e="";return e+=' name="'+w(typeof a===b?a.apply(a):a)+'"'}function m(a){var e="";return e+=' value="'+w(typeof a===b?a.apply(a):a)+'"'}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),r=this.merge(r,a.partials),o=o||{};var y,v,g="",b="function",w=this.escapeExpression,k=this,_=n.blockHelperMissing;return g+='<div class="ui-radio"><label class="ui-radio-',y=n.if_eq.call(e,e.checked,{hash:{compare:!0,"default":!1},inverse:k.program(3,i,o),fn:k.program(1,l,o),data:o}),(y||0===y)&&(g+=y),g+=" ui-btn",v={hash:{},inverse:k.noop,fn:k.program(5,p,o),data:o},(y=n.corner)?y=y.call(e,v):(y=e.corner,y=typeof y===b?y.apply(e):y),n.corner||(y=_.call(e,y,v)),(y||0===y)&&(g+=y),v={hash:{},inverse:k.noop,fn:k.program(7,t,o),data:o},(y=n.shadow)?y=y.call(e,v):(y=e.shadow,y=typeof y===b?y.apply(e):y),n.shadow||(y=_.call(e,y,v)),(y||0===y)&&(g+=y),y=n.if_eq.call(e,e.size,{hash:{compare:"small","default":"medium"},inverse:k.noop,fn:k.program(9,s,o),data:o}),(y||0===y)&&(g+=y),g+=" ui-fullsize ui-btn-icon-"+w(n.get.call(e,e.iconPos,{hash:{"default":"left",choices:"left|right"},data:o})),v={hash:{},inverse:k.noop,fn:k.program(11,c,o),data:o},(y=n.ui)?y=y.call(e,v):(y=e.ui,y=typeof y===b?y.apply(e):y),n.ui||(y=_.call(e,y,v)),(y||0===y)&&(g+=y),g+='"',y=k.invokePartial(r.for_id,"for_id",e,n,r,o),(y||0===y)&&(g+=y),v={hash:{},inverse:k.noop,fn:k.program(13,u,o),data:o},(y=n.id)?y=y.call(e,v):(y=e.id,y=typeof y===b?y.apply(e):y),n.id||(y=_.call(e,y,v)),(y||0===y)&&(g+=y),g+='><span class="ui-btn-inner"><span class="ui-btn-text">',v={hash:{},inverse:k.noop,fn:k.program(15,f,o),data:o},(y=n.label)?y=y.call(e,v):(y=e.label,y=typeof y===b?y.apply(e):y),n.label||(y=_.call(e,y,v)),(y||0===y)&&(g+=y),g+='</span><span class="ui-icon ui-icon-radio-',y=n.if_eq.call(e,e.checked,{hash:{compare:!0,"default":!1},inverse:k.program(3,i,o),fn:k.program(1,l,o),data:o}),(y||0===y)&&(g+=y),v={hash:{},inverse:k.noop,fn:k.program(17,d,o),data:o},(y=n.shadow)?y=y.call(e,v):(y=e.shadow,y=typeof y===b?y.apply(e):y),n.shadow||(y=_.call(e,y,v)),(y||0===y)&&(g+=y),g+='">&nbsp;</span></span></label><input type="radio"',v={hash:{},inverse:k.noop,fn:k.program(19,h,o),data:o},(y=n.name)?y=y.call(e,v):(y=e.name,y=typeof y===b?y.apply(e):y),n.name||(y=_.call(e,y,v)),(y||0===y)&&(g+=y),v={hash:{},inverse:k.noop,fn:k.program(21,m,o),data:o},(y=n.value)?y=y.call(e,v):(y=e.value,y=typeof y===b?y.apply(e):y),n.value||(y=_.call(e,y,v)),(y||0===y)&&(g+=y),y=k.invokePartial(r.id,"id",e,n,r,o),(y||0===y)&&(g+=y),g+="></div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.select=a(function(a,e,n,l,r){function t(){return'<div class="ui-field-contain ui-body ui-br">'}function o(a,e){var r,t="";return t+='<label class="ui-select"',r=H.invokePartial(l.for_id,"for_id",a,n,l,e),(r||0===r)&&(t+=r),t+=">"+P(typeof a===k?a.apply(a):a)+"</label>"}function i(a){var e="";return e+=' id="'+P(typeof a===k?a.apply(a):a)+'-ui"'}function p(){return" ui-shadow"}function s(a){var e="";return e+=" ui-btn-corner-"+P(typeof a===k?a.apply(a):a)}function c(){return" ui-disabled"}function u(){return" ui-mini"}function f(a){var e="";return e+=" ui-btn-up-"+P(typeof a===k?a.apply(a):a)}function d(a,e,l){var r="";return r+=P(n.getLabelFromValue.call(a,a,l.options,{hash:{},data:e}))}function h(a,e){var l,r,t="";return r={hash:{},inverse:H.noop,fn:H.program(20,v,e),data:e},(l=n.emptyText)?l=l.call(a,r):(l=a.emptyText,l=typeof l===k?l.apply(a):l),n.emptyText||(l=T.call(a,l,r)),(l||0===l)&&(t+=l),t}function v(a){return P(typeof a===k?a.apply(a):a)}function y(){return" ui-icon-shadow"}function m(a){var e="";return e+=' name="'+P(typeof a===k?a.apply(a):a)+'"'}function b(a,e,l){var r,t="";return t+='<option value="',(r=n.value)?r=r.call(a,{hash:{},data:e}):(r=a.value,r=typeof r===k?r.apply(a):r),t+=P(r)+'"',r=n.if_eq.call(a,a.value,{hash:{compare:l.value},inverse:H.noop,fn:H.program(27,g,e),data:e}),(r||0===r)&&(t+=r),r=n.if_eq.call(a,a.disabled,{hash:{compare:!0,"default":!1},inverse:H.noop,fn:H.program(29,w,e),data:e}),(r||0===r)&&(t+=r),t+=">",(r=n.label)?r=r.call(a,{hash:{},data:e}):(r=a.label,r=typeof r===k?r.apply(a):r),t+=P(r)+"</option>"}function g(){return' selected="selected"'}function w(){return' disabled="disabled"'}function I(){return"</div>"}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),l=this.merge(l,a.partials),r=r||{};var _,x,q="",H=this,k="function",P=this.escapeExpression,T=n.blockHelperMissing;return x={hash:{},inverse:H.noop,fn:H.program(1,t,r),data:r},(_=n.labelInline)?_=_.call(e,x):(_=e.labelInline,_=typeof _===k?_.apply(e):_),n.labelInline||(_=T.call(e,_,x)),(_||0===_)&&(q+=_),x={hash:{},inverse:H.noop,fn:H.program(3,o,r),data:r},(_=n.label)?_=_.call(e,x):(_=e.label,_=typeof _===k?_.apply(e):_),n.label||(_=T.call(e,_,x)),(_||0===_)&&(q+=_),q+="<div",x={hash:{},inverse:H.noop,fn:H.program(5,i,r),data:r},(_=n.id)?_=_.call(e,x):(_=e.id,_=typeof _===k?_.apply(e):_),n.id||(_=T.call(e,_,x)),(_||0===_)&&(q+=_),q+=' class="ui-select"><div class="ui-btn',x={hash:{},inverse:H.noop,fn:H.program(7,p,r),data:r},(_=n.shadow)?_=_.call(e,x):(_=e.shadow,_=typeof _===k?_.apply(e):_),n.shadow||(_=T.call(e,_,x)),(_||0===_)&&(q+=_),x={hash:{},inverse:H.noop,fn:H.program(9,s,r),data:r},(_=n.corner)?_=_.call(e,x):(_=e.corner,_=typeof _===k?_.apply(e):_),n.corner||(_=T.call(e,_,x)),(_||0===_)&&(q+=_),_=n.if_eq.call(e,e.disabled,{hash:{compare:!0,"default":!1},inverse:H.noop,fn:H.program(11,c,r),data:r}),(_||0===_)&&(q+=_),_=n.if_eq.call(e,e.size,{hash:{compare:"small","default":"medium"},inverse:H.noop,fn:H.program(13,u,r),data:r}),(_||0===_)&&(q+=_),q+=" ui-btn-icon-"+P(n.get.call(e,e.iconPos,{hash:{"default":"right",choices:"left|right"},data:r})),x={hash:{},inverse:H.noop,fn:H.program(15,f,r),data:r},(_=n.ui)?_=_.call(e,x):(_=e.ui,_=typeof _===k?_.apply(e):_),n.ui||(_=T.call(e,_,x)),(_||0===_)&&(q+=_),q+='"><span class="ui-btn-inner"><span class="ui-btn-text">',_=n["if"].call(e,e.value,{hash:{},inverse:H.program(19,h,r),fn:H.programWithDepth(17,d,r,e),data:r}),(_||0===_)&&(q+=_),q+='</span><span class="ui-icon ui-icon-arrow-d',x={hash:{},inverse:H.noop,fn:H.program(22,y,r),data:r},(_=n.shadow)?_=_.call(e,x):(_=e.shadow,_=typeof _===k?_.apply(e):_),n.shadow||(_=T.call(e,_,x)),(_||0===_)&&(q+=_),q+='">&nbsp;</span><select',x={hash:{},inverse:H.noop,fn:H.program(24,m,r),data:r},(_=n.name)?_=_.call(e,x):(_=e.name,_=typeof _===k?_.apply(e):_),n.name||(_=T.call(e,_,x)),(_||0===_)&&(q+=_),_=H.invokePartial(l.id,"id",e,n,l,r),(_||0===_)&&(q+=_),q+=">",_=n.each.call(e,e.options,{hash:{},inverse:H.noop,fn:H.programWithDepth(26,b,r,e),data:r}),(_||0===_)&&(q+=_),q+="</select></span></div></div>",x={hash:{},inverse:H.noop,fn:H.program(31,I,r),data:r},(_=n.labelInline)?_=_.call(e,x):(_=e.labelInline,_=typeof _===k?_.apply(e):_),n.labelInline||(_=T.call(e,_,x)),(_||0===_)&&(q+=_),q})}();!function(){var e=Handlebars.template,a=Handlebars.templates=Handlebars.templates||{};a.selectitembasic=e(function(e,a,t,l,n){function r(e,a,l){var n,r="";return r+='<option value="',(n=t.value)?n=n.call(e,{hash:{},data:a}):(n=e.value,n=typeof n===c?n.apply(e):n),r+=h(n)+'"',n=t.if_eq.call(e,e.value,{hash:{compare:l.value},inverse:d.noop,fn:d.program(2,s,a),data:a}),(n||0===n)&&(r+=n),n=t.if_eq.call(e,e.disabled,{hash:{compare:!0,"default":!1},inverse:d.noop,fn:d.program(4,o,a),data:a}),(n||0===n)&&(r+=n),r+=">",(n=t.label)?n=n.call(e,{hash:{},data:a}):(n=e.label,n=typeof n===c?n.apply(e):n),r+=h(n)+"</option>"}function s(){return' selected="selected"'}function o(){return' disabled="disabled"'}this.compilerInfo=[4,">= 1.0.0"],t=this.merge(t,e.helpers),n=n||{};var i,p="",c="function",h=this.escapeExpression,d=this;return i=t.each.call(a,a.options,{hash:{},inverse:d.noop,fn:d.programWithDepth(1,r,n,a),data:n}),(i||0===i)&&(p+=i),p})}();!function(){var a=Handlebars.template,n=Handlebars.templates=Handlebars.templates||{};n.tabbutton=a(function(a,n,t,e,o){function i(a,n,e){var o="";return o+=" ui-btn-icon-"+y(t.get.call(a,e.iconPos,{hash:{"default":"top",choices:"top|bottom|left|right"},data:n}))}function l(a){var n="";return n+=" ui-btn-up-"+y(typeof a===v?a.apply(a):a)}function p(a){var n="";return n+=' style="'+y(typeof a===v?a.apply(a):a)+'"'}function r(a){var n="";return n+='<span class="ui-btn-text">'+y(typeof a===v?a.apply(a):a)+"</span>"}function s(a,n){var e,o="";return o+='<span class="icon override-tab-icon">',e=t.geticon.call(a,a,{hash:{},inverse:b.noop,fn:b.program(10,c,n),data:n}),(e||0===e)&&(o+=e),o+="</span>"}function c(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],t=this.merge(t,a.helpers),e=this.merge(e,a.partials),o=o||{};var u,f,h="",y=this.escapeExpression,v="function",b=this,d=t.blockHelperMissing;return h+='<a class="ui-btn ui-btn-inline',f={hash:{},inverse:b.noop,fn:b.programWithDepth(1,i,o,n),data:o},(u=t.icon)?u=u.call(n,f):(u=n.icon,u=typeof u===v?u.apply(n):u),t.icon||(u=d.call(n,u,f)),(u||0===u)&&(h+=u),f={hash:{},inverse:b.noop,fn:b.program(3,l,o),data:o},(u=t.ui)?u=u.call(n,f):(u=n.ui,u=typeof u===v?u.apply(n):u),t.ui||(u=d.call(n,u,f)),(u||0===u)&&(h+=u),h+='"',f={hash:{},inverse:b.noop,fn:b.program(5,p,o),data:o},(u=t.style)?u=u.call(n,f):(u=n.style,u=typeof u===v?u.apply(n):u),t.style||(u=d.call(n,u,f)),(u||0===u)&&(h+=u),u=b.invokePartial(e.id,"id",n,t,e,o),(u||0===u)&&(h+=u),h+='><span class="ui-btn-inner">',f={hash:{},inverse:b.noop,fn:b.program(7,r,o),data:o},(u=t.text)?u=u.call(n,f):(u=n.text,u=typeof u===v?u.apply(n):u),t.text||(u=d.call(n,u,f)),(u||0===u)&&(h+=u),f={hash:{},inverse:b.noop,fn:b.program(9,s,o),data:o},(u=t.icon)?u=u.call(n,f):(u=n.icon,u=typeof u===v?u.apply(n):u),t.icon||(u=d.call(n,u,f)),(u||0===u)&&(h+=u),h+="</span></a>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.tabs=a(function(a,e,s,t,i){function n(){return" ui-mini"}function l(a){var e="";return e+=' id="'+d(typeof a===u?a.apply(a):a)+'"'}function r(a,e){var t,i,n="";return n+='<li class="ui-block-'+d(s.getIndexLetter.call(a,(t=e,null==t||t===!1?t:t.index),{hash:{},data:e}))+'">',i=s.safe.call(a,a,{hash:{},inverse:f.noop,fn:f.program(6,o,e),data:e}),(i||0===i)&&(n+=i),n+="</li>"}function o(){var a="";return a}this.compilerInfo=[4,">= 1.0.0"],s=this.merge(s,a.helpers),t=this.merge(t,a.partials),i=i||{};var c,p,h="",u="function",d=this.escapeExpression,f=this,m=s.blockHelperMissing;return h+='<div class="ui-navbar',c=s.if_eq.call(e,e.mini,{hash:{compare:!0,"default":!0},inverse:f.noop,fn:f.program(1,n,i),data:i}),(c||0===c)&&(h+=c),c=f.invokePartial(t.cssClass,"cssClass",e,s,t,i),(c||0===c)&&(h+=c),h+='"',c=f.invokePartial(t.style,"style",e,s,t,i),(c||0===c)&&(h+=c),p={hash:{},inverse:f.noop,fn:f.program(3,l,i),data:i},(c=s.id)?c=c.call(e,p):(c=e.id,c=typeof c===u?c.apply(e):c),s.id||(c=m.call(e,c,p)),(c||0===c)&&(h+=c),h+='><ul class="ui-grid-'+d(s.getLengthLetter.call(e,e.items,{hash:{},data:i}))+'">',c=s.each.call(e,e.items,{hash:{},inverse:f.noop,fn:f.program(5,r,i),data:i}),(c||0===c)&&(h+=c),h+="</ul></div>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.textarea=a(function(a,e,r,l,n){function t(a,e){var n,t="";return t+='<label class="ui-input-text"',n=H.invokePartial(l.for_id,"for_id",a,r,l,e),(n||0===n)&&(t+=n),t+=">"+q(typeof a===x?a.apply(a):a)+"</label>"}function o(a){var e="";return e+=" ui-body-"+q(typeof a===x?a.apply(a):a)}function p(){return" ui-disabled"}function i(a){var e="";return e+=' name="'+q(typeof a===x?a.apply(a):a)+'"'}function s(a){var e="";return e+=' placeholder="'+q(typeof a===x?a.apply(a):a)+'"'}function c(){return' disabled="disabled"'}function f(){return" required"}function u(a,e){var l;return(l=r.cols)?l=l.call(a,{hash:{},data:e}):(l=a.cols,l=typeof l===x?l.apply(a):l),q(l)}function d(){return"40"}function h(a,e){var l;return(l=r.rows)?l=l.call(a,{hash:{},data:e}):(l=a.rows,l=typeof l===x?l.apply(a):l),q(l)}function m(){return"8"}function y(a){return q(typeof a===x?a.apply(a):a)}this.compilerInfo=[4,">= 1.0.0"],r=this.merge(r,a.helpers),l=this.merge(l,a.partials),n=n||{};var v,b,g="",H=this,x="function",q=this.escapeExpression,w=r.blockHelperMissing;return b={hash:{},inverse:H.noop,fn:H.program(1,t,n),data:n},(v=r.label)?v=v.call(e,b):(v=e.label,v=typeof v===x?v.apply(e):v),r.label||(v=w.call(e,v,b)),(v||0===v)&&(g+=v),g+='<textarea class="ui-input-text ui-shadow-inset ui-corner-all ',b={hash:{},inverse:H.noop,fn:H.program(3,o,n),data:n},(v=r.ui)?v=v.call(e,b):(v=e.ui,v=typeof v===x?v.apply(e):v),r.ui||(v=w.call(e,v,b)),(v||0===v)&&(g+=v),v=r.if_eq.call(e,e.disabled,{hash:{compare:!0,"default":!1},inverse:H.noop,fn:H.program(5,p,n),data:n}),(v||0===v)&&(g+=v),g+='"',b={hash:{},inverse:H.noop,fn:H.program(7,i,n),data:n},(v=r.name)?v=v.call(e,b):(v=e.name,v=typeof v===x?v.apply(e):v),r.name||(v=w.call(e,v,b)),(v||0===v)&&(g+=v),b={hash:{},inverse:H.noop,fn:H.program(9,s,n),data:n},(v=r.placeHolder)?v=v.call(e,b):(v=e.placeHolder,v=typeof v===x?v.apply(e):v),r.placeHolder||(v=w.call(e,v,b)),(v||0===v)&&(g+=v),v=r.if_eq.call(e,e.disabled,{hash:{compare:!0,"default":!1},inverse:H.noop,fn:H.program(11,c,n),data:n}),(v||0===v)&&(g+=v),v=r.if_eq.call(e,e.required,{hash:{compare:!0,"default":!1},inverse:H.noop,fn:H.program(13,f,n),data:n}),(v||0===v)&&(g+=v),g+=' cols="',v=r["if"].call(e,e.cols,{hash:{},inverse:H.program(17,d,n),fn:H.program(15,u,n),data:n}),(v||0===v)&&(g+=v),g+='" rows="',v=r["if"].call(e,e.rows,{hash:{},inverse:H.program(21,m,n),fn:H.program(19,h,n),data:n}),(v||0===v)&&(g+=v),g+='"',v=H.invokePartial(l.id,"id",e,r,l,n),(v||0===v)&&(g+=v),g+=">",b={hash:{},inverse:H.noop,fn:H.program(23,y,n),data:n},(v=r.value)?v=v.call(e,b):(v=e.value,v=typeof v===x?v.apply(e):v),r.value||(v=w.call(e,v,b)),(v||0===v)&&(g+=v),g+="</textarea>"})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.textfield=a(function(a,e,n,l,r){function t(){return'<div class="ui-field-contain ui-body ui-br">'}function o(a,e){var r,t="";return t+='<label class="ui-input-text"',r=F.invokePartial(l.for_id,"for_id",a,n,l,e),(r||0===r)&&(t+=r),t+=">"+J(typeof a===G?a.apply(a):a)+"</label>"}function p(){return"ui-input-search"}function i(){return"ui-input-text"}function s(){return" ui-shadow-inset"}function u(){return"btn-"}function c(a){var e="";return e+=" ui-body-"+J(typeof a===G?a.apply(a):a)}function f(a){var e="";return e+=" "+J(typeof a===G?a.apply(a):a)}function d(a){var e="";return e+=" ui-icon-"+J(typeof a===G?a.apply(a):a)+"field"}function h(){return" ui-mini"}function y(){return" ui-disabled"}function v(a,e){var l;return(l=n.type)?l=l.call(a,{hash:{},data:e}):(l=a.type,l=typeof l===G?l.apply(a):l),J(l)}function m(){return"text"}function b(a){var e="";return e+=' autocomplete="'+J(typeof a===G?a.apply(a):a)+'"'}function g(a){var e="";return e+=' name="'+J(typeof a===G?a.apply(a):a)+'"'}function q(a){var e="";return e+=' pattern="'+J(typeof a===G?a.apply(a):a)+'"'}function x(a){var e="";return e+=' value="'+J(typeof a===G?a.apply(a):a)+'"'}function _(a){var e="";return e+=' placeholder="'+J(typeof a===G?a.apply(a):a)+'"'}function w(){var a="";return a+=" mobile-textinput-disabled ui-state-disabled"}function H(){return' disabled="disabled"'}function I(){return" required"}function k(){return" readonly"}function C(a,e){var l,r,t="";return t+='<a href="javascript:;" class="ui-input-clear ui-btn',r={hash:{},inverse:F.noop,fn:F.program(46,z,e),data:e},(l=n.ui)?l=l.call(a,r):(l=a.ui,l=typeof l===G?l.apply(a):l),n.ui||(l=K.call(a,l,r)),(l||0===l)&&(t+=l),t+=" ui-shadow ui-btn-corner-all ui-fullsize ui-btn-icon-notext",l=n["if"].call(a,a.value,{hash:{},inverse:F.program(49,P,e),fn:F.program(48,B,e),data:e}),(l||0===l)&&(t+=l),t+='"',r={hash:{},inverse:F.noop,fn:F.program(51,j,e),data:e},(l=n.id)?l=l.call(a,r):(l=a.id,l=typeof l===G?l.apply(a):l),n.id||(l=K.call(a,l,r)),(l||0===l)&&(t+=l),t+='><span class="ui-btn-inner"><span class="ui-btn-text">clear text</span><span class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span></span></a>'}function z(a){var e="";return e+=" ui-btn-up-"+J(typeof a===G?a.apply(a):a)}function B(a,e){var l,r,t="";return r=n.if_gteq.call(a,a.clearBtn,{hash:{compare:(l=a.value,null==l||l===!1?l:l.length),"default":1},inverse:F.noop,fn:F.program(49,P,e),data:e}),(r||0===r)&&(t+=r),t}function P(){var a="";return a+=" ui-input-clear-hidden"}function j(a){var e="";return e+=' id="'+J(typeof a===G?a.apply(a):a)+'-clearbtn"'}function E(){return"</div>"}this.compilerInfo=[4,">= 1.0.0"],n=this.merge(n,a.helpers),l=this.merge(l,a.partials),r=r||{};var M,A,D="",F=this,G="function",J=this.escapeExpression,K=n.blockHelperMissing;return A={hash:{},inverse:F.noop,fn:F.program(1,t,r),data:r},(M=n.labelInline)?M=M.call(e,A):(M=e.labelInline,M=typeof M===G?M.apply(e):M),n.labelInline||(M=K.call(e,M,A)),(M||0===M)&&(D+=M),A={hash:{},inverse:F.noop,fn:F.program(3,o,r),data:r},(M=n.label)?M=M.call(e,A):(M=e.label,M=typeof M===G?M.apply(e):M),n.label||(M=K.call(e,M,A)),(M||0===M)&&(D+=M),D+='<div class="',M=n["if"].call(e,e.icon,{hash:{},inverse:F.program(7,i,r),fn:F.program(5,p,r),data:r}),(M||0===M)&&(D+=M),A={hash:{},inverse:F.noop,fn:F.program(9,s,r),data:r},(M=n.shadow)?M=M.call(e,A):(M=e.shadow,M=typeof M===G?M.apply(e):M),n.shadow||(M=K.call(e,M,A)),(M||0===M)&&(D+=M),D+=" ui-",A={hash:{},inverse:F.noop,fn:F.program(11,u,r),data:r},(M=n.icon)?M=M.call(e,A):(M=e.icon,M=typeof M===G?M.apply(e):M),n.icon||(M=K.call(e,M,A)),(M||0===M)&&(D+=M),D+="corner-all ui-btn-shadow",A={hash:{},inverse:F.noop,fn:F.program(13,c,r),data:r},(M=n.ui)?M=M.call(e,A):(M=e.ui,M=typeof M===G?M.apply(e):M),n.ui||(M=K.call(e,M,A)),(M||0===M)&&(D+=M),A={hash:{},inverse:F.noop,fn:F.program(15,f,r),data:r},(M=n.cssClass)?M=M.call(e,A):(M=e.cssClass,M=typeof M===G?M.apply(e):M),n.cssClass||(M=K.call(e,M,A)),(M||0===M)&&(D+=M),A={hash:{},inverse:F.noop,fn:F.program(17,d,r),data:r},(M=n.icon)?M=M.call(e,A):(M=e.icon,M=typeof M===G?M.apply(e):M),n.icon||(M=K.call(e,M,A)),(M||0===M)&&(D+=M),M=n.if_eq.call(e,e.size,{hash:{compare:"small","default":"medium"},inverse:F.noop,fn:F.program(19,h,r),data:r}),(M||0===M)&&(D+=M),M=n.if_eq.call(e,e.disabled,{hash:{compare:!0,"default":!1},inverse:F.noop,fn:F.program(21,y,r),data:r}),(M||0===M)&&(D+=M),D+='"><input type="',M=n["if"].call(e,e.type,{hash:{},inverse:F.program(25,m,r),fn:F.program(23,v,r),data:r}),(M||0===M)&&(D+=M),D+='"',A={hash:{},inverse:F.noop,fn:F.program(27,b,r),data:r},(M=n.autocomplete)?M=M.call(e,A):(M=e.autocomplete,M=typeof M===G?M.apply(e):M),n.autocomplete||(M=K.call(e,M,A)),(M||0===M)&&(D+=M),A={hash:{},inverse:F.noop,fn:F.program(29,g,r),data:r},(M=n.name)?M=M.call(e,A):(M=e.name,M=typeof M===G?M.apply(e):M),n.name||(M=K.call(e,M,A)),(M||0===M)&&(D+=M),A={hash:{},inverse:F.noop,fn:F.program(31,q,r),data:r},(M=n.pattern)?M=M.call(e,A):(M=e.pattern,M=typeof M===G?M.apply(e):M),n.pattern||(M=K.call(e,M,A)),(M||0===M)&&(D+=M),A={hash:{},inverse:F.noop,fn:F.program(33,x,r),data:r},(M=n.value)?M=M.call(e,A):(M=e.value,M=typeof M===G?M.apply(e):M),n.value||(M=K.call(e,M,A)),(M||0===M)&&(D+=M),A={hash:{},inverse:F.noop,fn:F.program(35,_,r),data:r},(M=n.placeHolder)?M=M.call(e,A):(M=e.placeHolder,M=typeof M===G?M.apply(e):M),n.placeHolder||(M=K.call(e,M,A)),(M||0===M)&&(D+=M),D+='class="ui-input-text',M=n.if_eq.call(e,e.disabled,{hash:{compare:!0,"default":!1},inverse:F.noop,fn:F.program(37,w,r),data:r}),(M||0===M)&&(D+=M),A={hash:{},inverse:F.noop,fn:F.program(13,c,r),data:r},(M=n.ui)?M=M.call(e,A):(M=e.ui,M=typeof M===G?M.apply(e):M),n.ui||(M=K.call(e,M,A)),(M||0===M)&&(D+=M),D+='"',M=n.if_eq.call(e,e.disabled,{hash:{compare:!0,"default":!1},inverse:F.noop,fn:F.program(39,H,r),data:r}),(M||0===M)&&(D+=M),M=n.if_eq.call(e,e.required,{hash:{compare:!0,"default":!1},inverse:F.noop,fn:F.program(41,I,r),data:r}),(M||0===M)&&(D+=M),M=n.if_eq.call(e,e.readonly,{hash:{compare:!0,"default":!1},inverse:F.noop,fn:F.program(43,k,r),data:r}),(M||0===M)&&(D+=M),M=F.invokePartial(l.id,"id",e,n,l,r),(M||0===M)&&(D+=M),D+=">",M=n.if_neq.call(e,e.clearBtn,{hash:{compare:!1,"default":!1},inverse:F.noop,fn:F.program(45,C,r),data:r}),(M||0===M)&&(D+=M),D+="</div>",A={hash:{},inverse:F.noop,fn:F.program(53,E,r),data:r},(M=n.labelInline)?M=M.call(e,A):(M=e.labelInline,M=typeof M===G?M.apply(e):M),n.labelInline||(M=K.call(e,M,A)),(M||0===M)&&(D+=M),D})}();!function(){var a=Handlebars.template,e=Handlebars.templates=Handlebars.templates||{};e.toolbar=a(function(a,e,s,i,t){function r(a){var e="";return e+="ui-bar-"+c(typeof a===p?a.apply(a):a)}this.compilerInfo=[4,">= 1.0.0"],s=this.merge(s,a.helpers),i=this.merge(i,a.partials),t=t||{};var l,n,o="",p="function",c=this.escapeExpression,u=this,m=s.blockHelperMissing;return o+='<div class="',n={hash:{},inverse:u.noop,fn:u.program(1,r,t),data:t},(l=s.ui)?l=l.call(e,n):(l=e.ui,l=typeof l===p?l.apply(e):l),s.ui||(l=m.call(e,l,n)),(l||0===l)&&(o+=l),l=u.invokePartial(i.cssClass,"cssClass",e,s,i,t),(l||0===l)&&(o+=l),o+='">',l=u.invokePartial(i.items,"items",e,s,i,t),(l||0===l)&&(o+=l),o+="</div>"})}();


Fs.engines.JqueryMobile = (function() {

    var _handlebars = Handlebars,
        _fs = Fs,
        _views = _fs.views,
        _fsevent = _fs.events;
        _parent = _fs,
        _selector = _fs.Selector,
        _templates = _fs.Templates,
        _tpls = _handlebars.templates,
        _events = {},
        _overrides = {};

    
    var _win = window,
        _body = document.body;

    _win.addEventListener('orientationchange', function() {
        setTimeout(function() {
            _win.scrollTo(_body.scrollLeft, _body.scrollTop);
        }, 0);
    }, false);
    

    
    _handlebars.registerHelper('getIndexLetter', function(index) {
        return String.fromCharCode(97 + (index % 26));
    });

    _handlebars.registerHelper('getLengthLetter', function(array) {
        var length = array.length;

        if (length < 2) {
            return 'a';
        }
        return String.fromCharCode(95 + length);
    });

    
    function _removeUI(pattern, ui, el) {
        if (typeof ui !== 'object') {
            ui = [ui];
        }

        var len = ui.length;

        while (len--) {
            _selector.removeClass(el, pattern.replace('{{ui}}', ui[len]));
        }
    };

    function _addUI(pattern, ui, el) {
        if (typeof ui !== 'object') {
            ui = [ui];
        }

        var len = ui.length;

        while (len--) {
            _selector.addClass(el, pattern.replace('{{ui}}', ui[len]));
        }
    };

    function updateMarginTop() {
        if (this.el) {
            return;
        }
        var el = this.el,
            height = el.offsetHeight / 2;

        el.style.marginTop = '-' + height + 'px';
    };

    _events.alert = {
        show: function(cmp) {
            if (this.el) {
                updateMarginTop.call(this);
            } else {
                setTimeout(updateMarginTop.bind(this), 150);
            }
        }
    };

    _events.collapsible = {
        afterrender: function() {
            this.iconEl = _selector.get('.ui-icon', this.el);
            this.iconCollapsed = 'ui-icon-' + (this.config.iconCollapsed || 'plus');
            this.iconExpanded = 'ui-icon-' + (this.config.iconExpanded || 'minus');
        },
        collapse: function() {
            _selector.addClass(this.headerEl, 'ui-collapsible-heading-collapsed');
            _selector.addClass(this.contentEl, 'ui-collapsible-content-collapsed');
            _selector.addClass(this.el, 'ui-collapsible-collapsed');
            
            _selector.removeClass(this.iconEl, this.iconExpanded);
            _selector.addClass(this.iconEl, this.iconCollapsed);
        },
        expand: function() {
            _selector.removeClass(this.headerEl, 'ui-collapsible-heading-collapsed');
            _selector.removeClass(this.contentEl, 'ui-collapsible-content-collapsed');
            _selector.removeClass(this.el, 'ui-collapsible-collapsed');
            
            _selector.removeClass(this.iconEl, this.iconCollapsed);
            _selector.addClass(this.iconEl, this.iconExpanded);
        }
    };

    _handlebars.registerHelper('getLabelFromValue', function(value, values) {
        var item,
            i = values.length;

        while (i--) {
            item = values[i];
            if (item.value === value) {
                return item.label;
            }
        }
        return value;
    });
    _events.select = {
        change: function(cmp, item) {
            var textEl = _selector.get('.ui-btn-text', this.uiEl);

            if (textEl) {
                textEl.innerHTML = item.label;
            }
        },
        loading: function(cmp, isLoading) {
            var textEl = _selector.get('.ui-btn-text', this.uiEl);

            if (isLoading) {
                cmp.setDisabled(true);
                if (cmp.config.emptyText) {
                    textEl.innerHTML = 'Loading';
                }
            } else {
                cmp.setDisabled(false);
                if (cmp.config.emptyText) {
                    textEl.innerHTML = cmp.config.emptyText;
                }
            }
        },
        disabled: function(cmp, isDisabled) {
            var btnEl = _selector.get('.ui-btn', this.uiEl);

            if (isDisabled) {
                _selector.addClass(btnEl, 'ui-disabled');
            } else {
                _selector.removeClass(btnEl, 'ui-disabled');
            }
        }
    };

    _events.button = {
        disabled: function(cmp, isDisabled) {
            if (isDisabled) {
                _selector.addClass(this.el, 'ui-disabled');
            } else {
                _selector.removeClass(this.el, 'ui-disabled');
            }
        }
    };

    function _setUIBtn(ui) {
        _removeUI('ui-btn-up-{{ui}}', this.config.ui, this.el);
        _addUI('ui-btn-up-{{ui}}', ui, this.el);

        this.config.ui = ui;
    };

    if (_views.Button) {
        _views.Button.prototype.setUI = _setUIBtn;
    }
    if (_views.TabButton) {
        _views.TabButton.prototype.setUI = _setUIBtn;
    }

    function _setUIBar(ui) {
        _removeUI('ui-bar-{{ui}}', this.config.ui, this.el);
        _addUI('ui-bar-{{ui}}', ui, this.el);

        this.config.ui = ui;
    };

    if (_views.Header) {
        _views.Header.prototype.setUI = _setUIBar;
    }
    if (_views.Footer) {
        _views.Footer.prototype.setUI = _setUIBar;
    }
    if (_views.Toolbar) {
        _views.Toolbar.prototype.setUI = _setUIBar;
    }

    _events.radiobutton = {
        check: function() {
            if (!this.iconEl) {
                this.iconEl = _selector.get('.ui-icon', this.uiEl);
            }
            _selector.removeClass(this.iconEl, 'ui-icon-radio-off');
            _selector.addClass(this.iconEl, 'ui-icon-radio-on');
            _selector.removeClass(this.uiEl, 'ui-radio-off');
            _selector.addClass(this.uiEl, 'ui-radio-on');
        },
        uncheck: function() {
            if (!this.iconEl) {
                this.iconEl = _selector.get('.ui-icon', this.uiEl);
            }
            _selector.removeClass(this.iconEl, 'ui-icon-radio-on');
            _selector.addClass(this.iconEl, 'ui-icon-radio-off');
            _selector.removeClass(this.uiEl, 'ui-radio-on');
            _selector.addClass(this.uiEl, 'ui-radio-off');
        }
    };

    _events.panel = {
        afterrender: function() {
            overthrow.set();
        },
        expand: function() {
            var page = _selector.get('.ui-page-active');

            _selector.addClass(page, 'active');
            _selector.addClass(this.el, 'active');
        },
        collapse: function() {
            var page = _selector.get('.ui-page-active');

            _selector.removeClass(page, 'active');
            _selector.removeClass(this.el, 'active');
        }
    };

    _handlebars.registerHelper('isFlipSwitchOptionActive', function(values, index, value, options) {
        value = value || values[0].value;
        var current = values[index].value;

        if (current === value) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    _events.flipswitch = {
        afterrender: function() {
            var on = _selector.get('.ui-btn-active', this.uiEl),
                off = _selector.get('.ui-btn-down-c', this.uiEl),
                handle = _selector.get('.ui-slider-handle', this.uiEl);
            this.on('check', function() {
                on.style.width = '100%';
                off.style.width = '0%';
                handle.style.left = '100%';
            }, this, this.priority.VIEWS);
            this.on('uncheck', function() {
                on.style.width = '0%';
                off.style.width = '100%';
                handle.style.left = '0%';
            }, this, this.priority.VIEWS);
        }
    };

    _events.textarea = {
        afterrender: function() {
            this.eventFocus = new _fsevent.Focus({
                autoAttach: true,
                el: this.el,
                scope: this,
                handler: function() {
                    _selector.addClass(this.el, 'ui-focus');
                }
            });
            this.eventBlur = new _fsevent.Blur({
                autoAttach: true,
                el: this.el,
                scope: this,
                handler: function() {
                    _selector.removeClass(this.el, 'ui-focus');
                }
            });
        }
    };

    _events.checkbox = {
        afterrender: function() {
            var iconEl = _selector.get('.ui-icon', this.uiEl);
            this.on('check', function() {
                _selector.removeClass(this.uiEl, 'ui-checkbox-off');
                _selector.removeClass(iconEl, 'ui-icon-checkbox-off');
                _selector.addClass(this.uiEl, 'ui-checkbox-on');
                _selector.addClass(iconEl, 'ui-icon-checkbox-on');
            }, this, this.priority.VIEWS);
            this.on('uncheck', function() {
                _selector.removeClass(this.uiEl, 'ui-checkbox-on');
                _selector.removeClass(iconEl, 'ui-icon-checkbox-on');
                _selector.addClass(this.uiEl, 'ui-checkbox-off');
                _selector.addClass(iconEl, 'ui-icon-checkbox-off');
            }, this, this.priority.VIEWS);
        }
    };

    _events.textfield = {
        afterrender: function() {
            var parentNode = this.el.parentNode,
                self = this;

            self.eventFocus = new _fsevent.Focus({
                autoAttach: true,
                el: self.el,
                scope: self,
                handler: function() {
                    _selector.addClass(parentNode, 'ui-focus');
                }
            });
            self.eventBlur = new _fsevent.Blur({
                autoAttach: true,
                el: self.el,
                scope: self,
                handler: function() {
                    _selector.removeClass(parentNode, 'ui-focus');
                }
            });
            if (self.config.clearBtn) {
                self.clearBtnEl = document.getElementById(self.config.id + '-clearbtn');
                var keyupHandler = function() {
                    if (self.config.clearBtn <= self.el.value.length) {
                        _selector.removeClass(self.clearBtnEl, 'ui-input-clear-hidden');
                    } else {
                        _selector.addClass(self.clearBtnEl, 'ui-input-clear-hidden');
                    }
                };
                self.eventKeyup = new _fsevent.Abstract({
                    eventName: 'keyup',
                    autoAttach: true,
                    el: self.el,
                    globalFwd: window,
                    scope: self,
                    handler: keyupHandler
                });
                self.eventClick = new _fsevent.Click();
                self.eventClick.attach(self.clearBtnEl, function() {
                    if (_selector.hasClass(self.clearBtnEl, 'ui-disabled')) {
                        return false;
                    }
                    self.el.value = '';
                    keyupHandler.call(self);
                }, self);
            }
        },
        disabled: function(cmp, isDisabled) {
            if (isDisabled) {
                _selector.addClass(this.el, 'ui-disabled');
                if (this.clearBtnEl) {
                    _selector.addClass(this.clearBtnEl, 'ui-disabled');
                }
            } else {
                _selector.removeClass(this.el, 'ui-disabled');
                if (this.clearBtnEl) {
                    _selector.removeClass(this.clearBtnEl, 'ui-disabled');
                }
            }
        }
    };

    return _parent.subclass({

        xtype: 'jquerymobile',
        className: 'Fs.engines.JqueryMobile',

        constructor: function() {
            var q = _fs.Selector,
                html = q.get('html'),
                body = q.get('body');
            q.addClass(html, 'ui-mobile');
            q.addClass(body, 'ui-mobile-viewport');
        },

        getTpl: function(name) {
            return (_tpls[name] ||
                console.error('JqueryMobile engine: Template "%s" does not exists.', name));
        },

        getEvents: function(name) {
            return _events[name];
        }

    });

}());


Fs.App = (function() {

    var _fs = Fs,
        _debug = _fs.debug,
        _storage = _fs.Storage,
        _engines = _fs.engines;

    var _getEngine = function(name, defaultEngine) {
        var engine = _engines[name];
        if (engine) {
            return engine;
        }
        if (name) {
            _debug.error('app', 'Given engine "' + name +
                '" does not exists. Using "' + defaultEngine + '" instead.');
        }
        return _engines[defaultEngine];
    };

    return _fs.subclass({

        defaultEngine: 'Homemade',
        templateLoaded: true,
        domReady: false,

        constructor: function(opts) {
            var self = this;

            self.config = opts || {};
            if (typeof self.config.debug !== 'undefined') {
                _debug.set(self.config.debug);
            }
            if (typeof self.config.defaultRoute === 'string') {
                _fs.History.setDefaultRoute(self.config.defaultRoute);
            }
            if (self.config.require && self.config.require.templates) {
                self.templateLoaded = false;
                _fs.Templates.load(self.config.require.templates, function() {
                    self.templateLoaded = true;

                    if (self.domReady === true &&
                        self.config.onready) {
                        self.config.onready();
                    }
                });
            }
            if (self.config.onready) {
                self.registerListener();
            }
            
            _fs.config.engine = _getEngine(self.config.engine, self.defaultEngine);
            if (self.config.ui) {
                _fs.config.ui = self.config.ui;
            }
        },

        registerListener: function() {

            var self = this;

            function ready() {
                if (!self.domReady) {
                    document.removeEventListener('DOMContentLoaded', ready);
                }

                self.domReady = true;
                if (self.templateLoaded === true) {
                    self.config.onready();
                }
            };

            document.addEventListener('DOMContentLoaded', ready);
        }
    });

})();
