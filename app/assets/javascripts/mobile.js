//= require ./mobile/src/appvar.js
//= require ./mobile/vendors/handlebars.js
//= require ./mobile/vendors/fastclick.js
//= require ./mobile/vendors/moa.js
//= require ./mobile/vendors/helpers/i18n.js
//= require ./mobile/src/bootstrap.js

//= require ./mobile/src/configuration.js

//= require ./mobile/src/controllers/errors.js
//= require ./mobile/src/controllers/helpers.js
//= require ./mobile/src/lang/cn.js
//= require ./mobile/src/views/layout.js
//= require ./mobile/src/views/map.js
//= require ./mobile/src/views/contact.js
//= require ./mobile/src/views/home.js
//= require ./mobile/src/views/news/detail.js
//= require ./mobile/src/views/news/search.js
//= require ./mobile/src/views/user/favorites.js
//= require ./mobile/src/views/user/login.js
//= require ./mobile/src/views/user/profile.js
//= require ./mobile/src/views/user/register.js
//= require ./mobile/src/views/more/menu.js
//= require ./mobile/src/views/more/about.js
//= require ./mobile/src/controllers/main.js
//= require ./mobile/templates/compiled/all.js
//= require ./mobile/src/app.js
