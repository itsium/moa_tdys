#encoding: utf-8
ActiveAdmin.register Category do
	menu :priority => 4, label: "分类"
	config.filters = false
  actions :all, :except => [:show]
 
 form do |f|
    f.inputs "Content information" do
     f.input :name
     f.input :color, :as => :select, :collection => {:红色 => "red", :蓝色 => "blue", :橙色 => "orange", :绿色 => "yellow"}, :include_blank => false
   end
   f.actions
 end

 index do
  selectable_column
    column :id do |e|
      link_to e.id, admin_category_path(e)
    end    
    column :name do |e|
      link_to e.name, admin_category_path(e)
    end
    default_actions
 end
end
