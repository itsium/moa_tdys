#encoding: utf-8
ActiveAdmin.register User do
  menu :priority => 2, label: "用户"
  filter :email
  filter :name
  filter :company
  filter :phone
  filter :qq
  filter :wechat
  filter :role
  index do
    selectable_column
    column :id do |u|
    	link_to u.id, admin_user_path(u)
    end    
    column :email do |u|
    	link_to u.email, admin_user_path(u)
    end
    column t("name"), :name
    column :company 
    column :role
    column :phone    
    column :qq    
    column :wechat    
    column :interested    
    default_actions
  end
  form do |f|
    f.inputs "User Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :name
      f.input :company
      f.input :role
      f.input :phone
      f.input :qq
      f.input :wechat
      f.input :interested
    end
    f.actions
  end

    show do |user|
        attributes_table do
            row :email
            row :name
            row :company
            row :role
            row :phone
            row :qq
            row :wechat
            row :interested
        end
    end
end
