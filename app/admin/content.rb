#encoding: utf-8
ActiveAdmin.register Content do
	menu :priority => 3, label: "新闻活动"
  filter :title
  filter :data

  index do
    selectable_column
    column :id do |e|
      link_to e.id, admin_content_path(e)
    end    
    column :title do |e|
      link_to e.title, admin_content_path(e)
    end
    column :data do |e|
      link_to  ((e.data.size >= 100) ? "#{e.data[0,100]} ... " : e.data), admin_content_path(e)
    end
    column :created_at
    default_actions
  end

	 form :html => { :multipart => true } do |f|
   f.inputs "Content information" do
     f.input :title
     f.input :category, :as => :select
     f.input :data
     f.input :_is_event, :as => :boolean, input_html: { id: "show_event_info"}
   end

   f.inputs "Content images" do
     f.has_many :content_medias do |p|
       p.input :image, :as => :file, :label => "Image",:hint => p.object.image.nil? ? p.template.content_tag(:span, "No Image Yet") : p.template.image_tag(p.object.image.url(:thumb))
       p.input :_destroy, :as=>:boolean, :required => false, :label => 'Remove image'
     end 
   end
   
   f.inputs name: "Event info", id:  "event_info", style: "display: none;", :for => [f.object.event_info || EventInfo.new ]  do | event_info |
     event_info.input :adress
     event_info.input :map_image, :as => :file, :label => "Map Image"
     event_info.input :start_time, :as => :just_datetime_picker
     event_info.input :end_time, :as => :just_datetime_picker
     event_info.input :total_seat
     event_info.input :open_subscription
   end
   f.actions
 end

  # controller do
  #   def create
  #     lastcontent = Content.all(:order => "news_id DESC").first
  #     lastid = (lastcontent.nil?) ? 0 : lastcontent.news_id
  #     params[:content][:news_id] = (lastid < 500000) ? lastid+50000 : lastid+1
  #     super
  #   end
  # end

  show do |content|
	  attributes_table do
	    row :title
	    row :category
	    row :data

	    row :content_medias do |p|
	    	ul do
	    		content.content_medias.each do |img|
	    			li do 
	    				image_tag(img.image.url(:thumb))
	    			end
	    		end
			  end
		  end

	  end
    if content.event_info != nil
      panel t("event_info") do
        attributes_table_for content.event_info do
          row :adress 
          row :map_image do |p|
            image_tag(p.map_image.url(:original))
          end
          row :total_seat
          row :seats_left
          row :subscribers do |ei|
            link_to "报名会员列表", "/admin/users?utf8=%E2%9C%93&q%5Bcontents_id_eq%5D=#{ei.content.id}&order=id_desc"
          end
        end
      end
    end
	end
end
