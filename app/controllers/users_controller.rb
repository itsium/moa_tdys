class UsersController < ApplicationController
	before_filter :authenticate_user!, :except => :me

  def me

  end

  def favorites
    skip = 0
    limit = 10
    if params[:start] then skip = Integer(params[:start]) end
    if params[:limit] then limit = Integer(params[:limit]) end
    @contents = current_user.favorites
    @total = @contents.count()
    @contents = @contents.includes(:category, :content_medias).all(
      :limit => limit,
      :offset => skip,
      :order => 'created_at DESC')
  end

  def add_favorite
  		@content = Content.find_by_id(params[:user][:content_id])
  		@success = false
  		if @content.nil?
  			@message = I18n.t("errors.content_not_exist")
  		elsif current_user.favorites.include? @content
  			@message = I18n.t("errors.content_already_existe")
  		else
  			@success = true
  			current_user.favorites << @content
  			@message = I18n.t("success.favorite_added")
  		end
  end

  def delete_favorite
  	  @content = Content.find_by_id(params[:user][:content_id])
  		@success = false
  		if @content.nil?
  			@message = I18n.t("errors.content_not_exist")
  		elsif not current_user.favorites.include? @content
  			@message = I18n.t("errors.favorite_not_exist")
  		else
  			@success = true
  			current_user.favorites.delete @content
  			@message = I18n.t("success.favorite_deleted")
  		end
  end

  def list_favorites
  	@favorites = current_user.favorites
  	@success = true
 	end
end
