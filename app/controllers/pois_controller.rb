class PoisController < InheritedResources::Base

    def index

        @pois = Poi.all
        @request = request
        @center = {
            :latitude => Poi.average('latitude').to_f,
            :longitude => Poi.average('longitude').to_f
        }

    end

end
