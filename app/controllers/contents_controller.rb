class ContentsController < InheritedResources::Base
    before_filter :clean_is_event_flag, :only => [:create, :update]
    before_filter :authenticate_user!, :except => [:show, :index, :new, :invitation]
    layout "invitation_layout", :only => [ :invitation ]

    # GET /contents
    # GET /contents.json
    def index
        skip = 0
        limit = 10
        category = 0
        skip = Integer(params[:start]) if params[:start]
        limit = Integer(params[:limit]) if params[:limit]
        category = Integer(params[:category]) if params[:category]

        if category > 0 then
            @total = Content.where('category_id = ?', category).count()
            @contents = Content.includes(:category, :content_medias, :event_info).
                where('category_id = ?', category).
                limit(limit).offset(skip).
                order('created_at DESC')
        else
            @total = Content.all.count()
            @contents = Content.includes(:category, :content_medias, :event_info).all(
                :limit => limit,
                :offset => skip,
                :order => 'created_at DESC')
        end
    end

    def subscribe
        @success = false
        @content = Content.find_by_id params[:id]
        if @content.event_info.nil?
            @message = "This content is not a event !"
        elsif @content.event_info.total_seat != nil && @content.users.count >= @content.event_info.total_seat
            @message = "This event is full !"
        elsif @content.users.include? current_user
            @message = "You have already subscribed to this event."
        else
            @content.users << current_user
            if not current_user.favorites.include? @content
                current_user.favorites << @content
            end
            @success = true
            @message = "You have succefully subscribed to this events."
        end
    end

    def unsubscribe
        @success = false
        @content = Content.find_by_id params[:id]
        if @content.event_info.nil?
            @message = "This content is not a event !"
        elsif @content.users.include?(current_user) == false
            @message = "You are not subscribed to this event."
        else
            @content.users.delete(current_user)
            @success = true
            @message = "You have succefully unsubscribed to this events."
        end
    end

    def invitation
        @content = Content.find_by_id params[:id]
        my_host = "http://10.10.10.42:9030#{invitation_content_path(@content.id)}"
        #@qr = RQRCode::QRCode.new(invitation_content_url(@content.id), :size => 6, :level => :h)
        @qr = RQRCode::QRCode.new(my_host, :size => 6, :level => :h)
    end

private
    def clean_is_event_flag
       params[:user].delete("_is_event") == nil if params[:user][:_is_event]
    end
end
