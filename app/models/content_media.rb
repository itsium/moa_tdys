class ContentMedia < ActiveRecord::Base
  belongs_to :content
  belongs_to :company
  attr_accessible :caption, :content_id, :image, :company_id
  has_attached_file :image, :styles => { :original => '1100x1100>', :medium => "300x300>", :thumb => "120x120>" }, :default_url => "/assets/:style/image_missing.png"
  validates_attachment_presence :image
  validates_attachment_size :image, :less_than => 1.megabyte
  validates_attachment_content_type :image, :content_type => /^image\/(png|gif|jpeg)/
end
