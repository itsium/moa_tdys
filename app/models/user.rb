class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password,:company, :phone, :role, :password_confirmation, :remember_me, :qq, :wechat, :interested
  has_and_belongs_to_many :contents, :join_table => "users_contents"
  has_many :bookmarks
  has_many :favorites, source: :content, foreign_key: "content_id", through: :bookmarks
end
