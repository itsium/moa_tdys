class EventInfo < ActiveRecord::Base
	belongs_to :content
  attr_accessible :start_time, :end_time, :adress, :content_id, :open_subscription, :total_seat
  just_define_datetime_picker :start_time, :add_to_attr_accessible => true
  just_define_datetime_picker :end_time, :add_to_attr_accessible => true

  attr_accessible :map_image
  has_attached_file :map_image, :styles => { :original => '800x800>' }, :default_url => "/images/:style/missing.png"

  validates :adress, :presence => true  

  def seats_left
    return 0 if self.total_seat.nil?
  	return self.total_seat - self.content.users.count
  end

  def subscribers
  	self.content.users
  end
end
