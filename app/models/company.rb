class Company < ActiveRecord::Base
  attr_accessible :address, :description, :name, :phone, :content_medias_attributes
  has_many :content_medias, :dependent => :destroy
  accepts_nested_attributes_for :content_medias, :allow_destroy => :true, reject_if: proc { |t| t['image'].blank?  }
  
end
