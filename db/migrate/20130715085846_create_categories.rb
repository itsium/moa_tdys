class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.string :color
      t.boolean :is_event, default: false

      t.timestamps
    end
  end
end
