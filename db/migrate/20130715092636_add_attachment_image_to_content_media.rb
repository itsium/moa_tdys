class AddAttachmentImageToContentMedia < ActiveRecord::Migration
  def self.up
    change_table :content_media do |t|
      t.attachment :image
    end
  end

  def self.down
    drop_attached_file :content_media, :image
  end
end
