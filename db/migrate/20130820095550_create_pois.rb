class CreatePois < ActiveRecord::Migration
  def change
    create_table :pois do |t|
      t.string :name
      t.string :address
      t.float :latitude
      t.float :longitude
      t.string :description

      t.timestamps
    end
  end
end
