class AddFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :company, :string
    add_column :users, :role, :string
    add_column :users, :phone, :string
    add_index :users, :company 
  end
end
