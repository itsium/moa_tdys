class AddNewsIdToContents < ActiveRecord::Migration
  def change
    add_column :contents, :news_id, :integer
    add_index :contents, :news_id, :unique => true
  end
end
