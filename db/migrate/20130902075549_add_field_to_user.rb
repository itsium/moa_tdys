class AddFieldToUser < ActiveRecord::Migration
  def change
    add_column :users, :qq, :string
    add_column :users, :wechat, :string
    add_column :users, :interested, :string
  end
end
