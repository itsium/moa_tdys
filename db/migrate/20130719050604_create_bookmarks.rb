class CreateBookmarks < ActiveRecord::Migration
  def change
    create_table :bookmarks do |t|
      t.integer :user_id
      t.integer :content_id

      t.timestamps
    end
    add_index :bookmarks, [:user_id, :content_id]
    add_index :bookmarks, :content_id
  end
end
